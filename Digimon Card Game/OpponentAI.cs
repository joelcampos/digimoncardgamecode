﻿using System.Collections.Generic;

namespace Digimon_Card_Game
{
    public static class OpponentAI
    {
        public static int GetHandActiveChoice(Hand redHand, int deckCount)
        {
            int index = -1;

            List<int> R_Cards = redHand.GetSpecificLevelCards("R");
            //if the hand has a R digimon place it.
            if (R_Cards.Count > 0)
            {
                int rand = Rand.V(R_Cards.Count);
                index = R_Cards[rand];
                return index;
            }
            else
            {
                //if there is not R digimon redraw ONLY if the deck size is > 7.
                if (deckCount > 7)
                {
                    //Return -1 to mark redraw.
                    return -1;
                }
                else
                {
                    List<int> C_Cards = redHand.GetSpecificLevelCards("C");
                    //otherwise suck it up and place C
                    if (C_Cards.Count > 0)
                    {
                        int rand = Rand.V(C_Cards.Count);
                        index = C_Cards[rand];
                        return index;
                    }
                    else
                    {
                        //if thre is not C then U.
                        List<int> U_Cards = redHand.GetSpecificLevelCards("U");
                        int rand = Rand.V(U_Cards.Count);
                        index = U_Cards[rand];
                        return index;
                    }
                }
            }
        }

        public static int GetHandDPChoice(Hand redHand, CardInfo activeInfo)
        {
            List<int> candidates = new List<int>();

            //add all digimon cards as candidates
            for(int x = 0; x < 4; x++)
            {
                if(redHand.GetSprite(x) != null)
                {
                    if (redHand.GetSprite(x).Class == "Digimon") { candidates.Add(x); }
                }
            }

            //if a evo card can be use successfully, take this branch.
            if(CanAnEvolutionCardBeUsed(redHand, activeInfo))
            {
                //Get all evo cards in the hand
                List<int> EvoCards = new List<int>();
                for (int x = 0; x < 4; x++)
                {
                    if (redHand.GetSprite(x).Class == "Evolution") { EvoCards.Add(x); }
                }

                //remove evo candidates 
                for (int x = 0; x < EvoCards.Count; x++)
                {
                    List<int> evoCandidates = GetEvoCandidatesForEvoCard(redHand.GetSprite(candidates[x]).Name, redHand, activeInfo);
                    
                    //if any of the evoCandidates is in the candidates list, remove them
                    for(int y = 0; y < evoCandidates.Count; y++)
                    {
                        if(candidates.Contains(evoCandidates[y]))
                        {
                            candidates.RemoveAt(candidates.IndexOf(evoCandidates[y]));
                        }
                    }
                }

                //and if there is a card left place it
                if(candidates.Count > 0)
                {
                    if(candidates.Count > 1)
                    {
                        int lastCandidate = -1;
                        int currentLowestPPlus = 1000;
                        //pick the one with the highest Pplus
                        for(int x = 0; x < candidates.Count; x++)
                        {
                            if(redHand.GetSprite(candidates[x]).PlusP < currentLowestPPlus)
                            {
                                lastCandidate = candidates[x];
                                currentLowestPPlus = redHand.GetSprite(candidates[x]).PlusP;
                            }
                        }

                        //Return the last candidate
                        return lastCandidate;
                    }
                    else
                    {
                        //return only candidate
                        return candidates[0];
                    }
                }
                else
                {
                    //No candidates
                    return -1;
                }
            }
            //determine candidate based on DP needed for a possible digievolution
            else
            {
                string nextLevel = "none";
                if (activeInfo.level == "R") { nextLevel = "C"; }
                else if (activeInfo.level == "C") { nextLevel = "U"; }
                List<int> evoCandidates = redHand.GetEvoCandidates(activeInfo.type, nextLevel, 1000);

                //if any of the evoCandidates is in the candidates list, remove them
                for (int y = 0; y < evoCandidates.Count; y++)
                {
                    if (candidates.Contains(evoCandidates[y]))
                    {
                        candidates.RemoveAt(candidates.IndexOf(evoCandidates[y]));
                    }
                }

                //and if there is a card left place it
                if (candidates.Count > 0)
                {
                    if (candidates.Count > 1)
                    {
                        int lastCandidate = -1;
                        int currentLowestPPlus = 1000;
                        //pick the one with the highest Pplus
                        for (int x = 0; x < candidates.Count; x++)
                        {
                            if (redHand.GetSprite(candidates[x]).PlusP < currentLowestPPlus)
                            {
                                lastCandidate = candidates[x];
                                currentLowestPPlus = redHand.GetSprite(candidates[x]).PlusP;
                            }
                        }

                        //Return the last candidate
                        return lastCandidate;
                    }
                    else
                    {
                        //return only candidate
                        return candidates[0];
                    }
                }
                else
                {
                    //No candidates
                    return -1;
                }
            }
        }

        public static string GetAttackSelection(CardSprite activeSprite, CardSprite opponentSprite, Hand ownHand, Hand opponentHand)
        {
            /*
             * The goal is to defeat your opponent if possible
             * otherwise use the attack that would deal the most damage.
             * if you have a X effect that can work well agains opponent, use it.
             * 
             * This function will use a point-base system to determine which attack
             * should be selected. 
             * If an attack selection meets a condition, it would get points.
             * Once all validations are run, whatever attack selection with the most 
             * points will be selected. 
             * In case of a tie, the selection will be selected RND based on the
             * tie candidates.
            */

            int Circle_Points = 0;
            int Triangle_Points = 0;
            int Cross_Points = 0;

            //Add 3 points to any attack that can defeat opponent as it.
            if(activeSprite.Active_Circle >= opponentSprite.Active_HP) { Circle_Points += 3; }
            if(activeSprite.Active_Triangle >= opponentSprite.Active_HP) { Triangle_Points += 3; }
            if(activeSprite.Active_Cross >= opponentSprite.Active_HP) { Cross_Points += 3; }

            //Increase a point any attack that can be specifically booster by a card in the hand.
            if(ownHand.ContainsThisSupportEffect(S_Effect.Boost_Own_Specific, "CIRCLE")) { Circle_Points++; }
            if(ownHand.ContainsThisSupportEffect(S_Effect.Boost_Own_Specific, "TRIANGLE")) { Triangle_Points++; }
            if(ownHand.ContainsThisSupportEffect(S_Effect.Boost_Own_Specific, "CROSS")) { Cross_Points++; }

            //Increase 2 point any attack that can be specifically tripled by a card in the hand
            if (ownHand.ContainsThisSupportEffect(S_Effect.Boost_Own_Specific_Tripled, "CIRCLE")) { Circle_Points += 2; }
            if (ownHand.ContainsThisSupportEffect(S_Effect.Boost_Own_Specific_Tripled, "TRIANGLE")) { Triangle_Points += 2; }
            if (ownHand.ContainsThisSupportEffect(S_Effect.Boost_Own_Specific_Tripled, "CROSS")) { Cross_Points += 2; }

            //There is 50% change of giving Triangle an extra point as a "Safe Choice"
            if (Rand.WillHappen(50)) { Triangle_Points++; }

            //Give 1 extra point to CROSS if it has a useful effect:

            //If Cross Effect is counter there is 1 25% chance to get a point
            if (activeSprite.Data.Ceffect.Effect == X_Effect.Counter)
            {
                if (Rand.WillHappen(25)) { Cross_Points++; }
            }
            //if Cross effect is foeX and oppoent is actually the type useful with
            if (activeSprite.Data.Ceffect.Effect == X_Effect.Foe_XAttack)
            {
                if (opponentSprite.Type == activeSprite.Data.Ceffect.Effect_Mark)
                {
                    Cross_Points++;
                }
            }

            //Reduce 5 points any attack that can be counter by opponent
            //Unless FUCK it! (thre is a 20% change it wont reduced as a risk take).
            if (opponentSprite.Data.Ceffect.Effect == X_Effect.Counter)
            {
                switch(opponentSprite.Data.Ceffect.Effect_Mark)
                {
                    case "CIRCLE": if (!Rand.WillHappen(20)) { Circle_Points -= 5; } break;
                    case "TRIANGLE": if (!Rand.WillHappen(20)) { Triangle_Points -= 5; } break;
                    case "CROSS": if (!Rand.WillHappen(20)) { Cross_Points -= 5; } break;
                }
            }

            //Reduce 2 points any attack that can be reduced to 0 by opponents cross effect
            if (opponentSprite.Data.Ceffect.Effect == X_Effect.Attack_ToZero)
            {
                switch (opponentSprite.Data.Ceffect.Effect_Mark)
                {
                    case "CIRCLE": if (!Rand.WillHappen(20)) { Circle_Points -= 2; } break;
                    case "TRIANGLE": if (!Rand.WillHappen(20)) { Triangle_Points -= 2; } break;
                    case "CROSS": if (!Rand.WillHappen(20)) { Cross_Points -= 2; } break;
                }
            }


            //as a randon factor, each attack has a 30% change to get an extra point at the end
            if (Rand.WillHappen(30)) { Circle_Points++; }
            if (Rand.WillHappen(30)) { Triangle_Points++; }
            if (Rand.WillHappen(30)) { Cross_Points++; }

            //At the end, the attack with the most points wins
            if (Circle_Points > Triangle_Points && Circle_Points > Cross_Points) { return "CIRCLE"; }
            else if (Triangle_Points > Circle_Points && Triangle_Points > Cross_Points) { return "CIRCLE"; }
            else if (Cross_Points > Circle_Points && Cross_Points > Triangle_Points) { return "CIRCLE"; }
            else
            {
                if (Circle_Points == Triangle_Points) { if (Rand.WillHappen(50)) { return "CIRCLE"; } else { return "TRIANGLE"; } }
                else if (Circle_Points == Cross_Points) { if (Rand.WillHappen(50)) { return "CIRCLE"; } else { return "CROSS"; } }
                else if (Triangle_Points == Cross_Points) { if (Rand.WillHappen(50)) { return "TRIANGLE"; } else { return "CROSS"; } }
                else
                {
                    if (Rand.WillHappen(35)) { return "CIRCLE"; }
                    else if (Rand.WillHappen(40)) { return "TRIANGLE"; }
                    else { return "CROSS"; }
                }
            }
        }

        private static bool CanAnEvolutionCardBeUsed(Hand useHand, CardInfo activeInfo)
        {
            List<int> candidates = new List<int>();
            List<bool> statuses = new List<bool>();

            //add all evolution cards as candidates
            for (int x = 0; x < 4; x++)
            {
                if (useHand.GetSprite(x) != null)
                {
                    if (useHand.GetSprite(x).Class == "Evolution") { candidates.Add(x); statuses.Add(false); }
                }
            }

            //Check all candidates and save their statuses
            for(int x = 0; x < candidates.Count; x++)
            {
                List<int> evoCandidates = GetEvoCandidatesForEvoCard(useHand.GetSprite(candidates[x]).Name, useHand, activeInfo);
                if (evoCandidates.Count > 0) { statuses[x] = true; }
            }
            

            int trueStatusCount = 0;
            for(int x = 0; x < statuses.Count; x++)
            {
                if (statuses[x]) { trueStatusCount++; }
            }

            //return true if at least 1 status was true.
            return trueStatusCount > 0;
        }

        private static List<int> GetEvoCandidatesForEvoCard(string evoCardName, Hand useHand, CardInfo activeInfo)
        {
            string nextLevel = "none";
            string previousLevel = "none";
            List<int> evoCandidates = new List<int>();

            switch (evoCardName)
            {
                case "Speed Digivolve":
                    //Evolve to: [Same Type, Next Level] without DP
                    nextLevel = "none";
                    if (activeInfo.level == "R") { nextLevel = "C"; }
                    else if (activeInfo.level == "C") { nextLevel = "U"; }

                    //Use 1000 as the DP to basically disregard DP
                    evoCandidates = useHand.GetEvoCandidates(activeInfo.type, nextLevel, 1000);
                    break;
                case "Digi-devolve":
                    //Evolve to: [Same Type, previous level] without DP
                    previousLevel = "none";
                    if (activeInfo.level == "C") { previousLevel = "R"; }
                    else if (activeInfo.level == "U") { previousLevel = "C"; }

                    //Use 1000 as the DP to basically disregard DP
                    evoCandidates = useHand.GetEvoCandidates(activeInfo.type, previousLevel, 1000);
                    break;
            }

            return evoCandidates;
        }

        private static int GetSupportCardChoise(PlayerBattleData BlueData, PlayerBattleData RedData)
        {
            //Return Options [-1] == No selection | [0-3] = HandSlot 1-4 | [4] = Deck  
            int supportCardSelectionIndex = -1;

            //Initialize the Support Effect Analizer data to be use during this AI logic 
            SupportEffectAnalizer.InitializeAnalazerData(RedData, BlueData);

            //LOGIC: Determine which of the cards in your hand is the most useful.
            //if none of them are "good" select from deck if deck is not empty.

            //if hand is empty, select from deck
            if(RedData.CardsOnHand == 0)
            {
                if(RedData.CardsOnDeck > 0)
                {
                    supportCardSelectionIndex = 0;
                }
            }
            else
            {
                //Analize the hand for selection, if no cards are good or usuable,
                //select from deck if it is not empty

                List<int> candidateIndexes = new List<int>() { 0, 1, 2, 3 };

                //Step 1: check all cards on hand and determine if they activation conditions are met.
                for(int x = 0; x < 4; x++) 
                {
                    if(RedData.HandIndexIsOpenSlot(x))
                    {
                        //Remove this index as candidate
                        candidateIndexes.Remove(x);
                    }
                    else
                    {
                        //Check if the card meets its activation requirements
                        //otherwise remove it as candidate
                        bool meetsActivationCondition = SupportEffectAnalizer.IsConditionMet(RedData.CardOnHandAtIndex(x));
                        if(!meetsActivationCondition)
                        {
                            //Remove this index as candidate
                            candidateIndexes.Remove(x);
                        }
                    }
                }

                //Step 2: After verifying the activation conditions, if there are not hand candidates left,
                //select from deck, otherwise do a RND to select from the candidates
                if (candidateIndexes.Count == 0) 
                {
                    if (RedData.CardsOnDeck > 0)
                    {
                        supportCardSelectionIndex = 0;
                    }
                }
                else
                {
                    if(candidateIndexes.Count == 1)
                    {
                        supportCardSelectionIndex = candidateIndexes[0];
                    }
                    else
                    {
                        int rnd = Rand.Range(0, candidateIndexes.Count);
                        supportCardSelectionIndex = candidateIndexes[rnd];
                    }
                }

            }
            return supportCardSelectionIndex;
        }
    }
}
