﻿//Joel Campos
//2/27/2021
//Card Sprite Class

using System.Windows.Forms;
using System.Drawing;
using System;

namespace Digimon_Card_Game
{
    public class CardSprite
    {
        public CardSprite(int ID, BattleBoard board)
        {
            //Get the Card data from DB
            CardData = BattleBoard.CardDB[ID];

            //Card outerborder
            CardOuterBorder = new Panel();
            CardOuterBorder.Size = CardOuterBorderSize;
            CardOuterBorder.BorderStyle = BorderStyle.FixedSingle;
            CardOuterBorder.BackColor = Color.Black;

            BlinkingBorder = new PictureBox();
            BlinkingBorder.Size = CardOuterBorderSize;
            BlinkingBorder.Image = ImageServer.Blinking();
            BlinkingBorder.Visible = false;
            CardOuterBorder.Controls.Add(BlinkingBorder);
            BlinkingBorder.Location = new Point(0, 0);

            FaceDownSide = new PictureBox();
            FaceDownSide.Size = CardOuterBorderSize;
            FaceDownSide.Image = ImageServer.CardFaceDownImage();
            FaceDownSide.Visible = false;
            CardOuterBorder.Controls.Add(FaceDownSide);
            FaceDownSide.Location = new Point(0, 0);
            if (facedown) { FaceDownSide.Visible = false; }

            //Card Border
            CardBorder = new Panel();
            CardBorder.Size = CardBorderSize;
            CardBorder.BorderStyle = BorderStyle.None;
            switch(CardData.type)
            {
                case "FIRE":      CardBorder.BackColor = Color.Red; break;
                case "ICE":       CardBorder.BackColor = Color.Blue; break;
                case "NATURE":    CardBorder.BackColor = Color.Green; break;
                case "DARK":      CardBorder.BackColor = Color.Black; break;
                case "RARE":      CardBorder.BackColor = Color.Yellow; break;
                case "OPTION":    CardBorder.BackColor = Color.Silver; break;
                case "SEVENS":    CardBorder.BackColor = Color.Silver; break;
                case "EVOLUTION": CardBorder.BackColor = Color.Gold; break;
                default: throw new Exception("Card Sprite Creation: Invalid TYPE value. - Card ID: " + ID);
            }
            CardOuterBorder.Controls.Add(CardBorder);
            CardBorder.Location = new Point(2, 2);
            BlinkingBorder.SendToBack();
            
            //Card Image
            CardImage = new Panel();
            CardImage.Size = CardImageSize;
            CardImage.BackgroundImage = ImageServer.CardImage(ID);
            CardImage.BackgroundImageLayout = ImageLayout.Stretch;
            CardBorder.Controls.Add(CardImage);
            CardImage.Location = CardImageLoc;

            //Card Icon
            Icon = new PictureBox();
            Icon.Size = IconSize;
            switch(CardData.type)
            {
                case "OPTION": Icon.Image = ImageServer.OptionIcon(); break;
                case "SEVENS": Icon.Image = ImageServer.SevensIcon(); break;
                case "EVOLUTION": Icon.Image = ImageServer.EvolutionIcon(); break;
                default: Icon.Image = ImageServer.Level(CardData.level); break;
            }    
            Icon.SizeMode = PictureBoxSizeMode.StretchImage;
            CardImage.Controls.Add(Icon);
            switch (CardData.type)
            {
                case "OPTION": Icon.Location = IconRLoc; break;
                case "SEVENS": Icon.Location = IconRLoc; break;
                case "EVOLUTION": Icon.Location = IconRLoc; break;
                default: Icon.Location = IconLoc; break;
            }
            Icon.BackColor = Color.Transparent;
            //add the card to the board
            board.Controls.Add(CardOuterBorder);
            CardOuterBorder.BringToFront();

        }

        public void SetLocation(Point loc)
        {
            CardOuterBorder.Location = loc;
        }
        public void Dispose()
        {
            CardOuterBorder.Dispose();
        }
        public void PointCardForViewMode()
        {
            //CardOuterBorder.BackColor = Color.Fuchsia;
            //CardOuterBorder.BackgroundImage = ImageServer.Blinking();
            BlinkingBorder.Visible = true;
        }
        public void UnPointCardForViewMode()
        {
            //CardOuterBorder.BackColor = Color.Black;
            BlinkingBorder.Visible = false;
        }
        public void SetActiveStats(int modDenominator)
        {
            _ActiveHP = Data.hp / modDenominator;
            _ActiveCircle = Data.circle / modDenominator;
            _ActiveTriangle = Data.triangle / modDenominator;
            _ActiveCross = Data.cross / modDenominator;
        }
        public void DPResize()
        {
            //Outer border
            CardOuterBorder.Size = new Size(35, 39);
            //card border
            CardBorder.Location = new Point(1, 1);
            CardBorder.Size = new Size(31, 35);
            //card image
            CardImage.Location = new Point(1,4);
            CardImage.Size = new Size(29,29);
            //Level icon
            Icon.Size = new Size(10,10);

        }
        public void RestoreSize()
        {
            //Card outerborder
            CardOuterBorder.Size = CardOuterBorderSize;

            //Blinkin border
            BlinkingBorder.Size = CardOuterBorderSize;
            BlinkingBorder.Location = new Point(0, 0);

            //Card Border
            CardBorder.Size = CardBorderSize;
            CardBorder.Location = new Point(2, 2);
            BlinkingBorder.SendToBack();

            //Card Image
            CardImage.Size = CardImageSize;
            CardImage.Location = CardImageLoc;

            //Card Icon
            Icon.Size = IconSize;
            switch (CardData.type)
            {
                case "OPTION": Icon.Location = IconRLoc; break;
                case "SEVENS": Icon.Location = IconRLoc; break;
                case "EVOLUTION": Icon.Location = IconRLoc; break;
                default: Icon.Location = IconLoc; break;
            }
            CardOuterBorder.BringToFront();
        }
        public void FlipCard()
        {
            if(facedown)
            {
                //flip up
                facedown = false;
                FaceDownSide.Visible = false;
            }
            else
            {
                //flip down
                facedown = true;
                FaceDownSide.Visible = true;
            }
        }
        public void Hide()
        {
            CardOuterBorder.Visible = false;
        }
        public void Show()
        {
            CardOuterBorder.Visible = true;
        }


        public int ID { get { return CardData.id; } }
        public string Name { get { return CardData.name; } }
        public string Type { get { return CardData.type; }  }
        public string Class { get { return CardData.Class; }  }
        public string Level { get { return CardData.level; } }
        public int HP { get { return CardData.hp; } }
        public int Circle { get { return CardData.circle; } }
        public int Triangle { get { return CardData.triangle; } }
        public int Cross { get { return CardData.cross; } }
        public int DP { get { return CardData.dp; } }
        public int PlusP { get { return CardData.plusp; } }
        public string Cross_Effect { get { return CardData.xeffect; } }
        public string Cross_Priority { get { return CardData.xpriority; } }
        public string Support_Effect { get { return CardData.supporteffect; } }
        public string Support_Priority { get { return CardData.priority; } }



        //Active Stats
        private int _ActiveHP = 0;
        private int _ActiveCircle = 0;
        private int _ActiveTriangle = 0;
        private int _ActiveCross = 0;

        public int Active_HP { get { return _ActiveHP; } }
        public int Active_Circle { get { return _ActiveCircle; } }
        public int Active_Triangle { get { return _ActiveTriangle; } }
        public int Active_Cross { get { return _ActiveCross; } }

        public CardInfo Data { get { return CardData; } }
        public Point Location { get { return CardOuterBorder.Location; } }

        private Panel CardOuterBorder;
        private PictureBox BlinkingBorder;
        private Panel CardBorder;
        private Panel CardImage;
        private PictureBox Icon;
        private PictureBox FaceDownSide;

        //Data
        private Size CardOuterBorderSize = new Size(69, 90);
        private Size CardBorderSize = new Size(63, 84);
        private Size CardImageSize = new Size(61, 61);
        private Point CardImageLoc = new Point(1, 11);
        private Size IconSize = new Size(25, 25);
        private Point IconLoc = new Point(0, 0);
        private Point IconRLoc = new Point(40, 0);
        private bool facedown = false;
        private CardInfo CardData;
    }
}
