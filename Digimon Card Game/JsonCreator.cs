﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Collections;

namespace Digimon_Card_Game
{
    public partial class JsonCreator : Form
    {
        public JsonCreator()
        {
            InitializeComponent();
            string jsonFilePath = Directory.GetCurrentDirectory() + "\\Json\\CardDB.json";
            string rawdata = File.ReadAllText(jsonFilePath);
            MasterList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CardInfo>>(rawdata);

            for (int x = 0; x < MasterList.Count; x++)
            {
                listMainList.Items.Add(MasterList[x].id.ToString() + " - " + MasterList[x].name.ToString());
            }

            //Fill the lists
            for (int x = 0; x < 8; x++)
            {
                listCrossEffect.Items.Add(((X_Effect)x).ToString());
            }
            for (int x = 0; x < 71; x++)
            {
                listSupEffectCode.Items.Add(((S_Effect)x).ToString());
                listSupEffectCode2.Items.Add(((S_Effect)x).ToString());
                listSupEffectCode3.Items.Add(((S_Effect)x).ToString());
            }
            for (int x = 0; x < 23; x++)
            {
                listConditions.Items.Add(((Condition)x).ToString());
            }


            #region p checks
            /*
            //X priority check
            for (int x = 0; x < MasterList.Count; x++)
            {
                if (MasterList[x].Class != "Option" && MasterList[x].Class != "Evolution")
                {
                    if (MasterList[x].xpriority != "None" && MasterList[x].xpriority != "FAST" &&
                        MasterList[x].xpriority != "MEDIUM" && MasterList[x].xpriority != "SLOW")
                    {
                        throw new Exception("X priority is wrong for: " + MasterList[x].id + " " + MasterList[x].name);
                    }
                }
            }
            //support priority check
            for (int x = 0; x < MasterList.Count; x++)
            {
                if (MasterList[x].Class != "Evolution")
                {
                    if (MasterList[x].priority != "None" && MasterList[x].priority != "FAST" &&
                        MasterList[x].priority != "MEDIUM" && MasterList[x].priority != "SLOW")
                    {
                        throw new Exception("Priority is wrong for: " + MasterList[x].id + " " + MasterList[x].name);
                    }
                }
            }
            */
            #endregion

            #region Conditions added and verified
            /*
            //Add conditions
            int cardswithifs = 0;
            int cardswithconditions = 0;
            for (int x = 0; x < MasterList.Count; x++)
            {
                CardInfo activeCard = MasterList[x];
                if (activeCard.Class != "Evolution")
                {
                    if (activeCard.supporteffect.Contains("If "))
                    {
                        cardswithifs++;
                        //Attack selection conditions
                        if (activeCard.supporteffect.Contains("If own Attack is CIRCLE")) { activeCard.condition.condition = Condition.If_Own_Attack_Is; activeCard.condition.condition_Mark = "CIRCLE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Attack is TRIANGLE")) { activeCard.condition.condition = Condition.If_Own_Attack_Is; activeCard.condition.condition_Mark = "TRIANGLE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Attack is CROSS")) { activeCard.condition.condition = Condition.If_Own_Attack_Is; activeCard.condition.condition_Mark = "CROSS"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Attack is not CIRCLE")) { activeCard.condition.condition = Condition.If_Own_Attack_ISNOT; activeCard.condition.condition_Mark = "CIRCLE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Attack is not TRIANGLE")) { activeCard.condition.condition = Condition.If_Own_Attack_ISNOT; activeCard.condition.condition_Mark = "TRIANGLE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Attack is not CROSS")) { activeCard.condition.condition = Condition.If_Own_Attack_ISNOT; activeCard.condition.condition_Mark = "CROSS"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent uses CIRCLE")) { activeCard.condition.condition = Condition.If_Opo_Attack_Is; activeCard.condition.condition_Mark = "CIRCLE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent uses TRIANGLE")) { activeCard.condition.condition = Condition.If_Opo_Attack_Is; activeCard.condition.condition_Mark = "TRIANGLE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent uses CROSS")) { activeCard.condition.condition = Condition.If_Opo_Attack_Is; activeCard.condition.condition_Mark = "CROSS"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If both players use same Attack")) { activeCard.condition.condition = Condition.If_Both_Attack_Same; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If both Attacks are different")) { activeCard.condition.condition = Condition.If_Both_Attack_Different; cardswithconditions++; }

                        //Specialty conditions
                        if (activeCard.supporteffect.Contains("If own Specialty is FIRE")) { activeCard.condition.condition = Condition.If_Own_Speciality_Is; activeCard.condition.condition_Mark = "FIRE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Specialty is ICE")) { activeCard.condition.condition = Condition.If_Own_Speciality_Is; activeCard.condition.condition_Mark = "ICE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Specialty is NATURE")) { activeCard.condition.condition = Condition.If_Own_Speciality_Is; activeCard.condition.condition_Mark = "NATURE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Specialty is DARK")) { activeCard.condition.condition = Condition.If_Own_Speciality_Is; activeCard.condition.condition_Mark = "DARK"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Specialty is RARE")) { activeCard.condition.condition = Condition.If_Own_Speciality_Is; activeCard.condition.condition_Mark = "RARE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is FIRE or ICE")) { activeCard.condition.condition = Condition.If_Opo_Specialty_FIREICE;  cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is NATURE or DARK")) { activeCard.condition.condition = Condition.If_Opo_Specialty_NATUREDARK;  cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is FIRE,")) { activeCard.condition.condition = Condition.If_Opo_Speciality_Is; activeCard.condition.condition_Mark = "FIRE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is ICE")) { activeCard.condition.condition = Condition.If_Opo_Speciality_Is; activeCard.condition.condition_Mark = "ICE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is NATURE,")) { activeCard.condition.condition = Condition.If_Opo_Speciality_Is; activeCard.condition.condition_Mark = "NATURE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is DARK")) { activeCard.condition.condition = Condition.If_Opo_Speciality_Is; activeCard.condition.condition_Mark = "DARK"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is RARE")) { activeCard.condition.condition = Condition.If_Opo_Speciality_Is; activeCard.condition.condition_Mark = "RARE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is not FIRE")) { activeCard.condition.condition = Condition.If_Opo_Speciality_IsNot; activeCard.condition.condition_Mark = "FIRE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is not ICE")) { activeCard.condition.condition = Condition.If_Opo_Speciality_IsNot; activeCard.condition.condition_Mark = "ICE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is not NATURE")) { activeCard.condition.condition = Condition.If_Opo_Speciality_IsNot; activeCard.condition.condition_Mark = "NATURE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is not DARK")) { activeCard.condition.condition = Condition.If_Opo_Speciality_IsNot; activeCard.condition.condition_Mark = "DARK"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Specialty is not RARE")) { activeCard.condition.condition = Condition.If_Opo_Speciality_IsNot; activeCard.condition.condition_Mark = "RARE"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If Specialties are same")) { activeCard.condition.condition = Condition.If_Both_Specialties_Same; cardswithconditions++; }

                        //Level conditions
                        if (activeCard.supporteffect.Contains("If own Level is R")) { activeCard.condition.condition = Condition.If_Own_level_Is; activeCard.condition.condition_Mark = "R"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Level is C")) { activeCard.condition.condition = Condition.If_Own_level_Is; activeCard.condition.condition_Mark = "C"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Level is U")) { activeCard.condition.condition = Condition.If_Own_level_Is; activeCard.condition.condition_Mark = "U"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Level is A")) { activeCard.condition.condition = Condition.If_Own_level_Is; activeCard.condition.condition_Mark = "A"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's Level is A")) { activeCard.condition.condition = Condition.If_Opo_level_Is; activeCard.condition.condition_Mark = "A"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If both Levels are C")) { activeCard.condition.condition = Condition.If_Both_Levels_Are; activeCard.condition.condition_Mark = "C"; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Level is lower")) { activeCard.condition.condition = Condition.If_Own_level_IsLower; cardswithconditions++; }

                        //Hand Size condtions
                        if (activeCard.supporteffect.Contains("If 1 or less Cards left in Hand")) { activeCard.condition.condition = Condition.If_Own_Hand_LessThan; activeCard.condition.condition_Amount = 2; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own Cards in Hand 3 or more")) { activeCard.condition.condition = Condition.If_Own_Hand_MoreThan; activeCard.condition.condition_Amount = 2; cardswithconditions++; }


                        //HP conditions
                        if (activeCard.supporteffect.Contains("If own HP are less than 200")) { activeCard.condition.condition = Condition.If_OwnHP_Lessthan; activeCard.condition.condition_Amount = 200; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own HP are less than 500")) { activeCard.condition.condition = Condition.If_OwnHP_Lessthan; activeCard.condition.condition_Amount = 500; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own HP are more than opponent's HP")) { activeCard.condition.condition = Condition.If_OwnHp_IsHigher; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If own HP are less than opponent's HP")) { activeCard.condition.condition = Condition.If_OwnHp_IsLower; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's HP are lower then own")) { activeCard.condition.condition = Condition.If_OwnHp_IsHigher; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's HP are more than 1000")) { activeCard.condition.condition = Condition.If_OpoHp_MoreThan; activeCard.condition.condition_Amount = 1000; cardswithconditions++; }
                        if (activeCard.supporteffect.Contains("If opponent's HP are 1000+")) { activeCard.condition.condition = Condition.If_OpoHp_MoreThan; activeCard.condition.condition_Amount = 999; cardswithconditions++; }

                        //DP Cards conditions
                        if (activeCard.supporteffect.Contains("If opponent has more than 2 Cards in DP Slot")) { activeCard.condition.condition = Condition.If_Opo_DP_IsMore; activeCard.condition.condition_Amount = 2; cardswithconditions++; }


                    }
                    else
                    {
                        activeCard.condition.condition = Condition.None;
                    }
                }
            }

            //Verify
            txtSupport.Text = "Cards with ifs: " + cardswithifs + "\r\nCards with conditions added: " + cardswithconditions;

            for (int x = 0; x < MasterList.Count; x++)
            {
                CardInfo activeCard = MasterList[x];
                if (activeCard.Class != "Evolution")
                {
                    if (activeCard.supporteffect.Contains("If "))
                    {
                        if (activeCard.condition.condition == Condition.None)
                        {
                            throw new Exception("Card missing condition: " + activeCard.id + " - " + activeCard.name);
                        }
                    }
                }
            }
            */
            #endregion

            #region support effects
            /*
            int cardswithSupport = 0;
            int cardsAddedSuppor = 0;
            for(int x = 0; x < MasterList.Count; x++)
            {
                CardInfo activeCard = MasterList[x];
                if (activeCard.Class != "Evolution")
                {
                    if(activeCard.supporteffect != "none")
                    {
                        cardswithSupport++;
                        bool effect1added = false;
                        bool effect2added = false;
                        List<string> ignorelist = new List<string>();

                        for(int y = 0; y < 3; y++)
                        {
                            bool effectadded = false;
                            string effect = "ttack first"; S_Effect EnumEffect = S_Effect.Attack_First;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ttack second"; EnumEffect = S_Effect.Attack_Second;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange own Specialty to FIRE"; EnumEffect = S_Effect.Change_own_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "FIRE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "FIRE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "FIRE";  effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange own Specialty to ICE"; EnumEffect = S_Effect.Change_own_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "ICE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "ICE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "ICE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange own Specialty to NATURE"; EnumEffect = S_Effect.Change_own_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "NATURE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "NATURE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "NATURE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange own Specialty to DARK"; EnumEffect = S_Effect.Change_own_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "DARK"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "DARK"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "DARK"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange own Specialty to RARE"; EnumEffect = S_Effect.Change_own_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "RARE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "RARE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "RARE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange opponent's Specialty to FIRE"; EnumEffect = S_Effect.Change_Opo_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "FIRE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "FIRE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "FIRE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange opponent's Specialty to ICE"; EnumEffect = S_Effect.Change_Opo_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "ICE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "ICE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "ICE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange opponent's Specialty to NATURE"; EnumEffect = S_Effect.Change_Opo_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "NATURE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "NATURE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "NATURE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange opponent's Specialty to DARK"; EnumEffect = S_Effect.Change_Opo_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "DARK"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "DARK"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "DARK"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "hange opponent's Specialty to RARE"; EnumEffect = S_Effect.Change_Opo_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "RARE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "RARE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "RARE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }
                            
                            effect = "wap own Specialty with opponent's Specialty"; EnumEffect = S_Effect.Swap_Speciality;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "pponent's Specialty becomes the same as own Specialty"; EnumEffect = S_Effect.Change_Opo_Speciality_ToOWn;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect);}
                            }

                            effect = "Specialty become same as the opponent's"; EnumEffect = S_Effect.Change_Own_Speciality_ToOpo;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own Attack Power +100"; EnumEffect = S_Effect.Boost_Own_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 100; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 100; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 100; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own Attack Power +200"; EnumEffect = S_Effect.Boost_Own_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 200; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 200; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 200; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own Attack Power +300"; EnumEffect = S_Effect.Boost_Own_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 300; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 300; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 300; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own Attack Power +400"; EnumEffect = S_Effect.Boost_Own_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 400; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 400; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 400; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own Attack Power +500"; EnumEffect = S_Effect.Boost_Own_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 500; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 500; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 500; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }
                         
                            effect = "oost own CIRCLE Attack Power +100"; EnumEffect = S_Effect.Boost_Own_Specific;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; activeCard.effect1.Effect_Amount = 100; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; activeCard.effect2.Effect_Amount = 100; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; activeCard.effect3.Effect_Amount = 100; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own CIRCLE Attack Power +200"; EnumEffect = S_Effect.Boost_Own_Specific;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; activeCard.effect1.Effect_Amount = 200; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; activeCard.effect2.Effect_Amount = 200; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; activeCard.effect3.Effect_Amount = 200; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own CIRCLE Attack Power +300"; EnumEffect = S_Effect.Boost_Own_Specific;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; activeCard.effect1.Effect_Amount = 300; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; activeCard.effect2.Effect_Amount = 300; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; activeCard.effect3.Effect_Amount = 300; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own CROSS Attack Power +100"; EnumEffect = S_Effect.Boost_Own_Specific;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CROSS"; activeCard.effect1.Effect_Amount = 100; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CROSS"; activeCard.effect2.Effect_Amount = 100; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CROSS"; activeCard.effect3.Effect_Amount = 100; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own CROSS Attack Power +200"; EnumEffect = S_Effect.Boost_Own_Specific;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CROSS"; activeCard.effect1.Effect_Amount = 200; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CROSS"; activeCard.effect2.Effect_Amount = 200; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CROSS"; activeCard.effect3.Effect_Amount = 200; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oost own CROSS Attack Power +300"; EnumEffect = S_Effect.Boost_Own_Specific;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CROSS"; activeCard.effect1.Effect_Amount = 300; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CROSS"; activeCard.effect2.Effect_Amount = 300; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CROSS"; activeCard.effect3.Effect_Amount = 300; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn Attack Power is doubled"; EnumEffect = S_Effect.Boost_Own_Double;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn Attack Power is tripled"; EnumEffect = S_Effect.Boost_Own_Triple;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "CIRCLE Attack Power is Tripled"; EnumEffect = S_Effect.Boost_Own_Specific_Tripled;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn Attack Power is boosted by the number of own HP"; EnumEffect = S_Effect.Boost_Own_ByHP;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "dd number of Cards in Hand x100 to own Attack Power"; EnumEffect = S_Effect.Boost_Own_Attack_By_HandAmount100;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "iscard Cards in own DP Slot and multiply Attack Power by number of discards"; EnumEffect = S_Effect.Discard_Own_AllDP_MultiATAByAmount;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "iscard own Hand. Multiply own Attack Power by number of discarded Cards"; EnumEffect = S_Effect.Discard_Own_AllHand_MultiATAByAmount;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn Attack Power is halved"; EnumEffect = S_Effect.Lower_Own_Half;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn Attack Power becomes 0"; EnumEffect = S_Effect.Lower_Own_All_ToZero;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "pponent's Attack Power to 0"; EnumEffect = S_Effect.Lower_Opo_All_ToZero;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ower opponent's CIRCLE Attack Power to 0"; EnumEffect = S_Effect.Lower_Opo_Specific_ToZero;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ower opponent's TRIANGLE Attack Power to 0"; EnumEffect = S_Effect.Lower_Opo_Specific_ToZero;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "TRIANGLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "TRIANGLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "TRIANGLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }
                            
                            effect = "ower opponent's CROSS Attack Power to 0"; EnumEffect = S_Effect.Lower_Opo_Specific_ToZero;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CROSS"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CROSS"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CROSS"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "pponent's Attack Power is halved"; EnumEffect = S_Effect.Lower_Opo_Half;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ower opponent's Attack Power -100"; EnumEffect = S_Effect.Lower_Opo_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; activeCard.effect1.Effect_Amount = 100; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 100; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 100; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ower opponent's Attack Power -200"; EnumEffect = S_Effect.Lower_Opo_AllAttack;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; activeCard.effect1.Effect_Amount = 200; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 200; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 200; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "pponent's Attack Power becomes 300"; EnumEffect = S_Effect.Opo_Attack_Becomes;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; activeCard.effect1.Effect_Amount = 300; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 300; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 300; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn CIRCLE Attack Power becomes same as own HP"; EnumEffect = S_Effect.Own_Specific_Becomes_HP;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn CIRCLE Attack Power becomes same as own HP"; EnumEffect = S_Effect.Own_Specific_Becomes_HP;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "se CIRCLE "; EnumEffect = S_Effect.Change_Own_AttackTo;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "Opponent uses CIRCLE."; EnumEffect = S_Effect.Change_Opo_AttackTo;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "Opponent uses TRIANGLE."; EnumEffect = S_Effect.Change_Opo_AttackTo;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "TRIANGLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "TRIANGLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "TRIANGLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "Opponent uses CROSS."; EnumEffect = S_Effect.Change_Opo_AttackTo;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CROSS"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CROSS"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CROSS"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "oth Players use CIRCLE"; EnumEffect = S_Effect.Change_Both_AttackTo;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Mark = "CIRCLE"; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Mark = "CIRCLE"; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Mark = "CIRCLE"; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn HP becomes 10"; EnumEffect = S_Effect.Own_HP_Becomes;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 10; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 10; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 10; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn HP becomes 700"; EnumEffect = S_Effect.Own_HP_Becomes;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 700; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 700; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 700; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "pponent's HP becomes 10"; EnumEffect = S_Effect.Opo_HP_Becomes;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 10; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 10; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 10; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "wn HP become same as opponent's"; EnumEffect = S_Effect.Own_HP_BecomesOpp;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "pponent's HP become same as own"; EnumEffect = S_Effect.Opo_HP_BecomesOwn;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ecover own HP by +100"; EnumEffect = S_Effect.Recover_Own_Hp;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 100; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 100; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 100; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ecover own HP by +200"; EnumEffect = S_Effect.Recover_Own_Hp;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 200; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 200; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 200; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ecover own HP by +300"; EnumEffect = S_Effect.Recover_Own_Hp;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 300; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 300; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 300; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ecover own HP by +500"; EnumEffect = S_Effect.Recover_Own_Hp;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 500; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 500; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 500; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            effect = "ecover own HP by +1000"; EnumEffect = S_Effect.Recover_Own_Hp;
                            if (activeCard.supporteffect.Contains(effect) && !ignorelist.Contains(effect))
                            {
                                if (!effect1added) { activeCard.effect1.Effect = EnumEffect; activeCard.effect1.Effect_Amount = 1000; effect1added = true; effectadded = true; }
                                else if (!effect2added) { activeCard.effect2.Effect = EnumEffect; activeCard.effect2.Effect_Amount = 1000; effect2added = true; effectadded = true; }
                                else { activeCard.effect3.Effect = EnumEffect; activeCard.effect3.Effect_Amount = 1000; effectadded = true; }

                                if (effectadded) { ignorelist.Add(effect); }
                            }

                            if (effectadded) { cardsAddedSuppor++; }
                        }
                    }   
                }
            }

            txtSupport.Text = "Cards with effecs: " + cardswithSupport + "\r\nCards with effects added: " + cardsAddedSuppor;
            */
            #endregion

            #region x effects
            /*
            int cardswithXeffect = 0;
            int cardsAddedXeffect = 0;

            for (int x = 0; x < MasterList.Count; x++)
            {
                CardInfo activeCard = MasterList[x];
                if (activeCard.Class == "Digimon")
                {
                    if (activeCard.xeffect != "None")
                    {
                        cardswithXeffect++;
                        if (activeCard.xeffect.Contains("CIRCLE Counter"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Counter;
                            activeCard.Ceffect.Effect_Mark = "CIRCLE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("TRIANGLE Counter"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Counter;
                            activeCard.Ceffect.Effect_Mark = "TRIANGLE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("CROSS Counter"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Counter;
                            activeCard.Ceffect.Effect_Mark = "CROSS";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("FIRE foe x3"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Foe_XAttack;
                            activeCard.Ceffect.Effect_Mark = "FIRE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("ICE foe x3"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Foe_XAttack;
                            activeCard.Ceffect.Effect_Mark = "ICE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("NATURE foe x3"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Foe_XAttack;
                            activeCard.Ceffect.Effect_Mark = "NATURE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("DARK foe x3"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Foe_XAttack;
                            activeCard.Ceffect.Effect_Mark = "DARK";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("RARE foe x3"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Foe_XAttack;
                            activeCard.Ceffect.Effect_Mark = "RARE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("Eat-Up HP"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.EatUp;
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("CIRCLE to 0"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Attack_ToZero;
                            activeCard.Ceffect.Effect_Mark = "CIRCLE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("TRIANGLE to 0"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Attack_ToZero;
                            activeCard.Ceffect.Effect_Mark = "TRIANGLE";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("CROSS to 0"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Attack_ToZero;
                            activeCard.Ceffect.Effect_Mark = "CROSS";
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("1st Attack"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.FirstAttack;
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("Crash"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Crash;
                            cardsAddedXeffect++;
                        }
                        if (activeCard.xeffect.Contains("Jamming"))
                        {
                            activeCard.Ceffect.Effect = X_Effect.Jamming;
                            cardsAddedXeffect++;
                        }
                    }
                }
            }

            txtSupport.Text = "Cards with  x effecs: " + cardswithXeffect + "\r\nCards with effects added: " + cardsAddedXeffect;

            for (int x = 0; x < MasterList.Count; x++)
            {
                CardInfo activeCard = MasterList[x];
                if (activeCard.Class == "Digimon")
                {
                    if (activeCard.xeffect != ("None"))
                    {
                        if (activeCard.Ceffect.Effect == X_Effect.None)
                        {
                            throw new Exception("Card missing x effect: " + activeCard.id + " - " + activeCard.name);
                        }
                    }
                }
            }
            */
            #endregion
        }

        private List<CardInfo> MasterList;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            CardInfo newCard = new CardInfo();
            //Add data to it
            newCard.id = Convert.ToInt32(txtID.Text);
            newCard.name = txtName.Text;
            newCard.Class = txtClass.Text;
            newCard.level = txtLevel.Text;
            newCard.type = txtType.Text;
            newCard.hp = Convert.ToInt32(txtHp.Text);
            newCard.dp = Convert.ToInt32(txtDP.Text);
            newCard.plusp = Convert.ToInt32(txtPlusP.Text);
            newCard.circle = Convert.ToInt32(txtCircle.Text);
            newCard.triangle = Convert.ToInt32(txtTriangle.Text);
            newCard.cross = Convert.ToInt32(txtCross.Text);
            newCard.xeffect = txtXEffect.Text;
            newCard.xpriority = txtXPriority.Text;
            newCard.priority = txtPriority.Text;
            newCard.supporteffect = txtSupport.Text;
            //Add it to the list
            MasterList.Add(newCard);


            //write the Json
            string output = JsonConvert.SerializeObject(MasterList);
            File.WriteAllText(Directory.GetCurrentDirectory() + "\\Json\\CardDB.json", output);

            //Update list
            listMainList.Items.Add(newCard.id.ToString() + " - " + newCard.name.ToString());
        }

        private void listMainList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listMainList.SelectedIndex;

            CardInfo CardData = MasterList[index];

            txtID.Text = CardData.id.ToString();
            txtName.Text = CardData.name;
            txtClass.Text = CardData.Class;
            txtLevel.Text = CardData.level;
            txtType.Text = CardData.type;
            txtHp.Text = CardData.hp.ToString();
            txtDP.Text = CardData.dp.ToString();
            txtPlusP.Text = CardData.plusp.ToString();
            txtCircle.Text = CardData.circle.ToString();
            txtTriangle.Text = CardData.triangle.ToString();
            txtCross.Text = CardData.cross.ToString();
            txtXEffect.Text = CardData.xeffect;
            txtXPriority.Text = CardData.xpriority;
            txtPriority.Text = CardData.priority;
            txtSupport.Text = CardData.supporteffect;

            //Effects
            listCrossEffect.SelectedIndex = (int)CardData.Ceffect.Effect;
            txtXEffectMark.Text = CardData.Ceffect.Effect_Mark;

            listSupEffectCode.SelectedIndex = (int)CardData.effect1.Effect;
            txtSupEffectMark.Text = CardData.effect1.Effect_Mark;
            txtSupEffectAmount.Text = CardData.effect1.Effect_Amount.ToString();

            listSupEffectCode2.SelectedIndex = (int)CardData.effect2.Effect;
            txtSupEffectMark2.Text = CardData.effect2.Effect_Mark;
            txtSupEffectAmount2.Text = CardData.effect2.Effect_Amount.ToString();

            listSupEffectCode3.SelectedIndex = (int)CardData.effect3.Effect;
            txtSupEffectMark3.Text = CardData.effect3.Effect_Mark;
            txtSupEffectAmount3.Text = CardData.effect3.Effect_Amount.ToString();

            //Conditions
            listConditions.SelectedIndex = (int)CardData.condition.condition;
            txtConditionMark.Text = CardData.condition.condition_Mark;
            txtConditionAmount.Text = CardData.condition.condition_Amount.ToString();

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            int index = listMainList.SelectedIndex;

            MasterList[index].id = Convert.ToInt32(txtID.Text);
            MasterList[index].name = txtName.Text;
            MasterList[index].Class = txtClass.Text;
            MasterList[index].level = txtLevel.Text;
            MasterList[index].type = txtType.Text;
            MasterList[index].hp = Convert.ToInt32(txtHp.Text);
            MasterList[index].dp = Convert.ToInt32(txtDP.Text);
            MasterList[index].plusp = Convert.ToInt32(txtPlusP.Text);
            MasterList[index].circle = Convert.ToInt32(txtCircle.Text);
            MasterList[index].triangle = Convert.ToInt32(txtTriangle.Text);
            MasterList[index].cross = Convert.ToInt32(txtCross.Text);
            MasterList[index].xeffect = txtXEffect.Text;
            MasterList[index].xpriority = txtXPriority.Text;
            MasterList[index].priority = txtPriority.Text;
            MasterList[index].supporteffect = txtSupport.Text;

            //update effects
            MasterList[index].Ceffect.Effect = (X_Effect)listCrossEffect.SelectedIndex;
            MasterList[index].Ceffect.Effect_Mark = txtXEffectMark.Text;

            MasterList[index].effect1.Effect = (S_Effect)listSupEffectCode.SelectedIndex;
            MasterList[index].effect1.Effect_Mark = txtSupEffectMark.Text;
            MasterList[index].effect1.Effect_Amount = Convert.ToInt32(txtSupEffectAmount.Text);

            MasterList[index].effect2.Effect = (S_Effect)listSupEffectCode2.SelectedIndex;
            MasterList[index].effect2.Effect_Mark = txtSupEffectMark2.Text;
            MasterList[index].effect2.Effect_Amount = Convert.ToInt32(txtSupEffectAmount2.Text);

            MasterList[index].effect3.Effect = (S_Effect)listSupEffectCode3.SelectedIndex;
            MasterList[index].effect3.Effect_Mark = txtSupEffectMark3.Text;
            MasterList[index].effect3.Effect_Amount = Convert.ToInt32(txtSupEffectAmount3.Text);

            //Update Condition
            MasterList[index].condition.condition = (Condition)listConditions.SelectedIndex;
            MasterList[index].condition.condition_Mark = txtConditionMark.Text;
            MasterList[index].condition.condition_Amount = Convert.ToInt32(txtConditionAmount.Text);


            //write the Json
            string output = JsonConvert.SerializeObject(MasterList);
            File.WriteAllText(Directory.GetCurrentDirectory() + "\\Json\\CardDB.json", output);

        }
    }
}
