﻿namespace Digimon_Card_Game
{
    partial class BattleBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattleBoard));
            this.PanelOponentHand = new System.Windows.Forms.Panel();
            this.PanelRedDiscardBack = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.PicRedDiscardBlinking = new System.Windows.Forms.PictureBox();
            this.PanelRedDeckBack = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.PicRedDeckBlinking = new System.Windows.Forms.PictureBox();
            this.lblRedDeckCount = new System.Windows.Forms.Label();
            this.lblRedDiscardCount = new System.Windows.Forms.Label();
            this.lblREDName = new System.Windows.Forms.Label();
            this.lblREDDeckName = new System.Windows.Forms.Label();
            this.PicOponentGreen3 = new System.Windows.Forms.PictureBox();
            this.PicOponentGreen1 = new System.Windows.Forms.PictureBox();
            this.PicOponentGreen2 = new System.Windows.Forms.PictureBox();
            this.PanelPlayerHand = new System.Windows.Forms.Panel();
            this.PanelBlueDiscardBack = new System.Windows.Forms.Panel();
            this.PicBlueDiscardBack = new System.Windows.Forms.PictureBox();
            this.PicBlueDiscardBlinking = new System.Windows.Forms.PictureBox();
            this.PanelBlueDeckBack = new System.Windows.Forms.Panel();
            this.PicBlueDeckBack = new System.Windows.Forms.PictureBox();
            this.PicBlueDeckBlinking = new System.Windows.Forms.PictureBox();
            this.lblBlueDeckCount = new System.Windows.Forms.Label();
            this.lblBlueDiscardCount = new System.Windows.Forms.Label();
            this.lblBlueName = new System.Windows.Forms.Label();
            this.lblBlueDeckName = new System.Windows.Forms.Label();
            this.PicPlayerGreen3 = new System.Windows.Forms.PictureBox();
            this.PicPlayerGreen2 = new System.Windows.Forms.PictureBox();
            this.PicPlayerGreen1 = new System.Windows.Forms.PictureBox();
            this.PanelPlayerActive = new System.Windows.Forms.Panel();
            this.PicBlueDPMark8 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark7 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark6 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark5 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark4 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark3 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark2 = new System.Windows.Forms.PictureBox();
            this.PicBlueDPMark1 = new System.Windows.Forms.PictureBox();
            this.lblBlueActiveHP = new System.Windows.Forms.Label();
            this.lblBlueActiveCrossEffect = new System.Windows.Forms.Label();
            this.lblBlueDP = new System.Windows.Forms.Label();
            this.lblBlueActiveCross = new System.Windows.Forms.Label();
            this.lblBlueActiveTriangle = new System.Windows.Forms.Label();
            this.lblBlueActiveCircle = new System.Windows.Forms.Label();
            this.lblBlueActiveName = new System.Windows.Forms.Label();
            this.PanelOponentActive = new System.Windows.Forms.Panel();
            this.PicRedDPMark8 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark7 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark6 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark5 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark4 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark3 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark2 = new System.Windows.Forms.PictureBox();
            this.PicRedDPMark1 = new System.Windows.Forms.PictureBox();
            this.lblRedActiveHP = new System.Windows.Forms.Label();
            this.lblRedActiveCrossEffect = new System.Windows.Forms.Label();
            this.lblRedDP = new System.Windows.Forms.Label();
            this.lblRedActiveCross = new System.Windows.Forms.Label();
            this.lblRedActiveName = new System.Windows.Forms.Label();
            this.lblRedActiveTriangle = new System.Windows.Forms.Label();
            this.lblRedActiveCircle = new System.Windows.Forms.Label();
            this.PanelTurnDecider = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PanelCardOption2 = new System.Windows.Forms.Panel();
            this.PicOptionCard2 = new System.Windows.Forms.PictureBox();
            this.PanelCardOption1 = new System.Windows.Forms.Panel();
            this.PicOptionCard1 = new System.Windows.Forms.PictureBox();
            this.PanelUpperMessage = new System.Windows.Forms.Panel();
            this.lblUpperMessage = new System.Windows.Forms.Label();
            this.PanelLowerMessage = new System.Windows.Forms.Panel();
            this.lblLowerMessage = new System.Windows.Forms.Label();
            this.PicFirstAttackMark = new System.Windows.Forms.PictureBox();
            this.PanelPhaseDisplay = new System.Windows.Forms.Panel();
            this.PanelCentralMessage = new System.Windows.Forms.Panel();
            this.lblQuestionNo = new System.Windows.Forms.Label();
            this.lblQuestionYes = new System.Windows.Forms.Label();
            this.lblCentralMessage = new System.Windows.Forms.Label();
            this.PanelCardViewer = new System.Windows.Forms.Panel();
            this.lblCardViewerType = new System.Windows.Forms.Label();
            this.PicCardViewerLevel = new System.Windows.Forms.PictureBox();
            this.PicCardViewerCrossPriority = new System.Windows.Forms.PictureBox();
            this.PicCardViewerSupPriority = new System.Windows.Forms.PictureBox();
            this.lblCardViewerCrossEffect = new System.Windows.Forms.Label();
            this.lblCardViewerPplus = new System.Windows.Forms.Label();
            this.lblCardViewerDP = new System.Windows.Forms.Label();
            this.lblCardViewerHp = new System.Windows.Forms.Label();
            this.lblCardViewerCross = new System.Windows.Forms.Label();
            this.lblCardViewerTriangle = new System.Windows.Forms.Label();
            this.lblCardViewerCircle = new System.Windows.Forms.Label();
            this.lblCardViewerSupport = new System.Windows.Forms.Label();
            this.lblCardViewerName = new System.Windows.Forms.Label();
            this.PicCardViewerCard = new System.Windows.Forms.PictureBox();
            this.PicStatus1 = new System.Windows.Forms.PictureBox();
            this.PicStatus2 = new System.Windows.Forms.PictureBox();
            this.PicStatus3 = new System.Windows.Forms.PictureBox();
            this.PicStatus4 = new System.Windows.Forms.PictureBox();
            this.lblBlueDigivolve = new System.Windows.Forms.Label();
            this.lblRedDigivolve = new System.Windows.Forms.Label();
            this.PanelBlueAttackSelector = new System.Windows.Forms.Panel();
            this.lblATASelectorBlueTurnMark = new System.Windows.Forms.Label();
            this.lblATASelectorBlueType = new System.Windows.Forms.Label();
            this.PicATASelectorBlueLevel = new System.Windows.Forms.PictureBox();
            this.PicATASelectorBlueCrossPriority = new System.Windows.Forms.PictureBox();
            this.lblATASelectorBlueCrossEffect = new System.Windows.Forms.Label();
            this.lblATASelectorBlueHp = new System.Windows.Forms.Label();
            this.lblATASelectorBlueCross = new System.Windows.Forms.Label();
            this.lblATASelectorBlueTriangle = new System.Windows.Forms.Label();
            this.lblATASelectorBlueCircle = new System.Windows.Forms.Label();
            this.lblATASelectorBlueCrossName = new System.Windows.Forms.Label();
            this.lblATASelectorBlueTriangleName = new System.Windows.Forms.Label();
            this.lblATASelectorBlueCircleName = new System.Windows.Forms.Label();
            this.lblATASelectorBlueDigimonName = new System.Windows.Forms.Label();
            this.PanelRedAttackSelector = new System.Windows.Forms.Panel();
            this.lblATASelectorRedTurnMark = new System.Windows.Forms.Label();
            this.lblATASelectorRedType = new System.Windows.Forms.Label();
            this.PicATASelectorRedLevel = new System.Windows.Forms.PictureBox();
            this.PicATASelectorRedCrossPriority = new System.Windows.Forms.PictureBox();
            this.lblATASelectorRedCrossEffect = new System.Windows.Forms.Label();
            this.lblATASelectorRedHp = new System.Windows.Forms.Label();
            this.lblATASelectorRedCross = new System.Windows.Forms.Label();
            this.lblATASelectorRedTriangle = new System.Windows.Forms.Label();
            this.lblATASelectorRedCircle = new System.Windows.Forms.Label();
            this.lblATASelectorRedCrossName = new System.Windows.Forms.Label();
            this.lblATASelectorRedTriangleName = new System.Windows.Forms.Label();
            this.lblATASelectorRedCircleName = new System.Windows.Forms.Label();
            this.lblATASelectorRedDigimonName = new System.Windows.Forms.Label();
            this.PanelDebugInfoWindow = new System.Windows.Forms.Panel();
            this.lbldebugBlueHandSize = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbldebugIsBlueFirstAttack = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbldebugCurrentCardPointer = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbldebugCurrentPhase = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbldebugCurrentGameState = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDebugtitle = new System.Windows.Forms.Label();
            this.PicStatus5 = new System.Windows.Forms.PictureBox();
            this.PicRedSupportSlot = new System.Windows.Forms.PictureBox();
            this.PicBlueSupportSlot = new System.Windows.Forms.PictureBox();
            this.PanelOponentHand.SuspendLayout();
            this.PanelRedDiscardBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDiscardBlinking)).BeginInit();
            this.PanelRedDeckBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDeckBlinking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOponentGreen3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOponentGreen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOponentGreen2)).BeginInit();
            this.PanelPlayerHand.SuspendLayout();
            this.PanelBlueDiscardBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDiscardBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDiscardBlinking)).BeginInit();
            this.PanelBlueDeckBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDeckBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDeckBlinking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPlayerGreen3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPlayerGreen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPlayerGreen1)).BeginInit();
            this.PanelPlayerActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark1)).BeginInit();
            this.PanelOponentActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark1)).BeginInit();
            this.PanelTurnDecider.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PanelCardOption2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicOptionCard2)).BeginInit();
            this.PanelCardOption1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicOptionCard1)).BeginInit();
            this.PanelUpperMessage.SuspendLayout();
            this.PanelLowerMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicFirstAttackMark)).BeginInit();
            this.PanelCentralMessage.SuspendLayout();
            this.PanelCardViewer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerCrossPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerSupPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus4)).BeginInit();
            this.PanelBlueAttackSelector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorBlueLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorBlueCrossPriority)).BeginInit();
            this.PanelRedAttackSelector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorRedLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorRedCrossPriority)).BeginInit();
            this.PanelDebugInfoWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedSupportSlot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueSupportSlot)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelOponentHand
            // 
            this.PanelOponentHand.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelOponentHand.BackgroundImage")));
            this.PanelOponentHand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelOponentHand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelOponentHand.Controls.Add(this.PanelRedDiscardBack);
            this.PanelOponentHand.Controls.Add(this.PanelRedDeckBack);
            this.PanelOponentHand.Controls.Add(this.lblRedDeckCount);
            this.PanelOponentHand.Controls.Add(this.lblRedDiscardCount);
            this.PanelOponentHand.Controls.Add(this.lblREDName);
            this.PanelOponentHand.Controls.Add(this.lblREDDeckName);
            this.PanelOponentHand.Controls.Add(this.PicOponentGreen3);
            this.PanelOponentHand.Controls.Add(this.PicOponentGreen1);
            this.PanelOponentHand.Controls.Add(this.PicOponentGreen2);
            this.PanelOponentHand.Location = new System.Drawing.Point(87, 26);
            this.PanelOponentHand.Name = "PanelOponentHand";
            this.PanelOponentHand.Size = new System.Drawing.Size(517, 133);
            this.PanelOponentHand.TabIndex = 0;
            // 
            // PanelRedDiscardBack
            // 
            this.PanelRedDiscardBack.BackColor = System.Drawing.Color.Black;
            this.PanelRedDiscardBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRedDiscardBack.Controls.Add(this.pictureBox4);
            this.PanelRedDiscardBack.Controls.Add(this.PicRedDiscardBlinking);
            this.PanelRedDiscardBack.Location = new System.Drawing.Point(433, 38);
            this.PanelRedDiscardBack.Name = "PanelRedDiscardBack";
            this.PanelRedDiscardBack.Size = new System.Drawing.Size(47, 37);
            this.PanelRedDiscardBack.TabIndex = 15;
            this.PanelRedDiscardBack.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(2, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(42, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // PicRedDiscardBlinking
            // 
            this.PicRedDiscardBlinking.Image = ((System.Drawing.Image)(resources.GetObject("PicRedDiscardBlinking.Image")));
            this.PicRedDiscardBlinking.Location = new System.Drawing.Point(0, 0);
            this.PicRedDiscardBlinking.Name = "PicRedDiscardBlinking";
            this.PicRedDiscardBlinking.Size = new System.Drawing.Size(50, 40);
            this.PicRedDiscardBlinking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRedDiscardBlinking.TabIndex = 14;
            this.PicRedDiscardBlinking.TabStop = false;
            this.PicRedDiscardBlinking.Visible = false;
            // 
            // PanelRedDeckBack
            // 
            this.PanelRedDeckBack.BackColor = System.Drawing.Color.Black;
            this.PanelRedDeckBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRedDeckBack.Controls.Add(this.pictureBox3);
            this.PanelRedDeckBack.Controls.Add(this.PicRedDeckBlinking);
            this.PanelRedDeckBack.Location = new System.Drawing.Point(432, 84);
            this.PanelRedDeckBack.Name = "PanelRedDeckBack";
            this.PanelRedDeckBack.Size = new System.Drawing.Size(47, 37);
            this.PanelRedDeckBack.TabIndex = 14;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(2, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(42, 32);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // PicRedDeckBlinking
            // 
            this.PicRedDeckBlinking.Image = ((System.Drawing.Image)(resources.GetObject("PicRedDeckBlinking.Image")));
            this.PicRedDeckBlinking.Location = new System.Drawing.Point(0, 0);
            this.PicRedDeckBlinking.Name = "PicRedDeckBlinking";
            this.PicRedDeckBlinking.Size = new System.Drawing.Size(50, 40);
            this.PicRedDeckBlinking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRedDeckBlinking.TabIndex = 16;
            this.PicRedDeckBlinking.TabStop = false;
            this.PicRedDeckBlinking.Visible = false;
            // 
            // lblRedDeckCount
            // 
            this.lblRedDeckCount.BackColor = System.Drawing.Color.Transparent;
            this.lblRedDeckCount.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedDeckCount.ForeColor = System.Drawing.Color.White;
            this.lblRedDeckCount.Location = new System.Drawing.Point(481, 91);
            this.lblRedDeckCount.Name = "lblRedDeckCount";
            this.lblRedDeckCount.Size = new System.Drawing.Size(26, 20);
            this.lblRedDeckCount.TabIndex = 10;
            this.lblRedDeckCount.Text = "30";
            this.lblRedDeckCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRedDiscardCount
            // 
            this.lblRedDiscardCount.BackColor = System.Drawing.Color.Transparent;
            this.lblRedDiscardCount.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedDiscardCount.ForeColor = System.Drawing.Color.White;
            this.lblRedDiscardCount.Location = new System.Drawing.Point(482, 45);
            this.lblRedDiscardCount.Name = "lblRedDiscardCount";
            this.lblRedDiscardCount.Size = new System.Drawing.Size(26, 20);
            this.lblRedDiscardCount.TabIndex = 9;
            this.lblRedDiscardCount.Text = "0";
            this.lblRedDiscardCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblREDName
            // 
            this.lblREDName.BackColor = System.Drawing.Color.Transparent;
            this.lblREDName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblREDName.ForeColor = System.Drawing.Color.White;
            this.lblREDName.Location = new System.Drawing.Point(255, 2);
            this.lblREDName.Name = "lblREDName";
            this.lblREDName.Size = new System.Drawing.Size(235, 20);
            this.lblREDName.TabIndex = 7;
            this.lblREDName.Text = "This is the Red Player Name";
            // 
            // lblREDDeckName
            // 
            this.lblREDDeckName.BackColor = System.Drawing.Color.Transparent;
            this.lblREDDeckName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblREDDeckName.ForeColor = System.Drawing.Color.White;
            this.lblREDDeckName.Location = new System.Drawing.Point(4, 2);
            this.lblREDDeckName.Name = "lblREDDeckName";
            this.lblREDDeckName.Size = new System.Drawing.Size(235, 20);
            this.lblREDDeckName.TabIndex = 6;
            this.lblREDDeckName.Text = "This is the Red Deck Name";
            // 
            // PicOponentGreen3
            // 
            this.PicOponentGreen3.Image = ((System.Drawing.Image)(resources.GetObject("PicOponentGreen3.Image")));
            this.PicOponentGreen3.Location = new System.Drawing.Point(7, 98);
            this.PicOponentGreen3.Name = "PicOponentGreen3";
            this.PicOponentGreen3.Size = new System.Drawing.Size(71, 24);
            this.PicOponentGreen3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOponentGreen3.TabIndex = 5;
            this.PicOponentGreen3.TabStop = false;
            this.PicOponentGreen3.Visible = false;
            // 
            // PicOponentGreen1
            // 
            this.PicOponentGreen1.Image = ((System.Drawing.Image)(resources.GetObject("PicOponentGreen1.Image")));
            this.PicOponentGreen1.Location = new System.Drawing.Point(7, 38);
            this.PicOponentGreen1.Name = "PicOponentGreen1";
            this.PicOponentGreen1.Size = new System.Drawing.Size(71, 24);
            this.PicOponentGreen1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOponentGreen1.TabIndex = 3;
            this.PicOponentGreen1.TabStop = false;
            this.PicOponentGreen1.Visible = false;
            // 
            // PicOponentGreen2
            // 
            this.PicOponentGreen2.Image = ((System.Drawing.Image)(resources.GetObject("PicOponentGreen2.Image")));
            this.PicOponentGreen2.Location = new System.Drawing.Point(7, 68);
            this.PicOponentGreen2.Name = "PicOponentGreen2";
            this.PicOponentGreen2.Size = new System.Drawing.Size(71, 24);
            this.PicOponentGreen2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOponentGreen2.TabIndex = 4;
            this.PicOponentGreen2.TabStop = false;
            this.PicOponentGreen2.Visible = false;
            // 
            // PanelPlayerHand
            // 
            this.PanelPlayerHand.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelPlayerHand.BackgroundImage")));
            this.PanelPlayerHand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelPlayerHand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPlayerHand.Controls.Add(this.PanelBlueDiscardBack);
            this.PanelPlayerHand.Controls.Add(this.PanelBlueDeckBack);
            this.PanelPlayerHand.Controls.Add(this.lblBlueDeckCount);
            this.PanelPlayerHand.Controls.Add(this.lblBlueDiscardCount);
            this.PanelPlayerHand.Controls.Add(this.lblBlueName);
            this.PanelPlayerHand.Controls.Add(this.lblBlueDeckName);
            this.PanelPlayerHand.Controls.Add(this.PicPlayerGreen3);
            this.PanelPlayerHand.Controls.Add(this.PicPlayerGreen2);
            this.PanelPlayerHand.Controls.Add(this.PicPlayerGreen1);
            this.PanelPlayerHand.Location = new System.Drawing.Point(82, 281);
            this.PanelPlayerHand.Name = "PanelPlayerHand";
            this.PanelPlayerHand.Size = new System.Drawing.Size(517, 133);
            this.PanelPlayerHand.TabIndex = 1;
            // 
            // PanelBlueDiscardBack
            // 
            this.PanelBlueDiscardBack.BackColor = System.Drawing.Color.Black;
            this.PanelBlueDiscardBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelBlueDiscardBack.Controls.Add(this.PicBlueDiscardBack);
            this.PanelBlueDiscardBack.Controls.Add(this.PicBlueDiscardBlinking);
            this.PanelBlueDiscardBack.Location = new System.Drawing.Point(38, 11);
            this.PanelBlueDiscardBack.Name = "PanelBlueDiscardBack";
            this.PanelBlueDiscardBack.Size = new System.Drawing.Size(47, 37);
            this.PanelBlueDiscardBack.TabIndex = 13;
            this.PanelBlueDiscardBack.Visible = false;
            // 
            // PicBlueDiscardBack
            // 
            this.PicBlueDiscardBack.Image = ((System.Drawing.Image)(resources.GetObject("PicBlueDiscardBack.Image")));
            this.PicBlueDiscardBack.Location = new System.Drawing.Point(2, 2);
            this.PicBlueDiscardBack.Name = "PicBlueDiscardBack";
            this.PicBlueDiscardBack.Size = new System.Drawing.Size(42, 32);
            this.PicBlueDiscardBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBlueDiscardBack.TabIndex = 13;
            this.PicBlueDiscardBack.TabStop = false;
            // 
            // PicBlueDiscardBlinking
            // 
            this.PicBlueDiscardBlinking.Image = ((System.Drawing.Image)(resources.GetObject("PicBlueDiscardBlinking.Image")));
            this.PicBlueDiscardBlinking.Location = new System.Drawing.Point(0, 0);
            this.PicBlueDiscardBlinking.Name = "PicBlueDiscardBlinking";
            this.PicBlueDiscardBlinking.Size = new System.Drawing.Size(50, 40);
            this.PicBlueDiscardBlinking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBlueDiscardBlinking.TabIndex = 15;
            this.PicBlueDiscardBlinking.TabStop = false;
            this.PicBlueDiscardBlinking.Visible = false;
            // 
            // PanelBlueDeckBack
            // 
            this.PanelBlueDeckBack.BackColor = System.Drawing.Color.Black;
            this.PanelBlueDeckBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelBlueDeckBack.Controls.Add(this.PicBlueDeckBack);
            this.PanelBlueDeckBack.Controls.Add(this.PicBlueDeckBlinking);
            this.PanelBlueDeckBack.Location = new System.Drawing.Point(36, 59);
            this.PanelBlueDeckBack.Name = "PanelBlueDeckBack";
            this.PanelBlueDeckBack.Size = new System.Drawing.Size(47, 37);
            this.PanelBlueDeckBack.TabIndex = 13;
            // 
            // PicBlueDeckBack
            // 
            this.PicBlueDeckBack.Image = ((System.Drawing.Image)(resources.GetObject("PicBlueDeckBack.Image")));
            this.PicBlueDeckBack.Location = new System.Drawing.Point(2, 2);
            this.PicBlueDeckBack.Name = "PicBlueDeckBack";
            this.PicBlueDeckBack.Size = new System.Drawing.Size(42, 32);
            this.PicBlueDeckBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBlueDeckBack.TabIndex = 12;
            this.PicBlueDeckBack.TabStop = false;
            // 
            // PicBlueDeckBlinking
            // 
            this.PicBlueDeckBlinking.Image = ((System.Drawing.Image)(resources.GetObject("PicBlueDeckBlinking.Image")));
            this.PicBlueDeckBlinking.Location = new System.Drawing.Point(0, 0);
            this.PicBlueDeckBlinking.Name = "PicBlueDeckBlinking";
            this.PicBlueDeckBlinking.Size = new System.Drawing.Size(50, 40);
            this.PicBlueDeckBlinking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBlueDeckBlinking.TabIndex = 13;
            this.PicBlueDeckBlinking.TabStop = false;
            this.PicBlueDeckBlinking.Visible = false;
            // 
            // lblBlueDeckCount
            // 
            this.lblBlueDeckCount.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueDeckCount.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueDeckCount.ForeColor = System.Drawing.Color.White;
            this.lblBlueDeckCount.Location = new System.Drawing.Point(6, 64);
            this.lblBlueDeckCount.Name = "lblBlueDeckCount";
            this.lblBlueDeckCount.Size = new System.Drawing.Size(26, 20);
            this.lblBlueDeckCount.TabIndex = 11;
            this.lblBlueDeckCount.Text = "30";
            this.lblBlueDeckCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBlueDiscardCount
            // 
            this.lblBlueDiscardCount.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueDiscardCount.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueDiscardCount.ForeColor = System.Drawing.Color.White;
            this.lblBlueDiscardCount.Location = new System.Drawing.Point(5, 19);
            this.lblBlueDiscardCount.Name = "lblBlueDiscardCount";
            this.lblBlueDiscardCount.Size = new System.Drawing.Size(26, 20);
            this.lblBlueDiscardCount.TabIndex = 8;
            this.lblBlueDiscardCount.Text = "0";
            this.lblBlueDiscardCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBlueName
            // 
            this.lblBlueName.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueName.ForeColor = System.Drawing.Color.White;
            this.lblBlueName.Location = new System.Drawing.Point(263, 105);
            this.lblBlueName.Name = "lblBlueName";
            this.lblBlueName.Size = new System.Drawing.Size(235, 20);
            this.lblBlueName.TabIndex = 8;
            this.lblBlueName.Text = "This is the Blue Player Name";
            // 
            // lblBlueDeckName
            // 
            this.lblBlueDeckName.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueDeckName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueDeckName.ForeColor = System.Drawing.Color.White;
            this.lblBlueDeckName.Location = new System.Drawing.Point(4, 104);
            this.lblBlueDeckName.Name = "lblBlueDeckName";
            this.lblBlueDeckName.Size = new System.Drawing.Size(235, 20);
            this.lblBlueDeckName.TabIndex = 7;
            this.lblBlueDeckName.Text = "This is the Blue Deck Name";
            // 
            // PicPlayerGreen3
            // 
            this.PicPlayerGreen3.Image = ((System.Drawing.Image)(resources.GetObject("PicPlayerGreen3.Image")));
            this.PicPlayerGreen3.Location = new System.Drawing.Point(438, 71);
            this.PicPlayerGreen3.Name = "PicPlayerGreen3";
            this.PicPlayerGreen3.Size = new System.Drawing.Size(71, 24);
            this.PicPlayerGreen3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPlayerGreen3.TabIndex = 2;
            this.PicPlayerGreen3.TabStop = false;
            this.PicPlayerGreen3.Visible = false;
            // 
            // PicPlayerGreen2
            // 
            this.PicPlayerGreen2.Image = ((System.Drawing.Image)(resources.GetObject("PicPlayerGreen2.Image")));
            this.PicPlayerGreen2.Location = new System.Drawing.Point(438, 41);
            this.PicPlayerGreen2.Name = "PicPlayerGreen2";
            this.PicPlayerGreen2.Size = new System.Drawing.Size(71, 24);
            this.PicPlayerGreen2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPlayerGreen2.TabIndex = 1;
            this.PicPlayerGreen2.TabStop = false;
            this.PicPlayerGreen2.Visible = false;
            // 
            // PicPlayerGreen1
            // 
            this.PicPlayerGreen1.Image = ((System.Drawing.Image)(resources.GetObject("PicPlayerGreen1.Image")));
            this.PicPlayerGreen1.Location = new System.Drawing.Point(438, 12);
            this.PicPlayerGreen1.Name = "PicPlayerGreen1";
            this.PicPlayerGreen1.Size = new System.Drawing.Size(71, 24);
            this.PicPlayerGreen1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPlayerGreen1.TabIndex = 0;
            this.PicPlayerGreen1.TabStop = false;
            this.PicPlayerGreen1.Visible = false;
            // 
            // PanelPlayerActive
            // 
            this.PanelPlayerActive.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelPlayerActive.BackgroundImage")));
            this.PanelPlayerActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelPlayerActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark8);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark7);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark6);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark5);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark4);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark3);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark2);
            this.PanelPlayerActive.Controls.Add(this.PicBlueDPMark1);
            this.PanelPlayerActive.Controls.Add(this.lblBlueActiveHP);
            this.PanelPlayerActive.Controls.Add(this.lblBlueActiveCrossEffect);
            this.PanelPlayerActive.Controls.Add(this.lblBlueDP);
            this.PanelPlayerActive.Controls.Add(this.lblBlueActiveCross);
            this.PanelPlayerActive.Controls.Add(this.lblBlueActiveTriangle);
            this.PanelPlayerActive.Controls.Add(this.lblBlueActiveCircle);
            this.PanelPlayerActive.Controls.Add(this.lblBlueActiveName);
            this.PanelPlayerActive.Location = new System.Drawing.Point(130, 160);
            this.PanelPlayerActive.Name = "PanelPlayerActive";
            this.PanelPlayerActive.Size = new System.Drawing.Size(212, 118);
            this.PanelPlayerActive.TabIndex = 2;
            // 
            // PicBlueDPMark8
            // 
            this.PicBlueDPMark8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark8.Location = new System.Drawing.Point(36, 105);
            this.PicBlueDPMark8.Name = "PicBlueDPMark8";
            this.PicBlueDPMark8.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark8.TabIndex = 24;
            this.PicBlueDPMark8.TabStop = false;
            this.PicBlueDPMark8.Visible = false;
            // 
            // PicBlueDPMark7
            // 
            this.PicBlueDPMark7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark7.Location = new System.Drawing.Point(26, 105);
            this.PicBlueDPMark7.Name = "PicBlueDPMark7";
            this.PicBlueDPMark7.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark7.TabIndex = 23;
            this.PicBlueDPMark7.TabStop = false;
            this.PicBlueDPMark7.Visible = false;
            // 
            // PicBlueDPMark6
            // 
            this.PicBlueDPMark6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark6.Location = new System.Drawing.Point(16, 105);
            this.PicBlueDPMark6.Name = "PicBlueDPMark6";
            this.PicBlueDPMark6.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark6.TabIndex = 22;
            this.PicBlueDPMark6.TabStop = false;
            this.PicBlueDPMark6.Visible = false;
            // 
            // PicBlueDPMark5
            // 
            this.PicBlueDPMark5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark5.Location = new System.Drawing.Point(6, 105);
            this.PicBlueDPMark5.Name = "PicBlueDPMark5";
            this.PicBlueDPMark5.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark5.TabIndex = 21;
            this.PicBlueDPMark5.TabStop = false;
            this.PicBlueDPMark5.Visible = false;
            // 
            // PicBlueDPMark4
            // 
            this.PicBlueDPMark4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark4.Location = new System.Drawing.Point(36, 97);
            this.PicBlueDPMark4.Name = "PicBlueDPMark4";
            this.PicBlueDPMark4.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark4.TabIndex = 20;
            this.PicBlueDPMark4.TabStop = false;
            this.PicBlueDPMark4.Visible = false;
            // 
            // PicBlueDPMark3
            // 
            this.PicBlueDPMark3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark3.Location = new System.Drawing.Point(26, 97);
            this.PicBlueDPMark3.Name = "PicBlueDPMark3";
            this.PicBlueDPMark3.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark3.TabIndex = 19;
            this.PicBlueDPMark3.TabStop = false;
            this.PicBlueDPMark3.Visible = false;
            // 
            // PicBlueDPMark2
            // 
            this.PicBlueDPMark2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark2.Location = new System.Drawing.Point(16, 97);
            this.PicBlueDPMark2.Name = "PicBlueDPMark2";
            this.PicBlueDPMark2.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark2.TabIndex = 18;
            this.PicBlueDPMark2.TabStop = false;
            this.PicBlueDPMark2.Visible = false;
            // 
            // PicBlueDPMark1
            // 
            this.PicBlueDPMark1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBlueDPMark1.Location = new System.Drawing.Point(6, 97);
            this.PicBlueDPMark1.Name = "PicBlueDPMark1";
            this.PicBlueDPMark1.Size = new System.Drawing.Size(7, 5);
            this.PicBlueDPMark1.TabIndex = 17;
            this.PicBlueDPMark1.TabStop = false;
            this.PicBlueDPMark1.Visible = false;
            // 
            // lblBlueActiveHP
            // 
            this.lblBlueActiveHP.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueActiveHP.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueActiveHP.ForeColor = System.Drawing.Color.White;
            this.lblBlueActiveHP.Location = new System.Drawing.Point(140, 0);
            this.lblBlueActiveHP.Name = "lblBlueActiveHP";
            this.lblBlueActiveHP.Size = new System.Drawing.Size(74, 20);
            this.lblBlueActiveHP.TabIndex = 17;
            this.lblBlueActiveHP.Text = "HP 9999";
            this.lblBlueActiveHP.Visible = false;
            // 
            // lblBlueActiveCrossEffect
            // 
            this.lblBlueActiveCrossEffect.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueActiveCrossEffect.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueActiveCrossEffect.ForeColor = System.Drawing.Color.White;
            this.lblBlueActiveCrossEffect.Location = new System.Drawing.Point(54, 97);
            this.lblBlueActiveCrossEffect.Name = "lblBlueActiveCrossEffect";
            this.lblBlueActiveCrossEffect.Size = new System.Drawing.Size(70, 14);
            this.lblBlueActiveCrossEffect.TabIndex = 16;
            this.lblBlueActiveCrossEffect.Text = "9999";
            this.lblBlueActiveCrossEffect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBlueActiveCrossEffect.Visible = false;
            // 
            // lblBlueDP
            // 
            this.lblBlueDP.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueDP.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueDP.ForeColor = System.Drawing.Color.White;
            this.lblBlueDP.Location = new System.Drawing.Point(4, 23);
            this.lblBlueDP.Name = "lblBlueDP";
            this.lblBlueDP.Size = new System.Drawing.Size(43, 25);
            this.lblBlueDP.TabIndex = 15;
            this.lblBlueDP.Text = "0";
            this.lblBlueDP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBlueActiveCross
            // 
            this.lblBlueActiveCross.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueActiveCross.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueActiveCross.ForeColor = System.Drawing.Color.White;
            this.lblBlueActiveCross.Location = new System.Drawing.Point(68, 72);
            this.lblBlueActiveCross.Name = "lblBlueActiveCross";
            this.lblBlueActiveCross.Size = new System.Drawing.Size(65, 25);
            this.lblBlueActiveCross.TabIndex = 14;
            this.lblBlueActiveCross.Text = "9999";
            this.lblBlueActiveCross.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBlueActiveCross.Visible = false;
            // 
            // lblBlueActiveTriangle
            // 
            this.lblBlueActiveTriangle.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueActiveTriangle.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueActiveTriangle.ForeColor = System.Drawing.Color.White;
            this.lblBlueActiveTriangle.Location = new System.Drawing.Point(68, 47);
            this.lblBlueActiveTriangle.Name = "lblBlueActiveTriangle";
            this.lblBlueActiveTriangle.Size = new System.Drawing.Size(65, 25);
            this.lblBlueActiveTriangle.TabIndex = 13;
            this.lblBlueActiveTriangle.Text = "9999";
            this.lblBlueActiveTriangle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBlueActiveTriangle.Visible = false;
            // 
            // lblBlueActiveCircle
            // 
            this.lblBlueActiveCircle.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueActiveCircle.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueActiveCircle.ForeColor = System.Drawing.Color.White;
            this.lblBlueActiveCircle.Location = new System.Drawing.Point(68, 23);
            this.lblBlueActiveCircle.Name = "lblBlueActiveCircle";
            this.lblBlueActiveCircle.Size = new System.Drawing.Size(65, 25);
            this.lblBlueActiveCircle.TabIndex = 12;
            this.lblBlueActiveCircle.Text = "9999";
            this.lblBlueActiveCircle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBlueActiveCircle.Visible = false;
            // 
            // lblBlueActiveName
            // 
            this.lblBlueActiveName.BackColor = System.Drawing.Color.Transparent;
            this.lblBlueActiveName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueActiveName.ForeColor = System.Drawing.Color.White;
            this.lblBlueActiveName.Location = new System.Drawing.Point(39, 1);
            this.lblBlueActiveName.Name = "lblBlueActiveName";
            this.lblBlueActiveName.Size = new System.Drawing.Size(98, 20);
            this.lblBlueActiveName.TabIndex = 11;
            this.lblBlueActiveName.Text = "Digimon Name 1";
            this.lblBlueActiveName.Visible = false;
            // 
            // PanelOponentActive
            // 
            this.PanelOponentActive.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelOponentActive.BackgroundImage")));
            this.PanelOponentActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelOponentActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark8);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark7);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark6);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark5);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark4);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark3);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark2);
            this.PanelOponentActive.Controls.Add(this.PicRedDPMark1);
            this.PanelOponentActive.Controls.Add(this.lblRedActiveHP);
            this.PanelOponentActive.Controls.Add(this.lblRedActiveCrossEffect);
            this.PanelOponentActive.Controls.Add(this.lblRedDP);
            this.PanelOponentActive.Controls.Add(this.lblRedActiveCross);
            this.PanelOponentActive.Controls.Add(this.lblRedActiveName);
            this.PanelOponentActive.Controls.Add(this.lblRedActiveTriangle);
            this.PanelOponentActive.Controls.Add(this.lblRedActiveCircle);
            this.PanelOponentActive.Location = new System.Drawing.Point(346, 160);
            this.PanelOponentActive.Name = "PanelOponentActive";
            this.PanelOponentActive.Size = new System.Drawing.Size(212, 118);
            this.PanelOponentActive.TabIndex = 3;
            // 
            // PicRedDPMark8
            // 
            this.PicRedDPMark8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark8.Location = new System.Drawing.Point(197, 105);
            this.PicRedDPMark8.Name = "PicRedDPMark8";
            this.PicRedDPMark8.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark8.TabIndex = 32;
            this.PicRedDPMark8.TabStop = false;
            this.PicRedDPMark8.Visible = false;
            // 
            // PicRedDPMark7
            // 
            this.PicRedDPMark7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark7.Location = new System.Drawing.Point(187, 105);
            this.PicRedDPMark7.Name = "PicRedDPMark7";
            this.PicRedDPMark7.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark7.TabIndex = 31;
            this.PicRedDPMark7.TabStop = false;
            this.PicRedDPMark7.Visible = false;
            // 
            // PicRedDPMark6
            // 
            this.PicRedDPMark6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark6.Location = new System.Drawing.Point(177, 105);
            this.PicRedDPMark6.Name = "PicRedDPMark6";
            this.PicRedDPMark6.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark6.TabIndex = 30;
            this.PicRedDPMark6.TabStop = false;
            this.PicRedDPMark6.Visible = false;
            // 
            // PicRedDPMark5
            // 
            this.PicRedDPMark5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark5.Location = new System.Drawing.Point(167, 105);
            this.PicRedDPMark5.Name = "PicRedDPMark5";
            this.PicRedDPMark5.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark5.TabIndex = 29;
            this.PicRedDPMark5.TabStop = false;
            this.PicRedDPMark5.Visible = false;
            // 
            // PicRedDPMark4
            // 
            this.PicRedDPMark4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark4.Location = new System.Drawing.Point(197, 97);
            this.PicRedDPMark4.Name = "PicRedDPMark4";
            this.PicRedDPMark4.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark4.TabIndex = 28;
            this.PicRedDPMark4.TabStop = false;
            this.PicRedDPMark4.Visible = false;
            // 
            // PicRedDPMark3
            // 
            this.PicRedDPMark3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark3.Location = new System.Drawing.Point(187, 97);
            this.PicRedDPMark3.Name = "PicRedDPMark3";
            this.PicRedDPMark3.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark3.TabIndex = 27;
            this.PicRedDPMark3.TabStop = false;
            this.PicRedDPMark3.Visible = false;
            // 
            // PicRedDPMark2
            // 
            this.PicRedDPMark2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark2.Location = new System.Drawing.Point(177, 97);
            this.PicRedDPMark2.Name = "PicRedDPMark2";
            this.PicRedDPMark2.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark2.TabIndex = 26;
            this.PicRedDPMark2.TabStop = false;
            this.PicRedDPMark2.Visible = false;
            // 
            // PicRedDPMark1
            // 
            this.PicRedDPMark1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicRedDPMark1.Location = new System.Drawing.Point(167, 97);
            this.PicRedDPMark1.Name = "PicRedDPMark1";
            this.PicRedDPMark1.Size = new System.Drawing.Size(7, 5);
            this.PicRedDPMark1.TabIndex = 25;
            this.PicRedDPMark1.TabStop = false;
            this.PicRedDPMark1.Visible = false;
            // 
            // lblRedActiveHP
            // 
            this.lblRedActiveHP.BackColor = System.Drawing.Color.Transparent;
            this.lblRedActiveHP.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedActiveHP.ForeColor = System.Drawing.Color.White;
            this.lblRedActiveHP.Location = new System.Drawing.Point(104, 0);
            this.lblRedActiveHP.Name = "lblRedActiveHP";
            this.lblRedActiveHP.Size = new System.Drawing.Size(74, 20);
            this.lblRedActiveHP.TabIndex = 19;
            this.lblRedActiveHP.Text = "HP 9999";
            this.lblRedActiveHP.Visible = false;
            // 
            // lblRedActiveCrossEffect
            // 
            this.lblRedActiveCrossEffect.BackColor = System.Drawing.Color.Transparent;
            this.lblRedActiveCrossEffect.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedActiveCrossEffect.ForeColor = System.Drawing.Color.White;
            this.lblRedActiveCrossEffect.Location = new System.Drawing.Point(86, 97);
            this.lblRedActiveCrossEffect.Name = "lblRedActiveCrossEffect";
            this.lblRedActiveCrossEffect.Size = new System.Drawing.Size(70, 14);
            this.lblRedActiveCrossEffect.TabIndex = 18;
            this.lblRedActiveCrossEffect.Text = "9999";
            this.lblRedActiveCrossEffect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRedActiveCrossEffect.Visible = false;
            // 
            // lblRedDP
            // 
            this.lblRedDP.BackColor = System.Drawing.Color.Transparent;
            this.lblRedDP.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedDP.ForeColor = System.Drawing.Color.White;
            this.lblRedDP.Location = new System.Drawing.Point(165, 23);
            this.lblRedDP.Name = "lblRedDP";
            this.lblRedDP.Size = new System.Drawing.Size(43, 25);
            this.lblRedDP.TabIndex = 16;
            this.lblRedDP.Text = "0";
            this.lblRedDP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRedActiveCross
            // 
            this.lblRedActiveCross.BackColor = System.Drawing.Color.Transparent;
            this.lblRedActiveCross.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedActiveCross.ForeColor = System.Drawing.Color.White;
            this.lblRedActiveCross.Location = new System.Drawing.Point(100, 72);
            this.lblRedActiveCross.Name = "lblRedActiveCross";
            this.lblRedActiveCross.Size = new System.Drawing.Size(65, 25);
            this.lblRedActiveCross.TabIndex = 17;
            this.lblRedActiveCross.Text = "9999";
            this.lblRedActiveCross.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRedActiveCross.Visible = false;
            // 
            // lblRedActiveName
            // 
            this.lblRedActiveName.BackColor = System.Drawing.Color.Transparent;
            this.lblRedActiveName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedActiveName.ForeColor = System.Drawing.Color.White;
            this.lblRedActiveName.Location = new System.Drawing.Point(2, 1);
            this.lblRedActiveName.Name = "lblRedActiveName";
            this.lblRedActiveName.Size = new System.Drawing.Size(98, 20);
            this.lblRedActiveName.TabIndex = 12;
            this.lblRedActiveName.Text = "Digimon Name 1";
            this.lblRedActiveName.Visible = false;
            // 
            // lblRedActiveTriangle
            // 
            this.lblRedActiveTriangle.BackColor = System.Drawing.Color.Transparent;
            this.lblRedActiveTriangle.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedActiveTriangle.ForeColor = System.Drawing.Color.White;
            this.lblRedActiveTriangle.Location = new System.Drawing.Point(100, 47);
            this.lblRedActiveTriangle.Name = "lblRedActiveTriangle";
            this.lblRedActiveTriangle.Size = new System.Drawing.Size(65, 25);
            this.lblRedActiveTriangle.TabIndex = 16;
            this.lblRedActiveTriangle.Text = "9999";
            this.lblRedActiveTriangle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRedActiveTriangle.Visible = false;
            // 
            // lblRedActiveCircle
            // 
            this.lblRedActiveCircle.BackColor = System.Drawing.Color.Transparent;
            this.lblRedActiveCircle.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedActiveCircle.ForeColor = System.Drawing.Color.White;
            this.lblRedActiveCircle.Location = new System.Drawing.Point(100, 23);
            this.lblRedActiveCircle.Name = "lblRedActiveCircle";
            this.lblRedActiveCircle.Size = new System.Drawing.Size(65, 25);
            this.lblRedActiveCircle.TabIndex = 15;
            this.lblRedActiveCircle.Text = "9999";
            this.lblRedActiveCircle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRedActiveCircle.Visible = false;
            // 
            // PanelTurnDecider
            // 
            this.PanelTurnDecider.BackColor = System.Drawing.Color.Transparent;
            this.PanelTurnDecider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTurnDecider.Controls.Add(this.pictureBox1);
            this.PanelTurnDecider.Controls.Add(this.PanelCardOption2);
            this.PanelTurnDecider.Controls.Add(this.PanelCardOption1);
            this.PanelTurnDecider.Location = new System.Drawing.Point(696, 2);
            this.PanelTurnDecider.Name = "PanelTurnDecider";
            this.PanelTurnDecider.Size = new System.Drawing.Size(381, 192);
            this.PanelTurnDecider.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(174, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 39);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // PanelCardOption2
            // 
            this.PanelCardOption2.BackColor = System.Drawing.Color.Transparent;
            this.PanelCardOption2.Controls.Add(this.PicOptionCard2);
            this.PanelCardOption2.Location = new System.Drawing.Point(223, 20);
            this.PanelCardOption2.Name = "PanelCardOption2";
            this.PanelCardOption2.Size = new System.Drawing.Size(133, 160);
            this.PanelCardOption2.TabIndex = 2;
            // 
            // PicOptionCard2
            // 
            this.PicOptionCard2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicOptionCard2.Image = ((System.Drawing.Image)(resources.GetObject("PicOptionCard2.Image")));
            this.PicOptionCard2.Location = new System.Drawing.Point(5, 5);
            this.PicOptionCard2.Name = "PicOptionCard2";
            this.PicOptionCard2.Size = new System.Drawing.Size(121, 149);
            this.PicOptionCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOptionCard2.TabIndex = 0;
            this.PicOptionCard2.TabStop = false;
            // 
            // PanelCardOption1
            // 
            this.PanelCardOption1.BackColor = System.Drawing.Color.Yellow;
            this.PanelCardOption1.Controls.Add(this.PicOptionCard1);
            this.PanelCardOption1.Location = new System.Drawing.Point(21, 20);
            this.PanelCardOption1.Name = "PanelCardOption1";
            this.PanelCardOption1.Size = new System.Drawing.Size(133, 160);
            this.PanelCardOption1.TabIndex = 1;
            // 
            // PicOptionCard1
            // 
            this.PicOptionCard1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicOptionCard1.Image = ((System.Drawing.Image)(resources.GetObject("PicOptionCard1.Image")));
            this.PicOptionCard1.Location = new System.Drawing.Point(5, 5);
            this.PicOptionCard1.Name = "PicOptionCard1";
            this.PicOptionCard1.Size = new System.Drawing.Size(121, 149);
            this.PicOptionCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicOptionCard1.TabIndex = 0;
            this.PicOptionCard1.TabStop = false;
            // 
            // PanelUpperMessage
            // 
            this.PanelUpperMessage.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PanelUpperMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelUpperMessage.Controls.Add(this.lblUpperMessage);
            this.PanelUpperMessage.Location = new System.Drawing.Point(52, 3);
            this.PanelUpperMessage.Name = "PanelUpperMessage";
            this.PanelUpperMessage.Size = new System.Drawing.Size(590, 22);
            this.PanelUpperMessage.TabIndex = 5;
            // 
            // lblUpperMessage
            // 
            this.lblUpperMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblUpperMessage.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpperMessage.ForeColor = System.Drawing.Color.White;
            this.lblUpperMessage.Location = new System.Drawing.Point(3, -1);
            this.lblUpperMessage.Name = "lblUpperMessage";
            this.lblUpperMessage.Size = new System.Drawing.Size(509, 20);
            this.lblUpperMessage.TabIndex = 13;
            this.lblUpperMessage.Text = "Message:";
            // 
            // PanelLowerMessage
            // 
            this.PanelLowerMessage.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PanelLowerMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelLowerMessage.Controls.Add(this.lblLowerMessage);
            this.PanelLowerMessage.Location = new System.Drawing.Point(60, 416);
            this.PanelLowerMessage.Name = "PanelLowerMessage";
            this.PanelLowerMessage.Size = new System.Drawing.Size(590, 22);
            this.PanelLowerMessage.TabIndex = 6;
            // 
            // lblLowerMessage
            // 
            this.lblLowerMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblLowerMessage.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLowerMessage.ForeColor = System.Drawing.Color.White;
            this.lblLowerMessage.Location = new System.Drawing.Point(3, -1);
            this.lblLowerMessage.Name = "lblLowerMessage";
            this.lblLowerMessage.Size = new System.Drawing.Size(540, 20);
            this.lblLowerMessage.TabIndex = 13;
            this.lblLowerMessage.Text = "Message:";
            // 
            // PicFirstAttackMark
            // 
            this.PicFirstAttackMark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicFirstAttackMark.Image = ((System.Drawing.Image)(resources.GetObject("PicFirstAttackMark.Image")));
            this.PicFirstAttackMark.Location = new System.Drawing.Point(602, 294);
            this.PicFirstAttackMark.Name = "PicFirstAttackMark";
            this.PicFirstAttackMark.Size = new System.Drawing.Size(53, 104);
            this.PicFirstAttackMark.TabIndex = 7;
            this.PicFirstAttackMark.TabStop = false;
            this.PicFirstAttackMark.Visible = false;
            // 
            // PanelPhaseDisplay
            // 
            this.PanelPhaseDisplay.BackColor = System.Drawing.Color.Transparent;
            this.PanelPhaseDisplay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelPhaseDisplay.BackgroundImage")));
            this.PanelPhaseDisplay.Location = new System.Drawing.Point(0, 150);
            this.PanelPhaseDisplay.Name = "PanelPhaseDisplay";
            this.PanelPhaseDisplay.Size = new System.Drawing.Size(80, 125);
            this.PanelPhaseDisplay.TabIndex = 9;
            this.PanelPhaseDisplay.Visible = false;
            // 
            // PanelCentralMessage
            // 
            this.PanelCentralMessage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelCentralMessage.BackgroundImage")));
            this.PanelCentralMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelCentralMessage.Controls.Add(this.lblQuestionNo);
            this.PanelCentralMessage.Controls.Add(this.lblQuestionYes);
            this.PanelCentralMessage.Controls.Add(this.lblCentralMessage);
            this.PanelCentralMessage.Location = new System.Drawing.Point(706, 164);
            this.PanelCentralMessage.Name = "PanelCentralMessage";
            this.PanelCentralMessage.Size = new System.Drawing.Size(302, 79);
            this.PanelCentralMessage.TabIndex = 11;
            // 
            // lblQuestionNo
            // 
            this.lblQuestionNo.BackColor = System.Drawing.Color.Transparent;
            this.lblQuestionNo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestionNo.ForeColor = System.Drawing.Color.White;
            this.lblQuestionNo.Location = new System.Drawing.Point(179, 47);
            this.lblQuestionNo.Name = "lblQuestionNo";
            this.lblQuestionNo.Size = new System.Drawing.Size(49, 22);
            this.lblQuestionNo.TabIndex = 15;
            this.lblQuestionNo.Text = "No";
            this.lblQuestionNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblQuestionNo.Visible = false;
            // 
            // lblQuestionYes
            // 
            this.lblQuestionYes.BackColor = System.Drawing.Color.SteelBlue;
            this.lblQuestionYes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuestionYes.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestionYes.ForeColor = System.Drawing.Color.White;
            this.lblQuestionYes.Location = new System.Drawing.Point(88, 47);
            this.lblQuestionYes.Name = "lblQuestionYes";
            this.lblQuestionYes.Size = new System.Drawing.Size(49, 22);
            this.lblQuestionYes.TabIndex = 14;
            this.lblQuestionYes.Text = "Yes";
            this.lblQuestionYes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblQuestionYes.Visible = false;
            // 
            // lblCentralMessage
            // 
            this.lblCentralMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblCentralMessage.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCentralMessage.ForeColor = System.Drawing.Color.White;
            this.lblCentralMessage.Location = new System.Drawing.Point(15, 1);
            this.lblCentralMessage.Name = "lblCentralMessage";
            this.lblCentralMessage.Size = new System.Drawing.Size(266, 51);
            this.lblCentralMessage.TabIndex = 13;
            this.lblCentralMessage.Text = "This is an example message";
            this.lblCentralMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PanelCardViewer
            // 
            this.PanelCardViewer.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelCardViewer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelCardViewer.BackgroundImage")));
            this.PanelCardViewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelCardViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelCardViewer.Controls.Add(this.lblCardViewerType);
            this.PanelCardViewer.Controls.Add(this.PicCardViewerLevel);
            this.PanelCardViewer.Controls.Add(this.PicCardViewerCrossPriority);
            this.PanelCardViewer.Controls.Add(this.PicCardViewerSupPriority);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerCrossEffect);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerPplus);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerDP);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerHp);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerCross);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerTriangle);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerCircle);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerSupport);
            this.PanelCardViewer.Controls.Add(this.lblCardViewerName);
            this.PanelCardViewer.Controls.Add(this.PicCardViewerCard);
            this.PanelCardViewer.Location = new System.Drawing.Point(677, 265);
            this.PanelCardViewer.Name = "PanelCardViewer";
            this.PanelCardViewer.Size = new System.Drawing.Size(577, 165);
            this.PanelCardViewer.TabIndex = 12;
            // 
            // lblCardViewerType
            // 
            this.lblCardViewerType.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerType.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerType.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerType.Location = new System.Drawing.Point(430, 0);
            this.lblCardViewerType.Name = "lblCardViewerType";
            this.lblCardViewerType.Size = new System.Drawing.Size(41, 37);
            this.lblCardViewerType.TabIndex = 29;
            this.lblCardViewerType.Text = "🌼";
            // 
            // PicCardViewerLevel
            // 
            this.PicCardViewerLevel.BackColor = System.Drawing.Color.Transparent;
            this.PicCardViewerLevel.Image = ((System.Drawing.Image)(resources.GetObject("PicCardViewerLevel.Image")));
            this.PicCardViewerLevel.Location = new System.Drawing.Point(475, 4);
            this.PicCardViewerLevel.Name = "PicCardViewerLevel";
            this.PicCardViewerLevel.Size = new System.Drawing.Size(26, 26);
            this.PicCardViewerLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCardViewerLevel.TabIndex = 27;
            this.PicCardViewerLevel.TabStop = false;
            // 
            // PicCardViewerCrossPriority
            // 
            this.PicCardViewerCrossPriority.BackColor = System.Drawing.Color.Transparent;
            this.PicCardViewerCrossPriority.Image = ((System.Drawing.Image)(resources.GetObject("PicCardViewerCrossPriority.Image")));
            this.PicCardViewerCrossPriority.Location = new System.Drawing.Point(262, 137);
            this.PicCardViewerCrossPriority.Name = "PicCardViewerCrossPriority";
            this.PicCardViewerCrossPriority.Size = new System.Drawing.Size(23, 23);
            this.PicCardViewerCrossPriority.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicCardViewerCrossPriority.TabIndex = 26;
            this.PicCardViewerCrossPriority.TabStop = false;
            // 
            // PicCardViewerSupPriority
            // 
            this.PicCardViewerSupPriority.BackColor = System.Drawing.Color.Transparent;
            this.PicCardViewerSupPriority.Image = ((System.Drawing.Image)(resources.GetObject("PicCardViewerSupPriority.Image")));
            this.PicCardViewerSupPriority.Location = new System.Drawing.Point(510, 6);
            this.PicCardViewerSupPriority.Name = "PicCardViewerSupPriority";
            this.PicCardViewerSupPriority.Size = new System.Drawing.Size(25, 25);
            this.PicCardViewerSupPriority.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicCardViewerSupPriority.TabIndex = 25;
            this.PicCardViewerSupPriority.TabStop = false;
            // 
            // lblCardViewerCrossEffect
            // 
            this.lblCardViewerCrossEffect.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerCrossEffect.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerCrossEffect.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerCrossEffect.Location = new System.Drawing.Point(148, 144);
            this.lblCardViewerCrossEffect.Name = "lblCardViewerCrossEffect";
            this.lblCardViewerCrossEffect.Size = new System.Drawing.Size(109, 16);
            this.lblCardViewerCrossEffect.TabIndex = 24;
            this.lblCardViewerCrossEffect.Text = "Cross Effect";
            // 
            // lblCardViewerPplus
            // 
            this.lblCardViewerPplus.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerPplus.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerPplus.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerPplus.Location = new System.Drawing.Point(271, 101);
            this.lblCardViewerPplus.Name = "lblCardViewerPplus";
            this.lblCardViewerPplus.Size = new System.Drawing.Size(38, 25);
            this.lblCardViewerPplus.TabIndex = 23;
            this.lblCardViewerPplus.Text = "00";
            this.lblCardViewerPplus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCardViewerPplus.Visible = false;
            // 
            // lblCardViewerDP
            // 
            this.lblCardViewerDP.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerDP.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerDP.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerDP.Location = new System.Drawing.Point(270, 52);
            this.lblCardViewerDP.Name = "lblCardViewerDP";
            this.lblCardViewerDP.Size = new System.Drawing.Size(38, 25);
            this.lblCardViewerDP.TabIndex = 22;
            this.lblCardViewerDP.Text = "00";
            this.lblCardViewerDP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCardViewerDP.Visible = false;
            // 
            // lblCardViewerHp
            // 
            this.lblCardViewerHp.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerHp.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerHp.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerHp.Location = new System.Drawing.Point(192, 33);
            this.lblCardViewerHp.Name = "lblCardViewerHp";
            this.lblCardViewerHp.Size = new System.Drawing.Size(65, 25);
            this.lblCardViewerHp.TabIndex = 21;
            this.lblCardViewerHp.Text = "9999";
            this.lblCardViewerHp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCardViewerHp.Visible = false;
            // 
            // lblCardViewerCross
            // 
            this.lblCardViewerCross.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerCross.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerCross.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerCross.Location = new System.Drawing.Point(192, 114);
            this.lblCardViewerCross.Name = "lblCardViewerCross";
            this.lblCardViewerCross.Size = new System.Drawing.Size(65, 25);
            this.lblCardViewerCross.TabIndex = 20;
            this.lblCardViewerCross.Text = "9999";
            this.lblCardViewerCross.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCardViewerCross.Visible = false;
            // 
            // lblCardViewerTriangle
            // 
            this.lblCardViewerTriangle.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerTriangle.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerTriangle.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerTriangle.Location = new System.Drawing.Point(192, 87);
            this.lblCardViewerTriangle.Name = "lblCardViewerTriangle";
            this.lblCardViewerTriangle.Size = new System.Drawing.Size(65, 25);
            this.lblCardViewerTriangle.TabIndex = 19;
            this.lblCardViewerTriangle.Text = "9999";
            this.lblCardViewerTriangle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCardViewerTriangle.Visible = false;
            // 
            // lblCardViewerCircle
            // 
            this.lblCardViewerCircle.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerCircle.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerCircle.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerCircle.Location = new System.Drawing.Point(192, 60);
            this.lblCardViewerCircle.Name = "lblCardViewerCircle";
            this.lblCardViewerCircle.Size = new System.Drawing.Size(65, 25);
            this.lblCardViewerCircle.TabIndex = 18;
            this.lblCardViewerCircle.Text = "9999";
            this.lblCardViewerCircle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCardViewerCircle.Visible = false;
            // 
            // lblCardViewerSupport
            // 
            this.lblCardViewerSupport.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerSupport.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerSupport.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerSupport.Location = new System.Drawing.Point(323, 43);
            this.lblCardViewerSupport.Name = "lblCardViewerSupport";
            this.lblCardViewerSupport.Size = new System.Drawing.Size(238, 99);
            this.lblCardViewerSupport.TabIndex = 14;
            this.lblCardViewerSupport.Text = "support effect";
            // 
            // lblCardViewerName
            // 
            this.lblCardViewerName.BackColor = System.Drawing.Color.Transparent;
            this.lblCardViewerName.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardViewerName.ForeColor = System.Drawing.Color.White;
            this.lblCardViewerName.Location = new System.Drawing.Point(154, 6);
            this.lblCardViewerName.Name = "lblCardViewerName";
            this.lblCardViewerName.Size = new System.Drawing.Size(265, 25);
            this.lblCardViewerName.TabIndex = 13;
            this.lblCardViewerName.Text = "Digimon Name 1";
            // 
            // PicCardViewerCard
            // 
            this.PicCardViewerCard.Image = ((System.Drawing.Image)(resources.GetObject("PicCardViewerCard.Image")));
            this.PicCardViewerCard.Location = new System.Drawing.Point(2, 17);
            this.PicCardViewerCard.Name = "PicCardViewerCard";
            this.PicCardViewerCard.Size = new System.Drawing.Size(143, 144);
            this.PicCardViewerCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCardViewerCard.TabIndex = 0;
            this.PicCardViewerCard.TabStop = false;
            // 
            // PicStatus1
            // 
            this.PicStatus1.Image = ((System.Drawing.Image)(resources.GetObject("PicStatus1.Image")));
            this.PicStatus1.Location = new System.Drawing.Point(193, 254);
            this.PicStatus1.Name = "PicStatus1";
            this.PicStatus1.Size = new System.Drawing.Size(53, 35);
            this.PicStatus1.TabIndex = 13;
            this.PicStatus1.TabStop = false;
            this.PicStatus1.Visible = false;
            // 
            // PicStatus2
            // 
            this.PicStatus2.Image = ((System.Drawing.Image)(resources.GetObject("PicStatus2.Image")));
            this.PicStatus2.Location = new System.Drawing.Point(276, 254);
            this.PicStatus2.Name = "PicStatus2";
            this.PicStatus2.Size = new System.Drawing.Size(53, 35);
            this.PicStatus2.TabIndex = 14;
            this.PicStatus2.TabStop = false;
            this.PicStatus2.Visible = false;
            // 
            // PicStatus3
            // 
            this.PicStatus3.Image = ((System.Drawing.Image)(resources.GetObject("PicStatus3.Image")));
            this.PicStatus3.Location = new System.Drawing.Point(360, 254);
            this.PicStatus3.Name = "PicStatus3";
            this.PicStatus3.Size = new System.Drawing.Size(53, 35);
            this.PicStatus3.TabIndex = 15;
            this.PicStatus3.TabStop = false;
            this.PicStatus3.Visible = false;
            // 
            // PicStatus4
            // 
            this.PicStatus4.Image = ((System.Drawing.Image)(resources.GetObject("PicStatus4.Image")));
            this.PicStatus4.Location = new System.Drawing.Point(443, 254);
            this.PicStatus4.Name = "PicStatus4";
            this.PicStatus4.Size = new System.Drawing.Size(53, 35);
            this.PicStatus4.TabIndex = 16;
            this.PicStatus4.TabStop = false;
            this.PicStatus4.Visible = false;
            // 
            // lblBlueDigivolve
            // 
            this.lblBlueDigivolve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBlueDigivolve.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlueDigivolve.Image = ((System.Drawing.Image)(resources.GetObject("lblBlueDigivolve.Image")));
            this.lblBlueDigivolve.Location = new System.Drawing.Point(267, 218);
            this.lblBlueDigivolve.Name = "lblBlueDigivolve";
            this.lblBlueDigivolve.Size = new System.Drawing.Size(75, 26);
            this.lblBlueDigivolve.TabIndex = 17;
            this.lblBlueDigivolve.Text = "Digivolve";
            this.lblBlueDigivolve.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBlueDigivolve.Visible = false;
            // 
            // lblRedDigivolve
            // 
            this.lblRedDigivolve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRedDigivolve.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedDigivolve.Image = ((System.Drawing.Image)(resources.GetObject("lblRedDigivolve.Image")));
            this.lblRedDigivolve.Location = new System.Drawing.Point(348, 218);
            this.lblRedDigivolve.Name = "lblRedDigivolve";
            this.lblRedDigivolve.Size = new System.Drawing.Size(75, 26);
            this.lblRedDigivolve.TabIndex = 25;
            this.lblRedDigivolve.Text = "Digivolve";
            this.lblRedDigivolve.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblRedDigivolve.Visible = false;
            // 
            // PanelBlueAttackSelector
            // 
            this.PanelBlueAttackSelector.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelBlueAttackSelector.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelBlueAttackSelector.BackgroundImage")));
            this.PanelBlueAttackSelector.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelBlueAttackSelector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueTurnMark);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueType);
            this.PanelBlueAttackSelector.Controls.Add(this.PicATASelectorBlueLevel);
            this.PanelBlueAttackSelector.Controls.Add(this.PicATASelectorBlueCrossPriority);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueCrossEffect);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueHp);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueCross);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueTriangle);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueCircle);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueCrossName);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueTriangleName);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueCircleName);
            this.PanelBlueAttackSelector.Controls.Add(this.lblATASelectorBlueDigimonName);
            this.PanelBlueAttackSelector.Location = new System.Drawing.Point(52, 471);
            this.PanelBlueAttackSelector.Name = "PanelBlueAttackSelector";
            this.PanelBlueAttackSelector.Size = new System.Drawing.Size(558, 162);
            this.PanelBlueAttackSelector.TabIndex = 26;
            // 
            // lblATASelectorBlueTurnMark
            // 
            this.lblATASelectorBlueTurnMark.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueTurnMark.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueTurnMark.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueTurnMark.Location = new System.Drawing.Point(444, 130);
            this.lblATASelectorBlueTurnMark.Name = "lblATASelectorBlueTurnMark";
            this.lblATASelectorBlueTurnMark.Size = new System.Drawing.Size(81, 46);
            this.lblATASelectorBlueTurnMark.TabIndex = 32;
            this.lblATASelectorBlueTurnMark.Text = "1st\r\nAttack";
            this.lblATASelectorBlueTurnMark.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblATASelectorBlueType
            // 
            this.lblATASelectorBlueType.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueType.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueType.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueType.Location = new System.Drawing.Point(438, 0);
            this.lblATASelectorBlueType.Name = "lblATASelectorBlueType";
            this.lblATASelectorBlueType.Size = new System.Drawing.Size(41, 37);
            this.lblATASelectorBlueType.TabIndex = 31;
            this.lblATASelectorBlueType.Text = "🌼";
            // 
            // PicATASelectorBlueLevel
            // 
            this.PicATASelectorBlueLevel.BackColor = System.Drawing.Color.Transparent;
            this.PicATASelectorBlueLevel.Image = ((System.Drawing.Image)(resources.GetObject("PicATASelectorBlueLevel.Image")));
            this.PicATASelectorBlueLevel.Location = new System.Drawing.Point(492, 5);
            this.PicATASelectorBlueLevel.Name = "PicATASelectorBlueLevel";
            this.PicATASelectorBlueLevel.Size = new System.Drawing.Size(26, 26);
            this.PicATASelectorBlueLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicATASelectorBlueLevel.TabIndex = 30;
            this.PicATASelectorBlueLevel.TabStop = false;
            // 
            // PicATASelectorBlueCrossPriority
            // 
            this.PicATASelectorBlueCrossPriority.BackColor = System.Drawing.Color.Transparent;
            this.PicATASelectorBlueCrossPriority.Image = ((System.Drawing.Image)(resources.GetObject("PicATASelectorBlueCrossPriority.Image")));
            this.PicATASelectorBlueCrossPriority.Location = new System.Drawing.Point(406, 127);
            this.PicATASelectorBlueCrossPriority.Name = "PicATASelectorBlueCrossPriority";
            this.PicATASelectorBlueCrossPriority.Size = new System.Drawing.Size(28, 28);
            this.PicATASelectorBlueCrossPriority.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicATASelectorBlueCrossPriority.TabIndex = 27;
            this.PicATASelectorBlueCrossPriority.TabStop = false;
            // 
            // lblATASelectorBlueCrossEffect
            // 
            this.lblATASelectorBlueCrossEffect.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueCrossEffect.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueCrossEffect.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueCrossEffect.Location = new System.Drawing.Point(195, 125);
            this.lblATASelectorBlueCrossEffect.Name = "lblATASelectorBlueCrossEffect";
            this.lblATASelectorBlueCrossEffect.Size = new System.Drawing.Size(188, 30);
            this.lblATASelectorBlueCrossEffect.TabIndex = 20;
            this.lblATASelectorBlueCrossEffect.Text = "None";
            this.lblATASelectorBlueCrossEffect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblATASelectorBlueHp
            // 
            this.lblATASelectorBlueHp.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueHp.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueHp.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueHp.Location = new System.Drawing.Point(345, 1);
            this.lblATASelectorBlueHp.Name = "lblATASelectorBlueHp";
            this.lblATASelectorBlueHp.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorBlueHp.TabIndex = 19;
            this.lblATASelectorBlueHp.Text = "9999";
            this.lblATASelectorBlueHp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorBlueCross
            // 
            this.lblATASelectorBlueCross.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueCross.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueCross.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueCross.Location = new System.Drawing.Point(438, 92);
            this.lblATASelectorBlueCross.Name = "lblATASelectorBlueCross";
            this.lblATASelectorBlueCross.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorBlueCross.TabIndex = 18;
            this.lblATASelectorBlueCross.Text = "9999";
            this.lblATASelectorBlueCross.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorBlueTriangle
            // 
            this.lblATASelectorBlueTriangle.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueTriangle.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueTriangle.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueTriangle.Location = new System.Drawing.Point(437, 62);
            this.lblATASelectorBlueTriangle.Name = "lblATASelectorBlueTriangle";
            this.lblATASelectorBlueTriangle.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorBlueTriangle.TabIndex = 17;
            this.lblATASelectorBlueTriangle.Text = "9999";
            this.lblATASelectorBlueTriangle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorBlueCircle
            // 
            this.lblATASelectorBlueCircle.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueCircle.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueCircle.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueCircle.Location = new System.Drawing.Point(437, 31);
            this.lblATASelectorBlueCircle.Name = "lblATASelectorBlueCircle";
            this.lblATASelectorBlueCircle.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorBlueCircle.TabIndex = 16;
            this.lblATASelectorBlueCircle.Text = "9999";
            this.lblATASelectorBlueCircle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorBlueCrossName
            // 
            this.lblATASelectorBlueCrossName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueCrossName.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueCrossName.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueCrossName.Location = new System.Drawing.Point(96, 94);
            this.lblATASelectorBlueCrossName.Name = "lblATASelectorBlueCrossName";
            this.lblATASelectorBlueCrossName.Size = new System.Drawing.Size(302, 29);
            this.lblATASelectorBlueCrossName.TabIndex = 15;
            this.lblATASelectorBlueCrossName.Text = "Special Attack";
            // 
            // lblATASelectorBlueTriangleName
            // 
            this.lblATASelectorBlueTriangleName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueTriangleName.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueTriangleName.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueTriangleName.Location = new System.Drawing.Point(96, 64);
            this.lblATASelectorBlueTriangleName.Name = "lblATASelectorBlueTriangleName";
            this.lblATASelectorBlueTriangleName.Size = new System.Drawing.Size(302, 29);
            this.lblATASelectorBlueTriangleName.TabIndex = 14;
            this.lblATASelectorBlueTriangleName.Text = "Normal Attack!";
            // 
            // lblATASelectorBlueCircleName
            // 
            this.lblATASelectorBlueCircleName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueCircleName.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueCircleName.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorBlueCircleName.Location = new System.Drawing.Point(96, 33);
            this.lblATASelectorBlueCircleName.Name = "lblATASelectorBlueCircleName";
            this.lblATASelectorBlueCircleName.Size = new System.Drawing.Size(302, 29);
            this.lblATASelectorBlueCircleName.TabIndex = 13;
            this.lblATASelectorBlueCircleName.Text = "Deadly Attack!!";
            // 
            // lblATASelectorBlueDigimonName
            // 
            this.lblATASelectorBlueDigimonName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorBlueDigimonName.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorBlueDigimonName.ForeColor = System.Drawing.Color.Yellow;
            this.lblATASelectorBlueDigimonName.Location = new System.Drawing.Point(6, 3);
            this.lblATASelectorBlueDigimonName.Name = "lblATASelectorBlueDigimonName";
            this.lblATASelectorBlueDigimonName.Size = new System.Drawing.Size(306, 29);
            this.lblATASelectorBlueDigimonName.TabIndex = 12;
            this.lblATASelectorBlueDigimonName.Text = "Digimon Name 1";
            this.lblATASelectorBlueDigimonName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PanelRedAttackSelector
            // 
            this.PanelRedAttackSelector.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelRedAttackSelector.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelRedAttackSelector.BackgroundImage")));
            this.PanelRedAttackSelector.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelRedAttackSelector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedTurnMark);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedType);
            this.PanelRedAttackSelector.Controls.Add(this.PicATASelectorRedLevel);
            this.PanelRedAttackSelector.Controls.Add(this.PicATASelectorRedCrossPriority);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedCrossEffect);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedHp);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedCross);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedTriangle);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedCircle);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedCrossName);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedTriangleName);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedCircleName);
            this.PanelRedAttackSelector.Controls.Add(this.lblATASelectorRedDigimonName);
            this.PanelRedAttackSelector.Location = new System.Drawing.Point(696, 465);
            this.PanelRedAttackSelector.Name = "PanelRedAttackSelector";
            this.PanelRedAttackSelector.Size = new System.Drawing.Size(558, 162);
            this.PanelRedAttackSelector.TabIndex = 33;
            // 
            // lblATASelectorRedTurnMark
            // 
            this.lblATASelectorRedTurnMark.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedTurnMark.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedTurnMark.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedTurnMark.Location = new System.Drawing.Point(440, 130);
            this.lblATASelectorRedTurnMark.Name = "lblATASelectorRedTurnMark";
            this.lblATASelectorRedTurnMark.Size = new System.Drawing.Size(81, 40);
            this.lblATASelectorRedTurnMark.TabIndex = 33;
            this.lblATASelectorRedTurnMark.Text = "1st\r\nAttack";
            this.lblATASelectorRedTurnMark.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblATASelectorRedType
            // 
            this.lblATASelectorRedType.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedType.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedType.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedType.Location = new System.Drawing.Point(432, 0);
            this.lblATASelectorRedType.Name = "lblATASelectorRedType";
            this.lblATASelectorRedType.Size = new System.Drawing.Size(41, 37);
            this.lblATASelectorRedType.TabIndex = 31;
            this.lblATASelectorRedType.Text = "🌼";
            // 
            // PicATASelectorRedLevel
            // 
            this.PicATASelectorRedLevel.BackColor = System.Drawing.Color.Transparent;
            this.PicATASelectorRedLevel.Image = ((System.Drawing.Image)(resources.GetObject("PicATASelectorRedLevel.Image")));
            this.PicATASelectorRedLevel.Location = new System.Drawing.Point(489, 5);
            this.PicATASelectorRedLevel.Name = "PicATASelectorRedLevel";
            this.PicATASelectorRedLevel.Size = new System.Drawing.Size(26, 26);
            this.PicATASelectorRedLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicATASelectorRedLevel.TabIndex = 30;
            this.PicATASelectorRedLevel.TabStop = false;
            // 
            // PicATASelectorRedCrossPriority
            // 
            this.PicATASelectorRedCrossPriority.BackColor = System.Drawing.Color.Transparent;
            this.PicATASelectorRedCrossPriority.Image = ((System.Drawing.Image)(resources.GetObject("PicATASelectorRedCrossPriority.Image")));
            this.PicATASelectorRedCrossPriority.Location = new System.Drawing.Point(406, 127);
            this.PicATASelectorRedCrossPriority.Name = "PicATASelectorRedCrossPriority";
            this.PicATASelectorRedCrossPriority.Size = new System.Drawing.Size(28, 28);
            this.PicATASelectorRedCrossPriority.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicATASelectorRedCrossPriority.TabIndex = 27;
            this.PicATASelectorRedCrossPriority.TabStop = false;
            // 
            // lblATASelectorRedCrossEffect
            // 
            this.lblATASelectorRedCrossEffect.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedCrossEffect.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedCrossEffect.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedCrossEffect.Location = new System.Drawing.Point(195, 125);
            this.lblATASelectorRedCrossEffect.Name = "lblATASelectorRedCrossEffect";
            this.lblATASelectorRedCrossEffect.Size = new System.Drawing.Size(188, 30);
            this.lblATASelectorRedCrossEffect.TabIndex = 20;
            this.lblATASelectorRedCrossEffect.Text = "None";
            this.lblATASelectorRedCrossEffect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblATASelectorRedHp
            // 
            this.lblATASelectorRedHp.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedHp.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedHp.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedHp.Location = new System.Drawing.Point(339, 1);
            this.lblATASelectorRedHp.Name = "lblATASelectorRedHp";
            this.lblATASelectorRedHp.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorRedHp.TabIndex = 19;
            this.lblATASelectorRedHp.Text = "9999";
            this.lblATASelectorRedHp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorRedCross
            // 
            this.lblATASelectorRedCross.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedCross.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedCross.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedCross.Location = new System.Drawing.Point(430, 92);
            this.lblATASelectorRedCross.Name = "lblATASelectorRedCross";
            this.lblATASelectorRedCross.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorRedCross.TabIndex = 18;
            this.lblATASelectorRedCross.Text = "9999";
            this.lblATASelectorRedCross.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorRedTriangle
            // 
            this.lblATASelectorRedTriangle.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedTriangle.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedTriangle.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedTriangle.Location = new System.Drawing.Point(429, 62);
            this.lblATASelectorRedTriangle.Name = "lblATASelectorRedTriangle";
            this.lblATASelectorRedTriangle.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorRedTriangle.TabIndex = 17;
            this.lblATASelectorRedTriangle.Text = "9999";
            this.lblATASelectorRedTriangle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorRedCircle
            // 
            this.lblATASelectorRedCircle.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedCircle.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedCircle.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedCircle.Location = new System.Drawing.Point(429, 31);
            this.lblATASelectorRedCircle.Name = "lblATASelectorRedCircle";
            this.lblATASelectorRedCircle.Size = new System.Drawing.Size(81, 30);
            this.lblATASelectorRedCircle.TabIndex = 16;
            this.lblATASelectorRedCircle.Text = "9999";
            this.lblATASelectorRedCircle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblATASelectorRedCrossName
            // 
            this.lblATASelectorRedCrossName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedCrossName.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedCrossName.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedCrossName.Location = new System.Drawing.Point(96, 94);
            this.lblATASelectorRedCrossName.Name = "lblATASelectorRedCrossName";
            this.lblATASelectorRedCrossName.Size = new System.Drawing.Size(302, 29);
            this.lblATASelectorRedCrossName.TabIndex = 15;
            this.lblATASelectorRedCrossName.Text = "Special Attack";
            // 
            // lblATASelectorRedTriangleName
            // 
            this.lblATASelectorRedTriangleName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedTriangleName.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedTriangleName.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedTriangleName.Location = new System.Drawing.Point(96, 64);
            this.lblATASelectorRedTriangleName.Name = "lblATASelectorRedTriangleName";
            this.lblATASelectorRedTriangleName.Size = new System.Drawing.Size(302, 29);
            this.lblATASelectorRedTriangleName.TabIndex = 14;
            this.lblATASelectorRedTriangleName.Text = "Normal Attack!";
            // 
            // lblATASelectorRedCircleName
            // 
            this.lblATASelectorRedCircleName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedCircleName.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedCircleName.ForeColor = System.Drawing.Color.White;
            this.lblATASelectorRedCircleName.Location = new System.Drawing.Point(96, 33);
            this.lblATASelectorRedCircleName.Name = "lblATASelectorRedCircleName";
            this.lblATASelectorRedCircleName.Size = new System.Drawing.Size(302, 29);
            this.lblATASelectorRedCircleName.TabIndex = 13;
            this.lblATASelectorRedCircleName.Text = "Deadly Attack!!";
            // 
            // lblATASelectorRedDigimonName
            // 
            this.lblATASelectorRedDigimonName.BackColor = System.Drawing.Color.Transparent;
            this.lblATASelectorRedDigimonName.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATASelectorRedDigimonName.ForeColor = System.Drawing.Color.Yellow;
            this.lblATASelectorRedDigimonName.Location = new System.Drawing.Point(6, 3);
            this.lblATASelectorRedDigimonName.Name = "lblATASelectorRedDigimonName";
            this.lblATASelectorRedDigimonName.Size = new System.Drawing.Size(306, 29);
            this.lblATASelectorRedDigimonName.TabIndex = 12;
            this.lblATASelectorRedDigimonName.Text = "Digimon Name 1";
            this.lblATASelectorRedDigimonName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PanelDebugInfoWindow
            // 
            this.PanelDebugInfoWindow.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.PanelDebugInfoWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelDebugInfoWindow.Controls.Add(this.lbldebugBlueHandSize);
            this.PanelDebugInfoWindow.Controls.Add(this.label6);
            this.PanelDebugInfoWindow.Controls.Add(this.lbldebugIsBlueFirstAttack);
            this.PanelDebugInfoWindow.Controls.Add(this.label5);
            this.PanelDebugInfoWindow.Controls.Add(this.lbldebugCurrentCardPointer);
            this.PanelDebugInfoWindow.Controls.Add(this.label4);
            this.PanelDebugInfoWindow.Controls.Add(this.lbldebugCurrentPhase);
            this.PanelDebugInfoWindow.Controls.Add(this.label3);
            this.PanelDebugInfoWindow.Controls.Add(this.lbldebugCurrentGameState);
            this.PanelDebugInfoWindow.Controls.Add(this.label1);
            this.PanelDebugInfoWindow.Controls.Add(this.lblDebugtitle);
            this.PanelDebugInfoWindow.Location = new System.Drawing.Point(654, 9);
            this.PanelDebugInfoWindow.Name = "PanelDebugInfoWindow";
            this.PanelDebugInfoWindow.Size = new System.Drawing.Size(211, 428);
            this.PanelDebugInfoWindow.TabIndex = 34;
            // 
            // lbldebugBlueHandSize
            // 
            this.lbldebugBlueHandSize.AutoSize = true;
            this.lbldebugBlueHandSize.BackColor = System.Drawing.Color.Transparent;
            this.lbldebugBlueHandSize.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbldebugBlueHandSize.Location = new System.Drawing.Point(107, 108);
            this.lbldebugBlueHandSize.Name = "lbldebugBlueHandSize";
            this.lbldebugBlueHandSize.Size = new System.Drawing.Size(31, 13);
            this.lbldebugBlueHandSize.TabIndex = 10;
            this.lbldebugBlueHandSize.Text = "none";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(6, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Blue Hand Size:";
            // 
            // lbldebugIsBlueFirstAttack
            // 
            this.lbldebugIsBlueFirstAttack.AutoSize = true;
            this.lbldebugIsBlueFirstAttack.BackColor = System.Drawing.Color.Transparent;
            this.lbldebugIsBlueFirstAttack.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbldebugIsBlueFirstAttack.Location = new System.Drawing.Point(104, 77);
            this.lbldebugIsBlueFirstAttack.Name = "lbldebugIsBlueFirstAttack";
            this.lbldebugIsBlueFirstAttack.Size = new System.Drawing.Size(31, 13);
            this.lbldebugIsBlueFirstAttack.TabIndex = 8;
            this.lbldebugIsBlueFirstAttack.Text = "none";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(3, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "_IsBlueFirstA:";
            // 
            // lbldebugCurrentCardPointer
            // 
            this.lbldebugCurrentCardPointer.AutoSize = true;
            this.lbldebugCurrentCardPointer.BackColor = System.Drawing.Color.Transparent;
            this.lbldebugCurrentCardPointer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbldebugCurrentCardPointer.Location = new System.Drawing.Point(104, 60);
            this.lbldebugCurrentCardPointer.Name = "lbldebugCurrentCardPointer";
            this.lbldebugCurrentCardPointer.Size = new System.Drawing.Size(31, 13);
            this.lbldebugCurrentCardPointer.TabIndex = 6;
            this.lbldebugCurrentCardPointer.Text = "none";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(3, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "_CurrentCardPointer:";
            // 
            // lbldebugCurrentPhase
            // 
            this.lbldebugCurrentPhase.AutoSize = true;
            this.lbldebugCurrentPhase.BackColor = System.Drawing.Color.Transparent;
            this.lbldebugCurrentPhase.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbldebugCurrentPhase.Location = new System.Drawing.Point(104, 43);
            this.lbldebugCurrentPhase.Name = "lbldebugCurrentPhase";
            this.lbldebugCurrentPhase.Size = new System.Drawing.Size(31, 13);
            this.lbldebugCurrentPhase.TabIndex = 4;
            this.lbldebugCurrentPhase.Text = "none";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(3, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "_CurrentPhase:";
            // 
            // lbldebugCurrentGameState
            // 
            this.lbldebugCurrentGameState.AutoSize = true;
            this.lbldebugCurrentGameState.BackColor = System.Drawing.Color.Transparent;
            this.lbldebugCurrentGameState.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbldebugCurrentGameState.Location = new System.Drawing.Point(104, 26);
            this.lbldebugCurrentGameState.Name = "lbldebugCurrentGameState";
            this.lbldebugCurrentGameState.Size = new System.Drawing.Size(31, 13);
            this.lbldebugCurrentGameState.TabIndex = 2;
            this.lbldebugCurrentGameState.Text = "none";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(3, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "_CurrentGameState:";
            // 
            // lblDebugtitle
            // 
            this.lblDebugtitle.AutoSize = true;
            this.lblDebugtitle.BackColor = System.Drawing.Color.Transparent;
            this.lblDebugtitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDebugtitle.Location = new System.Drawing.Point(4, 4);
            this.lblDebugtitle.Name = "lblDebugtitle";
            this.lblDebugtitle.Size = new System.Drawing.Size(65, 13);
            this.lblDebugtitle.TabIndex = 0;
            this.lblDebugtitle.Text = "Debug Data";
            // 
            // PicStatus5
            // 
            this.PicStatus5.Image = ((System.Drawing.Image)(resources.GetObject("PicStatus5.Image")));
            this.PicStatus5.Location = new System.Drawing.Point(116, 305);
            this.PicStatus5.Name = "PicStatus5";
            this.PicStatus5.Size = new System.Drawing.Size(53, 35);
            this.PicStatus5.TabIndex = 35;
            this.PicStatus5.TabStop = false;
            this.PicStatus5.Visible = false;
            // 
            // PicRedSupportSlot
            // 
            this.PicRedSupportSlot.BackColor = System.Drawing.Color.Transparent;
            this.PicRedSupportSlot.Image = ((System.Drawing.Image)(resources.GetObject("PicRedSupportSlot.Image")));
            this.PicRedSupportSlot.Location = new System.Drawing.Point(350, 237);
            this.PicRedSupportSlot.Name = "PicRedSupportSlot";
            this.PicRedSupportSlot.Size = new System.Drawing.Size(88, 42);
            this.PicRedSupportSlot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRedSupportSlot.TabIndex = 36;
            this.PicRedSupportSlot.TabStop = false;
            // 
            // PicBlueSupportSlot
            // 
            this.PicBlueSupportSlot.BackColor = System.Drawing.Color.Transparent;
            this.PicBlueSupportSlot.Image = ((System.Drawing.Image)(resources.GetObject("PicBlueSupportSlot.Image")));
            this.PicBlueSupportSlot.Location = new System.Drawing.Point(252, 166);
            this.PicBlueSupportSlot.Name = "PicBlueSupportSlot";
            this.PicBlueSupportSlot.Size = new System.Drawing.Size(88, 42);
            this.PicBlueSupportSlot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBlueSupportSlot.TabIndex = 37;
            this.PicBlueSupportSlot.TabStop = false;
            // 
            // BattleBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(866, 442);
            this.Controls.Add(this.PicStatus5);
            this.Controls.Add(this.PanelDebugInfoWindow);
            this.Controls.Add(this.PanelRedAttackSelector);
            this.Controls.Add(this.PanelBlueAttackSelector);
            this.Controls.Add(this.lblRedDigivolve);
            this.Controls.Add(this.lblBlueDigivolve);
            this.Controls.Add(this.PanelCentralMessage);
            this.Controls.Add(this.PicStatus4);
            this.Controls.Add(this.PicStatus3);
            this.Controls.Add(this.PicStatus2);
            this.Controls.Add(this.PicStatus1);
            this.Controls.Add(this.PanelCardViewer);
            this.Controls.Add(this.PanelPhaseDisplay);
            this.Controls.Add(this.PicFirstAttackMark);
            this.Controls.Add(this.PanelLowerMessage);
            this.Controls.Add(this.PanelUpperMessage);
            this.Controls.Add(this.PanelTurnDecider);
            this.Controls.Add(this.PanelOponentActive);
            this.Controls.Add(this.PanelPlayerActive);
            this.Controls.Add(this.PanelPlayerHand);
            this.Controls.Add(this.PanelOponentHand);
            this.Controls.Add(this.PicRedSupportSlot);
            this.Controls.Add(this.PicBlueSupportSlot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BattleBoard";
            this.Text = "Digimon Digital Card Game";
            this.PanelOponentHand.ResumeLayout(false);
            this.PanelRedDiscardBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDiscardBlinking)).EndInit();
            this.PanelRedDeckBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDeckBlinking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOponentGreen3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOponentGreen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicOponentGreen2)).EndInit();
            this.PanelPlayerHand.ResumeLayout(false);
            this.PanelBlueDiscardBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDiscardBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDiscardBlinking)).EndInit();
            this.PanelBlueDeckBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDeckBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDeckBlinking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPlayerGreen3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPlayerGreen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPlayerGreen1)).EndInit();
            this.PanelPlayerActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueDPMark1)).EndInit();
            this.PanelOponentActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedDPMark1)).EndInit();
            this.PanelTurnDecider.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PanelCardOption2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicOptionCard2)).EndInit();
            this.PanelCardOption1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicOptionCard1)).EndInit();
            this.PanelUpperMessage.ResumeLayout(false);
            this.PanelLowerMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicFirstAttackMark)).EndInit();
            this.PanelCentralMessage.ResumeLayout(false);
            this.PanelCardViewer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerCrossPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerSupPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCardViewerCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus4)).EndInit();
            this.PanelBlueAttackSelector.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorBlueLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorBlueCrossPriority)).EndInit();
            this.PanelRedAttackSelector.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorRedLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicATASelectorRedCrossPriority)).EndInit();
            this.PanelDebugInfoWindow.ResumeLayout(false);
            this.PanelDebugInfoWindow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicStatus5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRedSupportSlot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBlueSupportSlot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelOponentHand;
        private System.Windows.Forms.Panel PanelPlayerHand;
        private System.Windows.Forms.PictureBox PicPlayerGreen1;
        private System.Windows.Forms.PictureBox PicPlayerGreen3;
        private System.Windows.Forms.PictureBox PicPlayerGreen2;
        private System.Windows.Forms.PictureBox PicOponentGreen3;
        private System.Windows.Forms.PictureBox PicOponentGreen1;
        private System.Windows.Forms.PictureBox PicOponentGreen2;
        private System.Windows.Forms.Panel PanelPlayerActive;
        private System.Windows.Forms.Panel PanelOponentActive;
        private System.Windows.Forms.Label lblREDName;
        private System.Windows.Forms.Label lblREDDeckName;
        private System.Windows.Forms.Label lblBlueName;
        private System.Windows.Forms.Label lblBlueDeckName;
        private System.Windows.Forms.Label lblRedDeckCount;
        private System.Windows.Forms.Label lblRedDiscardCount;
        private System.Windows.Forms.Label lblBlueDeckCount;
        private System.Windows.Forms.Label lblBlueDiscardCount;
        private System.Windows.Forms.Label lblBlueDP;
        private System.Windows.Forms.Label lblBlueActiveCross;
        private System.Windows.Forms.Label lblBlueActiveTriangle;
        private System.Windows.Forms.Label lblBlueActiveCircle;
        private System.Windows.Forms.Label lblBlueActiveName;
        private System.Windows.Forms.Label lblRedDP;
        private System.Windows.Forms.Label lblRedActiveCross;
        private System.Windows.Forms.Label lblRedActiveName;
        private System.Windows.Forms.Label lblRedActiveTriangle;
        private System.Windows.Forms.Label lblRedActiveCircle;
        private System.Windows.Forms.PictureBox PicBlueDeckBack;
        private System.Windows.Forms.Panel PanelTurnDecider;
        private System.Windows.Forms.Panel PanelCardOption2;
        private System.Windows.Forms.PictureBox PicOptionCard2;
        private System.Windows.Forms.Panel PanelCardOption1;
        private System.Windows.Forms.PictureBox PicOptionCard1;
        private System.Windows.Forms.Panel PanelUpperMessage;
        private System.Windows.Forms.Label lblUpperMessage;
        private System.Windows.Forms.Panel PanelLowerMessage;
        private System.Windows.Forms.Label lblLowerMessage;
        private System.Windows.Forms.PictureBox PicFirstAttackMark;
        private System.Windows.Forms.Panel PanelPhaseDisplay;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel PanelCentralMessage;
        private System.Windows.Forms.Label lblCentralMessage;
        private System.Windows.Forms.PictureBox PicBlueDiscardBack;
        private System.Windows.Forms.Panel PanelCardViewer;
        private System.Windows.Forms.Label lblCardViewerType;
        private System.Windows.Forms.PictureBox PicCardViewerLevel;
        private System.Windows.Forms.PictureBox PicCardViewerCrossPriority;
        private System.Windows.Forms.PictureBox PicCardViewerSupPriority;
        private System.Windows.Forms.Label lblCardViewerCrossEffect;
        private System.Windows.Forms.Label lblCardViewerPplus;
        private System.Windows.Forms.Label lblCardViewerDP;
        private System.Windows.Forms.Label lblCardViewerHp;
        private System.Windows.Forms.Label lblCardViewerCross;
        private System.Windows.Forms.Label lblCardViewerTriangle;
        private System.Windows.Forms.Label lblCardViewerCircle;
        private System.Windows.Forms.Label lblCardViewerSupport;
        private System.Windows.Forms.Label lblCardViewerName;
        private System.Windows.Forms.PictureBox PicCardViewerCard;
        private System.Windows.Forms.Panel PanelBlueDeckBack;
        private System.Windows.Forms.Panel PanelRedDeckBack;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel PanelBlueDiscardBack;
        private System.Windows.Forms.Panel PanelRedDiscardBack;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox PicRedDiscardBlinking;
        private System.Windows.Forms.PictureBox PicRedDeckBlinking;
        private System.Windows.Forms.PictureBox PicBlueDiscardBlinking;
        private System.Windows.Forms.PictureBox PicBlueDeckBlinking;
        private System.Windows.Forms.Label lblQuestionNo;
        private System.Windows.Forms.Label lblQuestionYes;
        private System.Windows.Forms.PictureBox PicStatus1;
        private System.Windows.Forms.PictureBox PicStatus2;
        private System.Windows.Forms.PictureBox PicStatus3;
        private System.Windows.Forms.PictureBox PicStatus4;
        private System.Windows.Forms.Label lblBlueActiveCrossEffect;
        private System.Windows.Forms.Label lblRedActiveCrossEffect;
        private System.Windows.Forms.Label lblBlueActiveHP;
        private System.Windows.Forms.Label lblRedActiveHP;
        private System.Windows.Forms.PictureBox PicBlueDPMark8;
        private System.Windows.Forms.PictureBox PicBlueDPMark7;
        private System.Windows.Forms.PictureBox PicBlueDPMark6;
        private System.Windows.Forms.PictureBox PicBlueDPMark5;
        private System.Windows.Forms.PictureBox PicBlueDPMark4;
        private System.Windows.Forms.PictureBox PicBlueDPMark3;
        private System.Windows.Forms.PictureBox PicBlueDPMark2;
        private System.Windows.Forms.PictureBox PicBlueDPMark1;
        private System.Windows.Forms.PictureBox PicRedDPMark8;
        private System.Windows.Forms.PictureBox PicRedDPMark7;
        private System.Windows.Forms.PictureBox PicRedDPMark6;
        private System.Windows.Forms.PictureBox PicRedDPMark5;
        private System.Windows.Forms.PictureBox PicRedDPMark4;
        private System.Windows.Forms.PictureBox PicRedDPMark3;
        private System.Windows.Forms.PictureBox PicRedDPMark2;
        private System.Windows.Forms.PictureBox PicRedDPMark1;
        private System.Windows.Forms.Label lblBlueDigivolve;
        private System.Windows.Forms.Label lblRedDigivolve;
        private System.Windows.Forms.Panel PanelBlueAttackSelector;
        private System.Windows.Forms.Label lblATASelectorBlueCrossEffect;
        private System.Windows.Forms.Label lblATASelectorBlueHp;
        private System.Windows.Forms.Label lblATASelectorBlueCross;
        private System.Windows.Forms.Label lblATASelectorBlueTriangle;
        private System.Windows.Forms.Label lblATASelectorBlueCircle;
        private System.Windows.Forms.Label lblATASelectorBlueCrossName;
        private System.Windows.Forms.Label lblATASelectorBlueTriangleName;
        private System.Windows.Forms.Label lblATASelectorBlueCircleName;
        private System.Windows.Forms.Label lblATASelectorBlueDigimonName;
        private System.Windows.Forms.Label lblATASelectorBlueType;
        private System.Windows.Forms.PictureBox PicATASelectorBlueLevel;
        private System.Windows.Forms.PictureBox PicATASelectorBlueCrossPriority;
        private System.Windows.Forms.Panel PanelRedAttackSelector;
        private System.Windows.Forms.Label lblATASelectorRedType;
        private System.Windows.Forms.PictureBox PicATASelectorRedLevel;
        private System.Windows.Forms.PictureBox PicATASelectorRedCrossPriority;
        private System.Windows.Forms.Label lblATASelectorRedCrossEffect;
        private System.Windows.Forms.Label lblATASelectorRedHp;
        private System.Windows.Forms.Label lblATASelectorRedCross;
        private System.Windows.Forms.Label lblATASelectorRedTriangle;
        private System.Windows.Forms.Label lblATASelectorRedCircle;
        private System.Windows.Forms.Label lblATASelectorRedCrossName;
        private System.Windows.Forms.Label lblATASelectorRedTriangleName;
        private System.Windows.Forms.Label lblATASelectorRedCircleName;
        private System.Windows.Forms.Label lblATASelectorRedDigimonName;
        private System.Windows.Forms.Label lblATASelectorBlueTurnMark;
        private System.Windows.Forms.Label lblATASelectorRedTurnMark;
        private System.Windows.Forms.Panel PanelDebugInfoWindow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDebugtitle;
        private System.Windows.Forms.Label lbldebugCurrentGameState;
        private System.Windows.Forms.Label lbldebugCurrentPhase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbldebugCurrentCardPointer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbldebugIsBlueFirstAttack;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbldebugBlueHandSize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox PicStatus5;
        private System.Windows.Forms.PictureBox PicRedSupportSlot;
        private System.Windows.Forms.PictureBox PicBlueSupportSlot;
    }
}

