﻿//Joel Campos
//4/1/2021
//Hand Class

using System.Drawing;
using System.Collections.Generic;

namespace Digimon_Card_Game
{
    public class Hand
    {
        public Hand(bool BlueHand)
        {
            IsBlue = BlueHand;
            if(IsBlue)
            {
                HandLocation[0] = BlueHand1Loc;
                HandLocation[1] = BlueHand2Loc;
                HandLocation[2] = BlueHand3Loc;
                HandLocation[3] = BlueHand4Loc;
            }
            else
            {
                HandLocation[0] = RedHand1Loc;
                HandLocation[1] = RedHand2Loc;
                HandLocation[2] = RedHand3Loc;
                HandLocation[3] = RedHand4Loc;
            }
        }
        public CardInfo GetCardInfo(int targetIndex)
        {
            return HandSprites[targetIndex].Data;
        }
        public int GetFirstUsedSlot()
        {
            int usedslot = -1;

            for(int x = 0; x < HandSize; x++)
            {
                if (HandSprites[x] != null)
                {
                    usedslot = x;
                    break;
                }
            }

            return usedslot;
        }
        public int GetFirstOpenSlot()
        {
            int index = -1;
            for (int x = 0; x < 4; x++)
            {
                if (HandSprites[x] == null)
                {
                    index = x; break;
                }
            }
            return index;
        }
        public CardSprite GetSprite(int targetIndex)
        {
            return HandSprites[targetIndex];
        }
        public void PointCard(int targetIndex)
        {
            HandSprites[targetIndex].PointCardForViewMode();
        }
        public void UnpointCard(int targetIndex)
        {
            HandSprites[targetIndex].UnPointCardForViewMode();
        }
        public void AddCard(CardSprite card)
        {
            //Set the open slot for the new card
            int openSpot = GetFirstOpenSlot();

            //Add the card sprite
            HandSprites[openSpot] = card;
            HandSize++;

            //Place the card in the open hand slot
            HandSprites[openSpot].SetLocation(HandLocation[openSpot]);
        }
        public void AddCard(CardSprite card, int index)
        {
            //Add the card sprite
            HandSprites[index] = card;
            HandSize++;

            //Place the card in the open hand slot
            HandSprites[index].SetLocation(HandLocation[index]);
        }
        public void RemoveCard(int targetIndex)
        {
            HandSprites[targetIndex] = null;
            HandSize--;
            HandActiveIndex = targetIndex;
        }
        public void ReturnCardToHand(CardSprite card)
        {
            //Set the working index
            int Index = HandActiveIndex;

            //Set the sprite back on the card
            HandSprites[Index] = card;
            HandSize++;

            //Move the Sprit to its hand location
            HandSprites[Index].SetLocation(HandLocation[Index]);

            //Clear the HandActiveIndex
            HandActiveIndex = -1;
        }
        public bool WasDigimonPlaceActiveThisTurn()
        {
            return HandActiveIndex != -1;
        }
        public void ClearActiveIndex()
        {
            HandActiveIndex = -1;
        }
        public bool WasDigimonPlacedAtDPThisTUrn()
        {
            return HandActiveIndex != -1;
        }
        public bool HasNoDigimon()
        {
            bool missing = true;
            for (int x = 0; x < 4; x++)
            {
                if (HandSprites[x] != null)
                {
                    if (HandSprites[x].Class == "Digimon")
                    {
                        missing = false;
                        break;
                    }
                }
            }
            return missing;
        }
        public bool HasThisDigimon(string type, string level, int dp)
        {
            bool found = false;
            for (int x = 0; x < 4; x++)
            {
                if(HandSprites[x] != null)
                {
                    if (HandSprites[x].Type == type && HandSprites[x].Level == level && HandSprites[x].DP == dp)
                    {
                        found = true; break;
                    }
                }
            }
            return found;
        }
        public List<int> GetEvoCandidates(string type, string level, int dp)
        {
            List<int> candidates = new List<int>();
            for (int x = 0; x < 4; x++)
            {
                if (HandSprites[x] != null)
                {
                    if (HandSprites[x].Type == type && HandSprites[x].Level == level && HandSprites[x].DP <= dp)
                    {
                        candidates.Add(x);
                    }
                }
            }
            return candidates;
        }


        public bool ContainsThisSupportEffect(S_Effect effect, string mark)
        {
            bool found = false;
            for (int x = 0; x < 4; x++)
            {
                if(HandSprites[x] != null)
                {
                    if((HandSprites[x].Data.effect1.Effect == effect && HandSprites[x].Data.effect1.Effect_Mark == mark) ||
                        (HandSprites[x].Data.effect2.Effect == effect && HandSprites[x].Data.effect2.Effect_Mark == mark) ||
                        (HandSprites[x].Data.effect3.Effect == effect && HandSprites[x].Data.effect3.Effect_Mark == mark))
                    {
                        found = true;
                        break;
                    }
                }
            }
            return found;
        }

        public List<int> GetSpecificLevelCards(string level)
        {
            List<int> candidates = new List<int>();

            for (int x = 0; x < 4; x++)
            {
                if (HandSprites[x] != null)
                {
                    if (HandSprites[x].Level == level)
                    {
                        candidates.Add(x);
                    }
                }
            }

            return candidates;
        }
        public bool HasDigivolveCard()
        {
            bool found = false;
            for (int x = 0; x < 4; x++)
            {
                if (HandSprites[x] != null)
                {
                    if (HandSprites[x].Class == "Evolution")
                    {
                        found = true;
                        break;
                    }
                }
            }
            return found;
        }

        public int Size { get { return HandSize; } }
        public bool IsEmpty
        {
            get { return HandSize == 0; }
        }
        public int ActiveIndex { get { return HandActiveIndex; } }

        public bool IsSlotOpen(int targetIndex)
        {
            return HandSprites[targetIndex] == null;
        }

        private bool IsBlue = false;
        private CardSprite[] HandSprites = new CardSprite[4];
        private int HandSize = 0;
        private int HandActiveIndex = -1;
        private int HandSupportActiveIndex = -1;

        private Point[] HandLocation = new Point[4];

        private Point BlueHand1Loc = new Point(185, 289);
        private Point BlueHand2Loc = new Point(267, 289);
        private Point BlueHand3Loc = new Point(352, 289);
        private Point BlueHand4Loc = new Point(436, 289);
        private Point RedHand1Loc = new Point(182, 62);
        private Point RedHand2Loc = new Point(267, 62);
        private Point RedHand3Loc = new Point(350, 62);
        private Point RedHand4Loc = new Point(433, 62);
    }
}
