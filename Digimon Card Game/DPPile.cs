﻿//Joel Campos
//DP Pile Class


using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Digimon_Card_Game
{
    public class DPPile
    {
        public DPPile(bool BlueHand, PictureBox[] marks, Label dpLabel)
        {
            IsBlue = BlueHand;
            if (IsBlue)
            {
                PileLocatiion = BlueDPPileLoc;
            }
            else
            {
                PileLocatiion = RedDPPileLoc;
            }

            MarkPictures = marks;
            TotalDP = dpLabel;
        }

        public void AddCard(CardSprite card)
        {
            //Add card
            PileSprites.Add(card);

            //Place the sprite in the pile location
            card.SetLocation(PileLocatiion);

            //Resize the card
            card.DPResize();

            //Update the DP markings
            MarkPictures[PileSprites.Count - 1].Visible = true;

            //Update amount
            TotalAmount += card.PlusP;
            TotalDP.Text = TotalAmount.ToString();
        }
        public CardInfo GetTopCard()
        {
            int index = PileSprites.Count - 1;
            return PileSprites[index].Data;
        }
        public CardSprite GetTopSprite()
        {
            int index = PileSprites.Count - 1;
            return PileSprites[index];
        }
        public void RemoveTopCard()
        {
            //The index will be the last card in the list
            int index = PileSprites.Count - 1;

            //Save the card plus p for later
            int tmpPlusP = PileSprites[index].PlusP;

            //Remove Card
            PileSprites.RemoveAt(index);

            //Update the DP markings
            MarkPictures[index].Visible = false;

            //Update amount
            TotalAmount -= tmpPlusP;
            TotalDP.Text = TotalAmount.ToString();
        }

        public int Count { get { return PileSprites.Count; } }
        public int Amount { get { return TotalAmount; } }

        private int TotalAmount = 0;
        private bool IsBlue = false;
        private List<CardSprite> PileSprites = new List<CardSprite>();
        private PictureBox[] MarkPictures;
        private Label TotalDP;

        private Point PileLocatiion = new Point(0, 0);
        private Point BlueDPPileLoc = new Point(139, 214);
        private Point RedDPPileLoc = new Point(516, 214);
    }
}
