﻿//Joel Campos
//2/28/2021
//Battle Board Form

using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System;

namespace Digimon_Card_Game
{
    public partial class BattleBoard : Form
    {
        #region Constructors
        public BattleBoard()
        {
            InitializeComponent();
            //Initialize database
            string jsonFilePath = Directory.GetCurrentDirectory() + "\\Json\\CardDB.json";
            string rawdata = File.ReadAllText(jsonFilePath);
            CardDB = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CardInfo>>(rawdata);

            //Fillout the decks
            for(int x = 0; x < 30; x ++)
            {
                int cardid = Rand.V(300);
                BlueDeck.Add(CardDB[cardid]);
                cardid = Rand.V(300);
                RedDeck.Add(CardDB[cardid]);
            }

            //BlueDeck[0] = CardDB[100];
            //BlueDeck[1] = CardDB[95];
            //BlueDeck[2] = CardDB[95];
            //BlueDeck[3] = CardDB[79];
            //BlueDeck[0] = CardDB[100];
            //BlueDeck[1] = CardDB[99];
            //BlueDeck[2] = CardDB[268];
            //BlueDeck[3] = CardDB[249];



            //RedDeck[0] = CardDB[246];
            //RedDeck[1] = CardDB[249];
            //RedDeck[2] = CardDB[268];
            //RedDeck[3] = CardDB[297];

            //Initialize DP Piles
            PictureBox[] blueMarks = new PictureBox[8];
            blueMarks[0] = PicBlueDPMark1;
            blueMarks[1] = PicBlueDPMark2;
            blueMarks[2] = PicBlueDPMark3;
            blueMarks[3] = PicBlueDPMark4;
            blueMarks[4] = PicBlueDPMark5;
            blueMarks[5] = PicBlueDPMark6;
            blueMarks[6] = PicBlueDPMark7;
            blueMarks[7] = PicBlueDPMark8;
            PictureBox[] redMarks = new PictureBox[8];
            redMarks[0] = PicRedDPMark1;
            redMarks[1] = PicRedDPMark2;
            redMarks[2] = PicRedDPMark3;
            redMarks[3] = PicRedDPMark4;
            redMarks[4] = PicRedDPMark5;
            redMarks[5] = PicRedDPMark6;
            redMarks[6] = PicRedDPMark7;
            redMarks[7] = PicRedDPMark8;
            BlueDPPile = new DPPile(true, blueMarks,lblBlueDP);
            RedDPPile = new DPPile(false, redMarks, lblRedDP);

            //INitialize status array
            Statuses = new PictureBox[4];
            Statuses[0] = PicStatus1;
            Statuses[1] = PicStatus2;
            Statuses[2] = PicStatus3;
            Statuses[3] = PicStatus4;


            //Set Players' names and Decks
            Red_Name = "Rosemon";
            RedDeck_Name = "Green Deck";
            Blue_Name = "Joel";
            BlueDeck_Name = "Main Deck";

            lblREDName.Text = Red_Name;
            lblREDDeckName.Text = RedDeck_Name;
            lblBlueName.Text = Blue_Name;
            lblBlueDeckName.Text = BlueDeck_Name;

            UpdateUpperMessage("Pre game: First Turn Decider");
            UpdateLowerMessage(SYM_DPAD + ": Change Selection       " + SYM_CROSS + ": Select Card");
            ShowTurnDecider();

            //Update Game State
            _CurrentGameState = GameState.TurnDeciderInView;
        }
        #endregion

        #region Private Member Functions
        /// <summary>
        /// Creates a delay without pausing the UI.
        /// </summary>
        /// <param name="milliseconds">The amount of millisecons to delay.</param>
        private static void WaitNSeconds(double milliseconds)
        {
            if (milliseconds < 1) return;
            DateTime _desired = DateTime.Now.AddMilliseconds(milliseconds);
            while (DateTime.Now < _desired)
            {
                Application.DoEvents();
            }
        }
        private string ParseTextWithSymbols(string msg)
        {
            msg = msg.Replace("FIRE", SYM_FIRE);
            msg = msg.Replace("ICE", SYM_ICE);
            msg = msg.Replace("NATURE", SYM_NATURE);
            msg = msg.Replace("DARK", SYM_DARK);
            msg = msg.Replace("RARE", SYM_RARE);
            msg = msg.Replace("CIRCLE", SYM_CIRCLE);
            msg = msg.Replace("TRIANGLE", SYM_TRIANGLE);
            msg = msg.Replace("CROSS", SYM_CROSS);
            return msg;
        }
        private int GetHandPointerIndex()
        {
            int handIndex = -1;
            switch (_CurrentCardPointer)
            {
                case CardPointerTarget.BlueHand1: handIndex = 0; break;
                case CardPointerTarget.BlueHand2: handIndex = 1; break;
                case CardPointerTarget.BlueHand3: handIndex = 2; break;
                case CardPointerTarget.BlueHand4: handIndex = 3; break;
            }
            return handIndex;
        }
        private int GetLevelModStat(int ogAmount, int div)
        {
            int newAmount = ogAmount;
            newAmount = newAmount / div;

            //Lower the first digit to 0
            newAmount = newAmount / 10;
            newAmount = newAmount * 10;

            return newAmount;
        }
        private void ShowTurnDecider()
        {
            PanelTurnDecider.Visible = true;
            PanelTurnDecider.Location = TurnDeciderPanelLoc;
        }
        private void UpdateUpperMessage(string msg)
        {
            lblUpperMessage.Text = msg;
        }
        private void UpdateLowerMessage(string msg)
        {
            string activePlayer = "PLAYER - ";
            if (_TurnPlayer == Player.Red) { activePlayer = "OPPONENT - "; }
            lblLowerMessage.Text = activePlayer + msg;
        }
        private void PlacePhaseDisplay()
        {
            //Phase color is based on the first attacker flag
            bool bluePhase = _TurnPlayer == Player.Blue;



            //except phase 7 - this has its own flag.
            if(_CurrentPhase == Phase.Phase7_UseSupportCard)
            {
                bluePhase = _isBlueSupportTurn;
            }

            //Upload the phase image
            PanelPhaseDisplay.BackgroundImage = ImageServer.Phase(bluePhase, _CurrentPhase);
            //Start sign at the center
            PanelPhaseDisplay.Location = PhasePanelCenterLoc;
            PanelPhaseDisplay.Visible = true;

            if (bluePhase)
            {
                //Move phase to blue position
                while (PanelPhaseDisplay.Location.Y != BluePhasePanelLoc.Y)
                {
                    if (PanelPhaseDisplay.Location.Y != BluePhasePanelLoc.Y)
                    {
                        PanelPhaseDisplay.Location = new Point(PanelPhaseDisplay.Location.X, PanelPhaseDisplay.Location.Y + 20);
                    }
                }
            }
            else
            {
                //Move phase to red position
                while (PanelPhaseDisplay.Location.Y != RedPhasePanelLoc.Y)
                {
                    if (PanelPhaseDisplay.Location.Y != RedPhasePanelLoc.Y)
                    {
                        PanelPhaseDisplay.Location = new Point(PanelPhaseDisplay.Location.X, PanelPhaseDisplay.Location.Y - 20);
                    }
                }
            }

            //Do a small delay
            WaitNSeconds(1000);
        }
        private void Switch1stAttackMark()
        {
            if(_FirstAttacker == Player.Blue)
            {
                PicFirstAttackMark.Location = Blue1stAttackPanelLoc;
            }
            else
            {
                PicFirstAttackMark.Location = Red1stAttackPanelLoc;
            }
        }
        private void ShowCentralMessage(string msg)
        {
            lblCentralMessage.Text = msg;
            PanelCentralMessage.Location = CentralMessageLoc;
            PanelCentralMessage.BringToFront();
        }
        private void ShowCentralMessageQuestion(string msg)
        {
            //Show message and question selectors
            lblCentralMessage.Text = msg;
            PanelCentralMessage.Location = CentralMessageLoc;
            PanelCentralMessage.BringToFront();
            lblQuestionYes.Visible = true;
            lblQuestionNo.Visible = true;

            //Mark no as default
            lblQuestionNo.BorderStyle = BorderStyle.FixedSingle;
            lblQuestionNo.BackColor = Color.SteelBlue;
            lblQuestionYes.BorderStyle = BorderStyle.None;
            lblQuestionYes.BackColor = Color.Transparent;
            _IsYesSelected = false;
        }
        private void CloseCentralMessage()
        {
            PanelCentralMessage.Location = new Point(1000, 1000);
            lblQuestionYes.Visible = false;
            lblQuestionNo.Visible = false;
        }
        private void EnterCardViewMode()
        {
            //Move the pointer to hand card 1 > 2 > 3 > 4 > Active Digimon
            //Point to the hand
            int FirstCardIndex = BlueHand.GetFirstUsedSlot();
            switch(FirstCardIndex)
            {
                case -1:
                    if (BlueActiveDigimon != null)
                    {
                        _CurrentCardPointer = CardPointerTarget.BlueActive;
                    }
                    break;
                case 0: _CurrentCardPointer = CardPointerTarget.BlueHand1; break;
                case 1: _CurrentCardPointer = CardPointerTarget.BlueHand2; break;
                case 2: _CurrentCardPointer = CardPointerTarget.BlueHand3; break;
                case 3: _CurrentCardPointer = CardPointerTarget.BlueHand4; break;
            }

            //During the UseSupportCard Phase, if the Hand is empty
            //but not the Deck Pile, have the pointer priority go to the deck instead
            //of the Active Digimon card
            //[The Above switch will return Blue Active as the Card Pointer Selection when the 
            //Hand is empty]
            if(_CurrentPhase == Phase.Phase7_UseSupportCard && _CurrentCardPointer == CardPointerTarget.BlueActive)
            {
                _CurrentCardPointer = CardPointerTarget.BlueDeck;
            }

            //Update pointer
            UpdateViewModePointer();
            UpdateCardViewWindowData();

            //Show the CardViewPanel
            PanelCardViewer.BringToFront();
            PanelCardViewer.Location = new Point(1, 1);

            //Update Message
            switch(_CurrentPhase)
            {
                case Phase.Phase1_DrawCards: UpdateLowerMessage(SYM_DPAD + " Select   " + SYM_TRIANGLE + "Return"); break;
                case Phase.Phase2_DigimonEntrance: UpdateLowerMessage(SYM_DPAD + " Select   "+ SYM_CROSS + "OK   " + SYM_TRIANGLE + "Return"); break;
                case Phase.Phase6_ChooseAnAttack: UpdateLowerMessage(SYM_DPAD + " Select   " + SYM_TRIANGLE + "Return"); break;
                case Phase.Phase7_UseSupportCard: UpdateLowerMessage(SYM_DPAD + " Select   " + SYM_CROSS + "OK   " + SYM_CIRCLE + "Cancel"); break;
            }

            //Update state
            _CurrentGameState = GameState.CardViewMode;
        }
        private void UpdateViewModePointer()
        {
            switch (_OldCardPointer)
            {
                case CardPointerTarget.BlueActive: BlueActiveDigimon.UnPointCardForViewMode(); break;
                case CardPointerTarget.RedActive: RedActiveDigimon.UnPointCardForViewMode(); break;
                case CardPointerTarget.BlueDeck: PicBlueDeckBlinking.Visible = false; break;
                case CardPointerTarget.RedDeck: PicRedDeckBlinking.Visible = false; break;
                case CardPointerTarget.BlueHand1: BlueHand.UnpointCard(0); break;
                case CardPointerTarget.BlueHand2: BlueHand.UnpointCard(1); break;
                case CardPointerTarget.BlueHand3: BlueHand.UnpointCard(2); break;
                case CardPointerTarget.BlueHand4: BlueHand.UnpointCard(3); break;
                case CardPointerTarget.RedHand1: RedHand.UnpointCard(0); break;
                case CardPointerTarget.RedHand2: RedHand.UnpointCard(1); break;
                case CardPointerTarget.RedHand3: RedHand.UnpointCard(2); break;
                case CardPointerTarget.RedHand4: RedHand.UnpointCard(3); break;
                case CardPointerTarget.BlueDP: BlueDPPile.GetTopSprite().UnPointCardForViewMode(); break;
                case CardPointerTarget.RedDP: RedDPPile.GetTopSprite().UnPointCardForViewMode(); break;
            }

            switch (_CurrentCardPointer)
            {
                case CardPointerTarget.BlueActive: BlueActiveDigimon.PointCardForViewMode(); break;
                case CardPointerTarget.RedActive: RedActiveDigimon.PointCardForViewMode(); break;
                case CardPointerTarget.BlueDeck: PicBlueDeckBlinking.Visible = true; break;
                case CardPointerTarget.RedDeck: PicRedDeckBlinking.Visible = true; break;
                case CardPointerTarget.BlueHand1: BlueHand.PointCard(0); break;
                case CardPointerTarget.BlueHand2: BlueHand.PointCard(1); break;
                case CardPointerTarget.BlueHand3: BlueHand.PointCard(2); break;
                case CardPointerTarget.BlueHand4: BlueHand.PointCard(3); break;
                case CardPointerTarget.RedHand1: RedHand.PointCard(0); break;
                case CardPointerTarget.RedHand2: RedHand.PointCard(1); break;
                case CardPointerTarget.RedHand3: RedHand.PointCard(2); break;
                case CardPointerTarget.RedHand4: RedHand.PointCard(3); break;
                case CardPointerTarget.BlueDP: BlueDPPile.GetTopSprite().PointCardForViewMode(); break;
                case CardPointerTarget.RedDP: RedDPPile.GetTopSprite().PointCardForViewMode(); break;
            }
        }
        private void UpdateCardViewWindowData()
        {
            CardSprite cardinview = null;
            switch(_CurrentCardPointer)
            {
                case CardPointerTarget.BlueDeck: break;
                case CardPointerTarget.BlueActive: cardinview = BlueActiveDigimon; break;
                case CardPointerTarget.RedActive: cardinview = RedActiveDigimon; break;
                case CardPointerTarget.BlueHand1: cardinview = BlueHand.GetSprite(0); break;
                case CardPointerTarget.BlueHand2: cardinview = BlueHand.GetSprite(1); break;
                case CardPointerTarget.BlueHand3: cardinview = BlueHand.GetSprite(2); break;
                case CardPointerTarget.BlueHand4: cardinview = BlueHand.GetSprite(3); break;
                case CardPointerTarget.RedHand1: cardinview = RedHand.GetSprite(0); break;
                case CardPointerTarget.RedHand2: cardinview = RedHand.GetSprite(1); break;
                case CardPointerTarget.RedHand3: cardinview = RedHand.GetSprite(2); break;
                case CardPointerTarget.RedHand4: cardinview = RedHand.GetSprite(3); break;
                case CardPointerTarget.BlueDP: cardinview = BlueDPPile.GetTopSprite(); break;
                case CardPointerTarget.RedDP: cardinview = RedDPPile.GetTopSprite(); break;
            }

            if(cardinview == null)
            {
                //Show no card
                lblCardViewerName.Visible = false;
                PicCardViewerCard.Image = ImageServer.CardImage(-1);
                PicCardViewerLevel.Visible = false;
                PicCardViewerSupPriority.Visible = false;
                PicCardViewerCrossPriority.Visible = false;
                lblCardViewerType.Visible = false;
                lblCardViewerSupport.Text = "";
                lblCardViewerHp.Visible = false;
                lblCardViewerCircle.Visible = false;
                lblCardViewerTriangle.Visible = false;
                lblCardViewerCross.Visible = false;
                lblCardViewerDP.Visible = false;
                lblCardViewerPplus.Visible = false;
                lblCardViewerCrossEffect.Visible = false;
            }
            else
            {
                //Actually show the card
                lblCardViewerName.Visible = true;
                lblCardViewerName.Text = cardinview.Name;
                PicCardViewerCard.Image = ImageServer.CardImage(cardinview.ID);
                lblCardViewerSupport.Text = ParseTextWithSymbols(cardinview.Support_Effect);
                if (cardinview.Support_Priority != "None")
                {
                    PicCardViewerSupPriority.Visible = true;
                    PicCardViewerSupPriority.Image = ImageServer.Priority(cardinview.Support_Priority);
                }
                else { PicCardViewerSupPriority.Visible = false; }


                if (cardinview.Class == "Digimon") 
                {
                    PicCardViewerLevel.Visible = true;
                    lblCardViewerType.Visible = true;
                    lblCardViewerHp.Visible = true;
                    lblCardViewerCircle.Visible = true;
                    lblCardViewerTriangle.Visible = true;
                    lblCardViewerCross.Visible = true;
                    lblCardViewerDP.Visible = true;
                    lblCardViewerPplus.Visible = true;
                    lblCardViewerCrossEffect.Visible = true;
                    PicCardViewerCrossPriority.Visible = true;

                    PicCardViewerLevel.Image = ImageServer.Level(cardinview.Level);
                    lblCardViewerType.Text = ParseTextWithSymbols(cardinview.Type);                   
                    lblCardViewerDP.Text = cardinview.DP.ToString();
                    lblCardViewerPplus.Text = cardinview.PlusP.ToString();
                    lblCardViewerCrossEffect.Text = ParseTextWithSymbols(cardinview.Cross_Effect);

                    //CARD STATS

                    bool statsNOTDivided = true;

                    //For cards in your hand only
                    int handpointerstatus = GetHandPointerIndex();

                    if(handpointerstatus != -1)
                    {
                        //durund the digimon entrance phase, if pointing a not R level. 
                        //Edit the temporary denominator
                        //During the Digimon Entrance Phase, if a Digimon is C reduce stats 1/4
                        //and U reduce stats 1/4
                        if ((_CurrentPhase == Phase.Phase2_DigimonEntrance && cardinview.Level != "R"))
                        {
                            int div = 1;
                            if (cardinview.Level == "C") { div = 2; }
                            if (cardinview.Level == "U") { div = 4; }

                            lblCardViewerHp.ForeColor = Color.Red;
                            lblCardViewerCircle.ForeColor = Color.Red;
                            lblCardViewerTriangle.ForeColor = Color.Red;
                            lblCardViewerCross.ForeColor = Color.Red;
                            lblCardViewerHp.Text = GetLevelModStat(cardinview.HP, div).ToString();
                            lblCardViewerCircle.Text = GetLevelModStat(cardinview.Circle, div).ToString();
                            lblCardViewerTriangle.Text = GetLevelModStat(cardinview.Triangle, div).ToString();
                            lblCardViewerCross.Text = GetLevelModStat(cardinview.Cross, div).ToString();
                            statsNOTDivided = false;
                        }
                        //if the digimon digivolve phase...
                        else if (_CurrentPhase == Phase.Phase5_DigimonDigivolve)
                        {
                            //if the current denominator is 2 and pointing a U
                            if(BlueActiveStatsDemominator == 2 && cardinview.Level == "U")
                            {
                                //show the U level as divided by 2
                                int div = 2;

                                lblCardViewerHp.ForeColor = Color.Red;
                                lblCardViewerCircle.ForeColor = Color.Red;
                                lblCardViewerTriangle.ForeColor = Color.Red;
                                lblCardViewerCross.ForeColor = Color.Red;
                                lblCardViewerHp.Text = GetLevelModStat(cardinview.HP, div).ToString();
                                lblCardViewerCircle.Text = GetLevelModStat(cardinview.Circle, div).ToString();
                                lblCardViewerTriangle.Text = GetLevelModStat(cardinview.Triangle, div).ToString();
                                lblCardViewerCross.Text = GetLevelModStat(cardinview.Cross, div).ToString();
                                statsNOTDivided = false;
                            }
                        }
                    }


                    //if pointing to the blue active and it current denominator is not 1
                    if (_CurrentCardPointer == CardPointerTarget.BlueActive && BlueActiveStatsDemominator != 1)
                    {
                        lblCardViewerHp.ForeColor = Color.Red;
                        lblCardViewerCircle.ForeColor = Color.Red;
                        lblCardViewerTriangle.ForeColor = Color.Red;
                        lblCardViewerCross.ForeColor = Color.Red;
                        lblCardViewerHp.Text = GetLevelModStat(cardinview.HP, BlueActiveStatsDemominator).ToString();
                        lblCardViewerCircle.Text = GetLevelModStat(cardinview.Circle, BlueActiveStatsDemominator).ToString();
                        lblCardViewerTriangle.Text = GetLevelModStat(cardinview.Triangle, BlueActiveStatsDemominator).ToString();
                        lblCardViewerCross.Text = GetLevelModStat(cardinview.Cross, BlueActiveStatsDemominator).ToString();
                        statsNOTDivided = false;
                    }
                    //same for oponent
                    if (_CurrentCardPointer == CardPointerTarget.RedActive && RedActiveStatsDemominator != 1)
                    {
                        lblCardViewerHp.ForeColor = Color.Red;
                        lblCardViewerCircle.ForeColor = Color.Red;
                        lblCardViewerTriangle.ForeColor = Color.Red;
                        lblCardViewerCross.ForeColor = Color.Red;
                        lblCardViewerHp.Text = GetLevelModStat(cardinview.HP, RedActiveStatsDemominator).ToString();
                        lblCardViewerCircle.Text = GetLevelModStat(cardinview.Circle, RedActiveStatsDemominator).ToString();
                        lblCardViewerTriangle.Text = GetLevelModStat(cardinview.Triangle, RedActiveStatsDemominator).ToString();
                        lblCardViewerCross.Text = GetLevelModStat(cardinview.Cross, RedActiveStatsDemominator).ToString();
                        statsNOTDivided = false;
                    }

                    //If stats were not divided turn it into white and normal
                    if(statsNOTDivided)
                    {
                        lblCardViewerHp.ForeColor = Color.White;
                        lblCardViewerCircle.ForeColor = Color.White;
                        lblCardViewerTriangle.ForeColor = Color.White;
                        lblCardViewerCross.ForeColor = Color.White;
                        lblCardViewerHp.Text = cardinview.HP.ToString();
                        lblCardViewerCircle.Text = cardinview.Circle.ToString();
                        lblCardViewerTriangle.Text = cardinview.Triangle.ToString();
                        lblCardViewerCross.Text = cardinview.Cross.ToString();
                    }

                    if (cardinview.Cross_Priority != "None")
                    {                        
                        PicCardViewerCrossPriority.Visible = true;
                        PicCardViewerCrossPriority.Image = ImageServer.Priority(cardinview.Cross_Priority);
                    }
                    else { PicCardViewerCrossPriority.Visible = false; }
                    
                }
                else 
                {
                    PicCardViewerLevel.Visible = false;
                    PicCardViewerCrossPriority.Visible = false;
                    lblCardViewerType.Visible = false;
                    lblCardViewerHp.Visible = false;
                    lblCardViewerCircle.Visible = false;
                    lblCardViewerTriangle.Visible = false;
                    lblCardViewerCross.Visible = false;
                    lblCardViewerDP.Visible = false;
                    lblCardViewerPplus.Visible = false;
                    lblCardViewerCrossEffect.Visible = false;
                }

            }
        }
        private void UpdateCardViewWindowPosition()
        {
            bool downPostion = false;
            switch(_CurrentCardPointer)
            {
                case CardPointerTarget.RedActive: downPostion = true; break;
                case CardPointerTarget.RedHand1: downPostion = true; break;
                case CardPointerTarget.RedHand2: downPostion = true; break;
                case CardPointerTarget.RedHand3: downPostion = true; break;
                case CardPointerTarget.RedHand4: downPostion = true; break;
                case CardPointerTarget.RedDP: downPostion = true; break;
                case CardPointerTarget.RedDeck: downPostion = true; break;
            }

            if (downPostion)
            {
                PanelCardViewer.Location = new Point(0,275);
            }
            else
            {
                PanelCardViewer.Location = new Point(0, 0);
            }
        }
        private void VanishStatusIcons()
        {
            //Vanish the statuses
            PicStatus1.Visible = false;
            PicStatus2.Visible = false;
            PicStatus3.Visible = false;
            PicStatus4.Visible = false;
            PicStatus5.Visible = false;
        }
        private void InitializeActiveDigimonStats(CardSprite card)
        {
            if (_TurnPlayer == Player.Blue)
            {
                lblBlueActiveName.Text = card.Name;
                //edit the amount based on the active denominator
                lblBlueActiveHP.Text = "HP " + card.Active_HP;
                lblBlueActiveCircle.Text = card.Active_Circle.ToString();
                lblBlueActiveTriangle.Text = card.Active_Triangle.ToString();
                lblBlueActiveCross.Text = card.Active_Cross.ToString();
                lblBlueActiveCrossEffect.Text = ParseTextWithSymbols(card.Cross_Effect);

                //if the demominator is not 1 (meaning C or U were directly set as active without digivolve)
                //turn labels red
                if(BlueActiveStatsDemominator != 1)
                {
                    lblBlueActiveCircle.ForeColor = Color.Red;
                    lblBlueActiveTriangle.ForeColor = Color.Red;
                    lblBlueActiveCross.ForeColor = Color.Red;
                    lblBlueActiveHP.ForeColor = Color.Red;
                }
                else
                {
                    lblBlueActiveCircle.ForeColor = Color.White;
                    lblBlueActiveTriangle.ForeColor = Color.White;
                    lblBlueActiveCross.ForeColor = Color.White;
                    lblBlueActiveHP.ForeColor = Color.White;
                }

                lblBlueActiveName.Visible = true;
                lblBlueActiveCircle.Visible = true;
                lblBlueActiveTriangle.Visible = true;
                lblBlueActiveCross.Visible = true;
                lblBlueActiveCrossEffect.Visible = true;
                lblBlueActiveHP.Visible = true;
            }
            else
            {
                lblRedActiveName.Text = card.Name;
                lblRedActiveHP.Text = "HP " + card.Active_HP;
                lblRedActiveCircle.Text = card.Active_Circle.ToString();
                lblRedActiveTriangle.Text = card.Active_Triangle.ToString();
                lblRedActiveCross.Text = card.Active_Cross.ToString();
                lblRedActiveCrossEffect.Text = ParseTextWithSymbols(card.Cross_Effect);

                //if the demominator is not 1 (meaning C or U were directly set as active without digivolve)
                //turn labels red
                if (RedActiveStatsDemominator != 1)
                {
                    lblRedActiveCircle.ForeColor = Color.Red;
                    lblRedActiveTriangle.ForeColor = Color.Red;
                    lblRedActiveCross.ForeColor = Color.Red;
                    lblRedActiveHP.ForeColor = Color.Red;
                }
                else
                {
                    lblRedActiveCircle.ForeColor = Color.White;
                    lblRedActiveTriangle.ForeColor = Color.White;
                    lblRedActiveCross.ForeColor = Color.White;
                    lblRedActiveHP.ForeColor = Color.White;
                }

                lblRedActiveName.Visible = true;
                lblRedActiveCircle.Visible = true;
                lblRedActiveTriangle.Visible = true;
                lblRedActiveCross.Visible = true;
                lblRedActiveCrossEffect.Visible = true;
                lblRedActiveHP.Visible = true;
            }
        }
        private void ClearActiveDigimonStats()
        {
            if (_TurnPlayer == Player.Blue)
            {
                lblBlueActiveName.Visible = false;
                lblBlueActiveCircle.Visible = false;
                lblBlueActiveTriangle.Visible = false;
                lblBlueActiveCross.Visible = false;
                lblBlueActiveCrossEffect.Visible = false;
                lblBlueActiveHP.Visible = false;
            }
            else
            {
                //TODO: do reds
            }
        }
        private void UpdateNormalDigivolveCandidates()
        {
            if(_TurnPlayer == Player.Blue)
            {
                string digimonType = BlueActiveDigimon.Type;
                string digimonLevel = BlueActiveDigimon.Level;
                int currentDP = BlueDPPile.Amount;
                string nextLevel = "NONE";
                if (digimonLevel == "R") { nextLevel = "C"; }
                else if (digimonLevel == "C") { nextLevel = "U"; }

                DigivolveCandidates = BlueHand.GetEvoCandidates(digimonType, nextLevel, currentDP);
            }
            else
            {
                string digimonType = RedActiveDigimon.Type;
                string digimonLevel = RedActiveDigimon.Level;
                int currentDP = RedDPPile.Amount;
                string nextLevel = "NONE";
                if (digimonLevel == "R") { nextLevel = "C"; }
                else if (digimonLevel == "C") { nextLevel = "U"; }

                DigivolveCandidates = RedHand.GetEvoCandidates(digimonType, nextLevel, currentDP);
            }
        }
        private void UnpointCurrentCard()
        {
            //Unpoint the current card
            switch (_CurrentCardPointer)
            {
                case CardPointerTarget.BlueActive: BlueActiveDigimon.UnPointCardForViewMode(); break;
                case CardPointerTarget.RedActive: RedActiveDigimon.UnPointCardForViewMode(); break;
                case CardPointerTarget.BlueDeck: PicBlueDeckBlinking.Visible = false; break;
                case CardPointerTarget.RedDeck: PicRedDeckBlinking.Visible = false; break;
                case CardPointerTarget.BlueHand1: BlueHand.UnpointCard(0); break;
                case CardPointerTarget.BlueHand2: BlueHand.UnpointCard(1); break;
                case CardPointerTarget.BlueHand3: BlueHand.UnpointCard(2); break;
                case CardPointerTarget.BlueHand4: BlueHand.UnpointCard(3); break;
                case CardPointerTarget.RedHand1: RedHand.UnpointCard(0); break;
                case CardPointerTarget.RedHand2: RedHand.UnpointCard(1); break;
                case CardPointerTarget.RedHand3: RedHand.UnpointCard(2); break;
                case CardPointerTarget.RedHand4: RedHand.UnpointCard(3); break;
                case CardPointerTarget.BlueDP: BlueDPPile.GetTopSprite().UnPointCardForViewMode(); break;
                case CardPointerTarget.RedDP: RedDPPile.GetTopSprite().UnPointCardForViewMode(); break;
            }
        }
        private void InitializeAttackSelectors()
        {
            //Fillout Blues

            //Top Row
            lblATASelectorBlueDigimonName.Text = BlueActiveDigimon.Name;
            lblATASelectorBlueType.Text = ParseTextWithSymbols(BlueActiveDigimon.Type);
            PicATASelectorBlueLevel.Image = ImageServer.Level(BlueActiveDigimon.Level);

            //Stats
            lblATASelectorBlueHp.Text = GetLevelModStat(BlueActiveDigimon.HP, BlueActiveStatsDemominator).ToString();
            lblATASelectorBlueCircle.Text = GetLevelModStat(BlueActiveDigimon.Circle, BlueActiveStatsDemominator).ToString();
            lblATASelectorBlueTriangle.Text = GetLevelModStat(BlueActiveDigimon.Triangle, BlueActiveStatsDemominator).ToString();
            lblATASelectorBlueCross.Text = GetLevelModStat(BlueActiveDigimon.Cross, BlueActiveStatsDemominator).ToString();
            //if the current digimon has its stats reduced, turn those labels red, otherwise keep them white.
            if(BlueActiveStatsDemominator != 1)
            {
                lblATASelectorBlueHp.ForeColor = Color.Red;
                lblATASelectorBlueCircle.ForeColor = Color.Red;
                lblATASelectorBlueTriangle.ForeColor = Color.Red;
                lblATASelectorBlueCross.ForeColor = Color.Red;
            }
            else
            {
                lblATASelectorBlueHp.ForeColor = Color.White;
                lblATASelectorBlueCircle.ForeColor = Color.White;
                lblATASelectorBlueTriangle.ForeColor = Color.White;
                lblATASelectorBlueCross.ForeColor = Color.White;
            }

            //Cross Effect
            lblATASelectorBlueCrossEffect.Text = ParseTextWithSymbols(BlueActiveDigimon.Cross_Effect);
            if(BlueActiveDigimon.Cross_Effect == "None")
            {
                PicATASelectorBlueCrossPriority.Visible = false;
            }
            else
            {
                PicATASelectorBlueCrossPriority.Visible = true;
                PicATASelectorBlueCrossPriority.Image = ImageServer.Priority(BlueActiveDigimon.Cross_Priority);
            }   

            //TurnMark
            if (_FirstAttacker == Player.Blue) { lblATASelectorBlueTurnMark.Text = "1st Attack"; lblATASelectorBlueTurnMark.ForeColor = Color.Yellow; }
            else { lblATASelectorBlueTurnMark.Text = "2nd Attack"; lblATASelectorBlueTurnMark.ForeColor = Color.Green; }

            ///////////////////////////////////////////////////////

            //Fillout Reds

            //Top Row
            lblATASelectorRedDigimonName.Text = RedActiveDigimon.Name;
            lblATASelectorRedType.Text = ParseTextWithSymbols(RedActiveDigimon.Type);
            PicATASelectorRedLevel.Image = ImageServer.Level(RedActiveDigimon.Level);

            //Stats
            lblATASelectorRedHp.Text = GetLevelModStat(RedActiveDigimon.HP, RedActiveStatsDemominator).ToString();
            lblATASelectorRedCircle.Text = GetLevelModStat(RedActiveDigimon.Circle, RedActiveStatsDemominator).ToString();
            lblATASelectorRedTriangle.Text = GetLevelModStat(RedActiveDigimon.Triangle, RedActiveStatsDemominator).ToString();
            lblATASelectorRedCross.Text = GetLevelModStat(RedActiveDigimon.Cross, RedActiveStatsDemominator).ToString();
            //if the current digimon has its stats reduced, turn those labels red, otherwise keep them white.
            if (RedActiveStatsDemominator != 1)
            {
                lblATASelectorRedHp.ForeColor = Color.Red;
                lblATASelectorRedCircle.ForeColor = Color.Red;
                lblATASelectorRedTriangle.ForeColor = Color.Red;
                lblATASelectorRedCross.ForeColor = Color.Red;
            }
            else
            {
                lblATASelectorRedHp.ForeColor = Color.White;
                lblATASelectorRedCircle.ForeColor = Color.White;
                lblATASelectorRedTriangle.ForeColor = Color.White;
                lblATASelectorRedCross.ForeColor = Color.White;
            }

            //Cross Effect
            lblATASelectorRedCrossEffect.Text = ParseTextWithSymbols(RedActiveDigimon.Cross_Effect);
            if (RedActiveDigimon.Cross_Effect == "None")
            {
                PicATASelectorRedCrossPriority.Visible = false;
            }
            else
            {
                PicATASelectorRedCrossPriority.Visible = true;
                PicATASelectorRedCrossPriority.Image = ImageServer.Priority(RedActiveDigimon.Cross_Priority);
            }

            //TurnMark
            if (_FirstAttacker == Player.Red) { lblATASelectorRedTurnMark.Text = "1st Attack"; lblATASelectorRedTurnMark.ForeColor = Color.Yellow; }
            else { lblATASelectorRedTurnMark.Text = "2nd Attack"; lblATASelectorRedTurnMark.ForeColor = Color.Green; }


            //Place both panel in its respective plave
            PanelBlueAttackSelector.BringToFront();
            PanelRedAttackSelector.BringToFront();
            PanelBlueAttackSelector.Location = BlueAttackSelectorLoc;
            PanelRedAttackSelector.Location = RedAttackSelectorLoc;
        }
        private void CloseAttackSelectors()
        {
            PanelBlueAttackSelector.Location = new Point(1000, 1000);
            PanelRedAttackSelector.Location = new Point(1000, 1000);
        }
        private void UpdateDebugData()
        {
            lbldebugCurrentGameState.Text = _CurrentGameState.ToString();
            lbldebugCurrentPhase.Text = _CurrentPhase.ToString();
            lbldebugCurrentCardPointer.Text = _CurrentCardPointer.ToString();
            lbldebugBlueHandSize.Text = BlueHand.Size.ToString();
            lbldebugIsBlueFirstAttack.Text = _FirstAttacker.ToString();
        }
        #endregion

        #region Internal Data
        public static List<CardInfo> CardDB = new List<CardInfo>();            
        private string Red_Name = "";
        private string Blue_Name = "";
        private string RedDeck_Name = "";
        private string BlueDeck_Name = "";
        private GameState _CurrentGameState = GameState.None;
        private Phase _CurrentPhase = Phase.None;
        private CardPointerTarget _CurrentCardPointer = CardPointerTarget.None;
        private CardPointerTarget _OldCardPointer = CardPointerTarget.None;
        private bool TurnSelector_LeftSelected = true;
        private bool _IsYesSelected = false;

        private Player _TurnPlayer = Player.Blue;
        private Player _FirstAttacker = Player.Blue;

        private bool _isBlueSupportTurn = true;
        private List<CardInfo> BlueDeck = new List<CardInfo>();
        private List<CardInfo> RedDeck = new List<CardInfo>();
        private List<CardInfo> BlueDiscardPile = new List<CardInfo>();
        private List<CardInfo> RedDiscardPile = new List<CardInfo>();
        private CardSprite BlueActiveDigimon = null;
        private CardSprite RedActiveDigimon = null;
        private CardSprite BlueSupportCard = null;
        private CardSprite RedSupportCard = null;
        private int BlueActiveStatsDemominator = 1;
        private int RedActiveStatsDemominator = 1;
        private Hand BlueHand = new Hand(true);
        private Hand RedHand = new Hand(false);
        private DPPile BlueDPPile;
        private DPPile RedDPPile;
        PictureBox[] Statuses = new PictureBox[4];
        private List<int> DigivolveCandidates;
        private string BlueAttack = "None";
        private string RedAttack = "None";
        #endregion

        #region Input Commands
        //Turn Decider Vier State
        private void LeftCommand_TurnDeciderView()
        {
            //Change the selection
            if (TurnSelector_LeftSelected)
            {
                TurnSelector_LeftSelected = false;
                PanelCardOption1.BackColor = Color.Transparent;
                PanelCardOption2.BackColor = Color.Yellow;
            }
            else
            {
                TurnSelector_LeftSelected = true;
                PanelCardOption1.BackColor = Color.Yellow;
                PanelCardOption2.BackColor = Color.Transparent;
            }
        }
        private void RightCommand_TurnDeciderView()
        {
            //Change the selection
            if (TurnSelector_LeftSelected)
            {
                TurnSelector_LeftSelected = false;
                PanelCardOption1.BackColor = Color.Transparent;
                PanelCardOption2.BackColor = Color.Yellow;
            }
            else
            {
                TurnSelector_LeftSelected = true;
                PanelCardOption1.BackColor = Color.Yellow;
                PanelCardOption2.BackColor = Color.Transparent;
            }
        }
        private void SelectCommand_TurnDeciderView()
        {
            //Disable any state until player can do an input
            _CurrentGameState = GameState.None;

            //TODO: randomize the result Rand.WillHappen(50);
            _TurnPlayer = Player.Blue;
            bool playerIsFirst = true;
            if (TurnSelector_LeftSelected)
            {

                PicOptionCard1.Image = ImageServer.TurnDeciderCard(playerIsFirst);
            }
            else
            {
                PicOptionCard2.Image = ImageServer.TurnDeciderCard(playerIsFirst);
            }
                    
            //Wait and Close the turn selector
            WaitNSeconds(1000);
            PanelTurnDecider.Visible = false;
            UpdateLowerMessage("");
            WaitNSeconds(1000);

            //Place the 1st attack on whoever is first.
            if(_TurnPlayer == Player.Blue)
            {
                PicFirstAttackMark.Visible = true;
            }
            else
            {
                Switch1stAttackMark();
                PicFirstAttackMark.Visible = true;
            }

            //Change the phase
            ChangePhase(Phase.Phase1_DrawCards);

        }

        //Redrawing State
        private void SelectCommand_Redrawing()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close Central message
            CloseCentralMessage();

            //Do a small delay to show first card discardted
            WaitNSeconds(300);

            //Change upper message
            UpdateUpperMessage("Preparation: Changing all Cards.");
         
            //Discard all cards to the discards pile
            Blue_DiscardHand(4);

            //Re do the Draw cards phase
            Phase1DrawCards();
        }

        //Preparing Hand State
        private void SquareCommand_PreparingHand()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            EnterCardViewMode();
        }
        private void TriangleCommand_PreparingHand()
        {
            //Player will attempt to discard hand and redraw
            //Update messages
            UpdateLowerMessage("");
            UpdateUpperMessage("Preparation: Change all Cards.");

            //Open Confirmation Message
            ShowCentralMessageQuestion("This will discard all Cards.\r\nIs this OK?");

            //Change State
            _CurrentGameState = GameState.DiscardQuestion;
        }
        private void CrossCommand_PreparingHand()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            if (BlueActiveDigimon == null)
            {
                UpdateLowerMessage("");
                WaitNSeconds(500);
                ChangePhase(Phase.Phase2_DigimonEntrance);
            }
            else
            {
                //End of the pre phasse
                UpdateUpperMessage("Preparation: Preparation Complete.");
                UpdateLowerMessage("");

                WaitNSeconds(1000);

                ShowCentralMessageQuestion("Is it OK to end the Preparation Phase?");

                //Change state
                _CurrentGameState = GameState.EndOfPreparationQuestion;
            }
        }

        //CardViewMode State
        private void LeftCommand_CardViewMode()
        {
            //Save the old pointer
            _OldCardPointer = _CurrentCardPointer;

            switch (_CurrentCardPointer)
            {
                case CardPointerTarget.BlueHand4:
                    if (BlueHand.IsSlotOpen(2))
                    {
                        if (BlueHand.IsSlotOpen(1))
                        {
                            if (BlueHand.IsSlotOpen(0))
                            {
                                if (BlueDeck.Count == 0)
                                {
                                    //Pointer does not move                           
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                    break;
                case CardPointerTarget.BlueHand3:
                    if (BlueHand.IsSlotOpen(1))
                    {
                        if (BlueHand.IsSlotOpen(0))
                        {
                            if (BlueDeck.Count == 0)
                            {
                                //Pointer does not move                           
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                    break;
                case CardPointerTarget.BlueHand2:
                    if (BlueHand.IsSlotOpen(0))
                    {
                        if (BlueDeck.Count == 0)
                        {
                            //Pointer does not move                           
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                    break;
                case CardPointerTarget.BlueHand1:
                    if(BlueDeck.Count > 0)
                    {
                        _CurrentCardPointer = CardPointerTarget.BlueDeck;
                    }
                    break;
                case CardPointerTarget.BlueActive:
                    if(BlueDPPile.Count > 0)
                    {
                        _CurrentCardPointer = CardPointerTarget.BlueDP;
                    }
                    break;
                case CardPointerTarget.RedActive:
                    if(BlueActiveDigimon == null)
                    {
                        if(BlueDPPile.Count == 0)
                        {
                            //do nothing
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueDP; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    break;
                case CardPointerTarget.RedDeck:
                    if (RedHand.GetSprite(3) == null)
                    {
                        if (RedHand.GetSprite(2) == null)
                        {
                            if (RedHand.GetSprite(1) == null)
                            {
                                if (RedHand.GetSprite(0) == null)
                                {
                                    //do nothing.
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                    break;
                case CardPointerTarget.RedHand4:
                    if (RedHand.GetSprite(2) == null)
                    {
                        if (RedHand.GetSprite(1) == null)
                        {
                            if (RedHand.GetSprite(0) == null)
                            {
                                //do nothing.
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                    break;
                case CardPointerTarget.RedHand3:
                    if (RedHand.GetSprite(1) == null)
                    {
                        if (RedHand.GetSprite(0) == null)
                        {
                            //do nothing.
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                    break;
                case CardPointerTarget.RedHand2:
                    if (RedHand.GetSprite(0) == null)
                    {
                        //do nothing.
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                    break;
                case CardPointerTarget.RedDP:
                    if(RedActiveDigimon == null)
                    {
                        if(BlueActiveDigimon == null)
                        {
                            if(BlueDPPile.Count == 0)
                            {

                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDP; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
            }

            //Update UI
            UpdateViewModePointer();

            //Update the viewer position
            UpdateCardViewWindowPosition();

            //Update View Mode Window
            UpdateCardViewWindowData();

            //Small delay
            WaitNSeconds(100);
        }
        private void RightCommand_CardViewMode()
        {
            //Save the old pointer
            _OldCardPointer = _CurrentCardPointer;

            switch(_CurrentCardPointer)
            {
                case CardPointerTarget.BlueDeck:
                    if (BlueHand.IsSlotOpen(0))
                    {
                        if (BlueHand.IsSlotOpen(1))
                        {
                            if (BlueHand.IsSlotOpen(2))
                            {
                                if (BlueHand.IsSlotOpen(3))
                                {
                                    //Pointer does not move
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                        break;
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                    break;
                case CardPointerTarget.BlueHand1:
                    if (BlueHand.IsSlotOpen(1))
                    {
                        if (BlueHand.IsSlotOpen(2))
                        {
                            if (BlueHand.IsSlotOpen(3))
                            {
                                //Pointer does not move
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                        }
                        else {  _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                    break;
                case CardPointerTarget.BlueHand2:
                    if (BlueHand.IsSlotOpen(2))
                    {
                        if (BlueHand.IsSlotOpen(3))
                        {
                            //Pointer does not move
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                    break;
                case CardPointerTarget.BlueHand3:
                    if (BlueHand.IsSlotOpen(3))
                    {
                        //Pointer does not move
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                    break;
                case CardPointerTarget.BlueDP:
                    if (BlueActiveDigimon == null)
                    {
                        if(RedActiveDigimon == null)
                        {
                            if(RedDPPile.Count > 0)
                            {
                                //do nothing
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedDP; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    break;
                case CardPointerTarget.BlueActive:
                    if (RedActiveDigimon == null)
                    {
                        if (RedDPPile.Count == 0)
                        {
                            //do nothing
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedDP; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
                case CardPointerTarget.RedActive:
                    if (RedDPPile.Count == 0)
                    {
                        //do nothing
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedDP; }
                    break;
                case CardPointerTarget.RedHand1:
                    if(RedHand.GetSprite(1) == null)
                    {
                        if (RedHand.GetSprite(2) == null)
                        {
                            if (RedHand.GetSprite(3) == null)
                            {
                                if(RedDeck.Count == 0)
                                {
                                    //do nothing
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                    break;
                case CardPointerTarget.RedHand2:
                    if (RedHand.GetSprite(2) == null)
                    {
                        if (RedHand.GetSprite(3) == null)
                        {
                            if (RedDeck.Count == 0)
                            {
                                //do nothing
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                    break;
                case CardPointerTarget.RedHand3:
                    if (RedHand.GetSprite(3) == null)
                    {
                        if (RedDeck.Count == 0)
                        {
                            //do nothing
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                    break;
                case CardPointerTarget.RedHand4:
                    if (RedDeck.Count == 0)
                    {
                        //do nothing
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                    break;
            }

            //Update UI
            UpdateViewModePointer();

            //Update the viewer position
            UpdateCardViewWindowPosition();

            //Update View Mode Window
            UpdateCardViewWindowData();

            //Small delay
            WaitNSeconds(100);
        }
        private void UpCommand_CardViewMode()
        {
            //Save the old pointer
            _OldCardPointer = _CurrentCardPointer;

            switch (_CurrentCardPointer)
            {
                case CardPointerTarget.BlueDeck:
                    if (BlueHand.IsSlotOpen(0))
                    {
                        if (BlueHand.IsSlotOpen(1))
                        {
                            if (BlueHand.IsSlotOpen(2))
                            {
                                if (BlueHand.IsSlotOpen(3))
                                {
                                    if (BlueActiveDigimon == null)
                                    {
                                        if (RedActiveDigimon == null)
                                        {
                                            if (RedDeck.Count == 0)
                                            {
                                                //Pointer does not move
                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                        break;
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand1;   }
                    break;
                case CardPointerTarget.BlueHand1:
                    if (BlueActiveDigimon == null)
                    {
                        if (RedActiveDigimon == null)
                        {
                            if (RedDeck.Count == 0)
                            {
                                if (BlueHand.IsSlotOpen(0))
                                {
                                    if(BlueHand.IsSlotOpen(1))
                                    {
                                        if (BlueHand.IsSlotOpen(2))
                                        {
                                            if (BlueHand.IsSlotOpen(3))
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    break;
                case CardPointerTarget.BlueHand2:
                    if (BlueActiveDigimon == null)
                    {
                        if (RedActiveDigimon == null)
                        {
                            if (RedDeck.Count == 0)
                            {
                                if (BlueHand.IsSlotOpen(0))
                                {
                                    if (BlueHand.IsSlotOpen(1))
                                    {
                                        if (BlueHand.IsSlotOpen(2))
                                        {
                                            if (BlueHand.IsSlotOpen(3))
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    break;
                case CardPointerTarget.BlueHand3:
                    if (BlueActiveDigimon == null)
                    {
                        if (RedActiveDigimon == null)
                        {
                            if (RedDeck.Count == 0)
                            {
                                if (BlueHand.IsSlotOpen(0))
                                {
                                    if (BlueHand.IsSlotOpen(1))
                                    {
                                        if (BlueHand.IsSlotOpen(2))
                                        {
                                            if (BlueHand.IsSlotOpen(3))
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    break;
                case CardPointerTarget.BlueHand4:
                    if (BlueActiveDigimon == null)
                    {
                        if (RedActiveDigimon == null)
                        {
                            if (RedDeck.Count == 0)
                            {
                                if (BlueHand.IsSlotOpen(0))
                                {
                                    if (BlueHand.IsSlotOpen(1))
                                    {
                                        if (BlueHand.IsSlotOpen(2))
                                        {
                                            if (BlueHand.IsSlotOpen(3))
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    break;
                case CardPointerTarget.BlueActive:
                    if (RedHand.IsSlotOpen(1))
                    {
                        if (RedHand.IsSlotOpen(0))
                        {
                            if (RedHand.IsSlotOpen(2))
                            {
                                if (RedHand.IsSlotOpen(3))
                                {
                                    if (RedDeck.Count == 0)
                                    {
                                        //then do nothing
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                    break;
                case CardPointerTarget.RedActive:
                    if (RedHand.IsSlotOpen(2))
                    {
                        if (RedHand.IsSlotOpen(3))
                        {
                            if (RedHand.IsSlotOpen(1))
                            {
                                if (RedHand.IsSlotOpen(0))
                                {
                                    if (RedDeck.Count == 0)
                                    {
                                        //then do nothing
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.RedDeck; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.RedHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.RedHand2; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.RedHand4; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedHand3; }
                    break;
            }

            //Update UI
            UpdateViewModePointer();

            //Update the viewer position
            UpdateCardViewWindowPosition();

            //Update View Mode Window
            UpdateCardViewWindowData();

            //Small delay
            WaitNSeconds(100);
        }
        private void DownCommand_CardViewMode()
        {
            //Save the old pointer
            _OldCardPointer = _CurrentCardPointer;

            switch (_CurrentCardPointer)
            {
                case CardPointerTarget.BlueActive:
                    if(BlueHand.IsSlotOpen(1))
                    {
                        if (BlueHand.IsSlotOpen(0))
                        {
                            if (BlueHand.IsSlotOpen(2))
                            {
                                if (BlueHand.IsSlotOpen(3))
                                {
                                    if (BlueDeck.Count == 0)
                                    {
                                        //then do nothing
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                    break;
                case CardPointerTarget.RedActive:
                    if (BlueHand.IsSlotOpen(2))
                    {
                        if (BlueHand.IsSlotOpen(3))
                        {
                            if (BlueHand.IsSlotOpen(1))
                            {
                                if (BlueHand.IsSlotOpen(0))
                                {
                                    if (BlueDeck.Count == 0)
                                    {
                                        //then do nothing
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                    break;
                case CardPointerTarget.RedDeck:
                    if (RedActiveDigimon == null)
                    {
                        if (BlueActiveDigimon == null)
                        {
                            if (BlueDeck.Count == 0)
                            {
                                if (BlueHand.GetSprite(0) == null)
                                {
                                    if (BlueHand.GetSprite(1) == null)
                                    {
                                        if (BlueHand.GetSprite(2) == null)
                                        {
                                            if (BlueHand.GetSprite(3) == null)
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
                case CardPointerTarget.RedHand1:
                    if (RedActiveDigimon == null)
                    {
                        if (BlueActiveDigimon == null)
                        {
                            if(BlueDeck.Count == 0)
                            {
                                if (BlueHand.GetSprite(0) == null)
                                {
                                    if (BlueHand.GetSprite(1) == null)
                                    {
                                        if (BlueHand.GetSprite(2) == null)
                                        {
                                            if (BlueHand.GetSprite(3) == null)
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
                case CardPointerTarget.RedHand2:
                    if (RedActiveDigimon == null)
                    {
                        if (BlueActiveDigimon == null)
                        {
                            if (BlueDeck.Count == 0)
                            {
                                if (BlueHand.GetSprite(0) == null)
                                {
                                    if (BlueHand.GetSprite(1) == null)
                                    {
                                        if (BlueHand.GetSprite(2) == null)
                                        {
                                            if (BlueHand.GetSprite(3) == null)
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
                case CardPointerTarget.RedHand3:
                    if (RedActiveDigimon == null)
                    {
                        if (BlueActiveDigimon == null)
                        {
                            if (BlueDeck.Count == 0)
                            {
                                if (BlueHand.GetSprite(0) == null)
                                {
                                    if (BlueHand.GetSprite(1) == null)
                                    {
                                        if (BlueHand.GetSprite(2) == null)
                                        {
                                            if (BlueHand.GetSprite(3) == null)
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
                case CardPointerTarget.RedHand4:
                    if (RedActiveDigimon == null)
                    {
                        if (BlueActiveDigimon == null)
                        {
                            if (BlueDeck.Count == 0)
                            {
                                if (BlueHand.GetSprite(0) == null)
                                {
                                    if (BlueHand.GetSprite(1) == null)
                                    {
                                        if (BlueHand.GetSprite(2) == null)
                                        {
                                            if (BlueHand.GetSprite(3) == null)
                                            {

                                            }
                                            else { _CurrentCardPointer = CardPointerTarget.BlueHand4; }
                                        }
                                        else { _CurrentCardPointer = CardPointerTarget.BlueHand3; }
                                    }
                                    else { _CurrentCardPointer = CardPointerTarget.BlueHand2; }
                                }
                                else { _CurrentCardPointer = CardPointerTarget.BlueHand1; }
                            }
                            else { _CurrentCardPointer = CardPointerTarget.BlueDeck; }
                        }
                        else { _CurrentCardPointer = CardPointerTarget.BlueActive; }
                    }
                    else { _CurrentCardPointer = CardPointerTarget.RedActive; }
                    break;
            }

            //Update UI
            UpdateViewModePointer();

            //Update the viewer position
            UpdateCardViewWindowPosition();

            //Update View Mode Window
            UpdateCardViewWindowData();

            //Small delay
            WaitNSeconds(100);
        }
        private void TriangleCommand_CardViewMode()
        {
            //Exit Card View Mode

            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Phase 7 is the only one that can be exit out
            if(_CurrentPhase != Phase.Phase7_UseSupportCard)
            {
                //Unpoint the current card
                UnpointCurrentCard();

                //Vanish the CardViewPanel
                PanelCardViewer.Location = new Point(1000, 1000);
            }         

            switch(_CurrentPhase)
            {
                case Phase.Phase1_DrawCards:
                    //Restore lower message
                    UpdateLowerMessage(SYM_CROSS + "OK  " + SYM_TRIANGLE + "Change All Cards   " + SYM_SQUARE + "View Cards");
                    //Change State        
                    _CurrentGameState = GameState.PreparingHand;
                    break;
                case Phase.Phase2_DigimonEntrance:
                    //Restore lower message
                    UpdateLowerMessage(SYM_CROSS + "OK  " + SYM_TRIANGLE + "Change All Cards   " + SYM_SQUARE + "View Cards");
                    //Vanish the statuses
                    VanishStatusIcons();
                    _CurrentPhase = Phase.Phase1_DrawCards;
                    PlacePhaseDisplay();
                    //Change State        
                    _CurrentGameState = GameState.PreparingHand;
                    break;
                case Phase.Phase6_ChooseAnAttack:
                    //restore the attack selector
                    InitializeAttackSelectors();
                    UpdateLowerMessage(SYM_CIRCLE + SYM_TRIANGLE + SYM_CROSS + "OK   " + SYM_SQUARE + "View Cards");
                    //Change State
                    _CurrentGameState = GameState.AttackSelecion;
                    break;
                case Phase.Phase7_UseSupportCard:
                    //NO ACTION WILL HAPPEN. Player must select a card or cancel
                    _CurrentGameState = GameState.CardViewMode;
                    break;
            }
        }
        private void CrossCommand_CardViewMode()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //In card view mode, set which card in hand is being point to
            CardSprite pointedCard = null;
            int handIndex = -1;
            if (_CurrentCardPointer == CardPointerTarget.BlueHand1 ||
                _CurrentCardPointer == CardPointerTarget.BlueHand2 ||
                _CurrentCardPointer == CardPointerTarget.BlueHand3 ||
                _CurrentCardPointer == CardPointerTarget.BlueHand4)
            {
                handIndex = GetHandPointerIndex();
                pointedCard = BlueHand.GetSprite(handIndex);
            }

            //Player can only select a card in this State
            //the current card pointed to is a hand card
            //EXTRA: In the Use Support Card Phase, you can select from the deck
            if(pointedCard != null || (_CurrentPhase == Phase.Phase7_UseSupportCard && _CurrentCardPointer == CardPointerTarget.BlueDeck))
            {
                switch (_CurrentPhase)
                {
                    case Phase.Phase1_DrawCards:
                        //Nothing happens but enable inputs again.
                        _CurrentGameState = GameState.CardViewMode;
                        break;
                    case Phase.Phase2_DigimonEntrance:
                        //Place card as active digimon, only if the pointed card is a Digimon
                        if (pointedCard != null && pointedCard.Class == "Digimon")
                        {
                            //flag if the new active card is C or U
                            if (pointedCard.Level == "U") { BlueActiveStatsDemominator = 4; }
                            else if (pointedCard.Level == "C") { BlueActiveStatsDemominator = 2; }
                            else { BlueActiveStatsDemominator = 1; }

                            //Move card to the active spot
                            SetDigimonAsActive(handIndex);
                        }
                        else
                        {
                            //Nothing happens but enable inputs again.
                            _CurrentGameState = GameState.CardViewMode;
                        }
                        break;

                    case Phase.Phase3_RackUpDP:
                        //Place the digimon in the DP pile
                        //Place card in the DP, only if the pointed card is a Digimon IN THE HAND
                        pointedCard = null;
                        handIndex = GetHandPointerIndex();

                        if (handIndex != -1) { pointedCard = BlueHand.GetSprite(handIndex); }

                        if (pointedCard != null && pointedCard.Class == "Digimon")
                        {
                            //Move card to the DP pile
                            MoveDigimonToDPpile(handIndex);

                            //Check if digimon can normal digievolve
                            UpdateNormalDigivolveCandidates();

                            if (DigivolveCandidates.Count > 0)
                            {
                                //Change Phase
                                ChangePhase(Phase.Phase5_DigimonDigivolve);
                            }
                            else
                            {
                                //ask end the digivolve phase
                                ShowCentralMessageQuestion("Is it OK to end the Digivolve Phase?");

                                //Change state
                                _CurrentGameState = GameState.EndOfDigivolveQuestion;
                            }
                        }
                        else
                        {
                            //Nothing happens but enable inputs again.
                            _CurrentGameState = GameState.CardViewMode;
                        }
                        break;

                    case Phase.Phase5_DigimonDigivolve:
                        //Digivolve to the digimon selected if, it is a candidate
                        handIndex = GetHandPointerIndex();

                        //If hand pointer index is in the candidate list
                        if (DigivolveCandidates.Contains(handIndex))
                        {
                            //Digivolve Digimon
                            lblBlueDigivolve.BringToFront();
                            lblBlueDigivolve.Visible = true;
                            WaitNSeconds(1500);
                            lblBlueDigivolve.Visible = false;

                            //Discard all dp cards
                            Blue_DiscardDP(BlueDPPile.Count);

                            //Move candidate as the active digimon
                            SetDigimonAsActive(handIndex);

                        }
                        else
                        {
                            //Nothing happens but enable inputs again.
                            _CurrentGameState = GameState.CardViewMode;
                        }
                        break;

                    case Phase.Phase6_ChooseAnAttack:
                        //Nothing happens but enable inputs again.
                        _CurrentGameState = GameState.CardViewMode;
                        break;

                    case Phase.Phase7_UseSupportCard:
                        //Select the Pointed card as support
                     
                        //if the selection is from the deck, generate the card sprite
                        //to set the pointed card.
                        if(_CurrentCardPointer == CardPointerTarget.BlueDeck)
                        {
                            CardInfo cardInfoFromDeck = BlueDeck[BlueDeck.Count - 1];
                            CardSprite cardFromDeck = new CardSprite(cardInfoFromDeck.id, this);
                            cardFromDeck.FlipCard();
                            cardFromDeck.Hide();
                            pointedCard = cardFromDeck;
                        }

                        //Pointed card must not be Evolution card from the hand
                        if (pointedCard.Class != "Evolution" && _CurrentCardPointer != CardPointerTarget.BlueDeck)
                        {
                            //now send the pointed  card to the Support Slot
                            SetCardAsSupport(handIndex, pointedCard);
                        }
                        break;
                }
            }
            else
            {
                //Nothing happens but enable inputs again.
                _CurrentGameState = GameState.CardViewMode;
            }
        }
        private void CircleCommand_CardViewMode()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            switch (_CurrentPhase)
            {
                //Circle will skip the rack up phase
                case Phase.Phase3_RackUpDP: 
                    //If the player has a Digivolve card move to phase 4
                    if(BlueHand.HasDigivolveCard())
                    {
                        ChangePhase(Phase.Phase4_UseDigivolveOption);
                    }
                    //Check if digmon can normal digivolve
                    else
                    {
                        //Check if digimon can normal digievolve
                        UpdateNormalDigivolveCandidates();

                        if (DigivolveCandidates.Count > 0)
                        {
                            //Change Phase
                            ChangePhase(Phase.Phase5_DigimonDigivolve);
                        }
                        else
                        {
                            //Ask to end the digivolve phase

                            //vanish the status and close card viewer
                            VanishStatusIcons();
                            PanelCardViewer.Location = new Point(1000, 1000);
                            //Unpoint the current card
                            UnpointCurrentCard();

                            //ask end the digivolve phase
                            ShowCentralMessageQuestion("Is it OK to end the Digivolve Phase?");

                            //Change state
                            _CurrentGameState = GameState.EndOfDigivolveQuestion;
                        }
                    }
                    break;
                case Phase.Phase7_UseSupportCard:
                    //Ask player to confirm to skip support
                    ShowCentralMessageQuestion("You are not using any Support Card.\nIs this OK?");
                    //Enter question state to have the player answer
                    _CurrentGameState = GameState.NoSupportCardUsageQuestion;
                    break;
                default: _CurrentGameState = GameState.CardViewMode; break;
            }
        }

        //LeftRight Command for any yes no question state
        private void LeftRightCommand_Question()
        {
            if (_IsYesSelected)
            {
                //Change selection to NO
                lblQuestionNo.BorderStyle = BorderStyle.FixedSingle;
                lblQuestionNo.BackColor = Color.SteelBlue;
                lblQuestionYes.BorderStyle = BorderStyle.None;
                lblQuestionYes.BackColor = Color.Transparent;
                _IsYesSelected = false;
            }
            else
            {
                lblQuestionYes.BorderStyle = BorderStyle.FixedSingle;
                lblQuestionYes.BackColor = Color.SteelBlue;
                lblQuestionNo.BorderStyle = BorderStyle.None;
                lblQuestionNo.BackColor = Color.Transparent;
                _IsYesSelected = true;
            }
        }

        //Discard Question State
        private void SelectCommand_DiscardQuestion()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close central message
            CloseCentralMessage();

            if (_IsYesSelected)
            {
                //Do a small delay to show first card discardted
                WaitNSeconds(300);

                //Discard all cards to the discards pile
                Blue_DiscardHand(4);

                //Re do the Draw cards phase
                Phase1DrawCards();
            }
            else
            {
                //return to the preparing hand state
                //Update the message
                UpdateUpperMessage("Preparation: Preparing a Hand");
                UpdateLowerMessage(SYM_CROSS + "OK  " + SYM_TRIANGLE + "Change All Cards   " + SYM_SQUARE + "View Cards");

                //Change game state
                _CurrentGameState = GameState.PreparingHand;
            }
        }

        //End Prep Question
        private void SelectCommand_EndOfPreparationQuestion()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close central message
            CloseCentralMessage();

            if (_IsYesSelected)
            {
                //Clear hand active index
                BlueHand.ClearActiveIndex();

                //Move to the Rack up phase
                ChangePhase(Phase.Phase3_RackUpDP);
            }
            else
            {
                //Return the Active Digimon to the hand if
                //the active was placed this turn
                if(BlueHand.WasDigimonPlaceActiveThisTurn())
                {
                    ReturnActiveToHand();
                }

                //Restore the messages
                UpdateUpperMessage("Preparation: Preparing a Hand");
                UpdateLowerMessage(SYM_CROSS + "OK  " + SYM_TRIANGLE + "Change All Cards   " + SYM_SQUARE + "View Cards");

                //Return to the Draw Phase
                _CurrentPhase = Phase.Phase1_DrawCards;
                PlacePhaseDisplay();

                //Change State        
                _CurrentGameState = GameState.PreparingHand;
            }
        }

        //No Battle Phase state
        private void SelectCommand_NoBattlePhaseMessage()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close the central message
            CloseCentralMessage();

            //End Turn
            EndPhase();
        }

        //End of Digivolve question state
        private void SelectCommand_EndOfDigivolveQuestion()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close central message
            CloseCentralMessage();

            if (_IsYesSelected)
            {
                ChangePhase(Phase.Phase6_ChooseAnAttack);
            }
            else
            {
                //return all cards back to the hand
                //(DP card placed and any digivolve card used)
                if(BlueHand.WasDigimonPlacedAtDPThisTUrn())
                {
                    //Return DP card to hand

                    //Save sprite
                    CardSprite tmpsprite = BlueDPPile.GetTopSprite();

                    //Remove card from DP
                    BlueDPPile.RemoveTopCard();

                    //Get the index of the hand where this card was originally at
                    int index = BlueHand.ActiveIndex;

                    //Add card to the hand
                    BlueHand.AddCard(tmpsprite, index);

                    //Restore sprite to its original size
                    tmpsprite.RestoreSize();
                }
                
                //After that return to the beginning of the rack up phasew
                ChangePhase(Phase.Phase3_RackUpDP);
            }
        }

        //Deciding an attack state
        private void SquareCommand_DecidingAttack()
        {
            //Hide the attack selectors
            CloseAttackSelectors();

            //You will enter card view mode.
            EnterCardViewMode();
        }
        private void AttackCommand_DecidingAttack(string selection)
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Set Both attacks
            RedAttack = OpponentAI.GetAttackSelection(RedActiveDigimon, BlueActiveDigimon, RedHand, BlueHand);
            BlueAttack = selection;

            //Vanish the attack selectors
            CloseAttackSelectors();
            WaitNSeconds(500);

            //Enter Use Support Card Phase
            ChangePhase(Phase.Phase7_UseSupportCard);      
        }

        //No Support Cards state
        private void SelectCommand_NoSupportCardsMessage()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close the central message
            CloseCentralMessage();

            //If oponent needs to use support cards
            //TODO:
            //if both players did their support selection, start support resolution
            //TODO:
        }

        //No Support Usage Question State state
        private void SelectCommand_NoSupportCardUsageQuestion()
        {
            //currently disable any state to prevent inputs
            _CurrentGameState = GameState.None;

            //Close central message
            CloseCentralMessage();

            if (_IsYesSelected)
            {
                //if blue was first attacker == blue was second to set support
                if (_FirstAttacker == Player.Blue)
                {
                    //TOD0: Start the Support Resolution phase
                }
                else
                {
                    //TODO: have REd select support
                    _isBlueSupportTurn = false;
                    Red_OpenSupportSlot();
                }

            }
            else
            {
                ///GO back to card view mode to select
                CloseCentralMessage();
                _CurrentGameState = GameState.CardViewMode;
            }
        }
        #endregion

        #region PhasesTriggers
        private void ChangePhase(Phase newPhase)
        {
            //Update the new Phase value
            _CurrentPhase = newPhase;

            //Do the phase sign animation
            PlacePhaseDisplay();
                  
            //Trigger the next action based on the updated phase
            switch(_CurrentPhase)
            {
                case Phase.Phase1_DrawCards: Phase1DrawCards(); break;
                case Phase.Phase2_DigimonEntrance: Phase2DigimonEntrance(); break;
                case Phase.Phase3_RackUpDP: Phase3RackUpDP(); break;
                //todo case Phase.Phase4_UseDigivolveOption: 
                case Phase.Phase5_DigimonDigivolve: Phase5DigimonDigivolve(); break;
                case Phase.Phase6_ChooseAnAttack: Phase6SelectAttack(); break;
                case Phase.Phase7_UseSupportCard: StartUseSupportCardPhase(); break;
            }
        }

        private void Phase1DrawCards()
        {
            //Update the message
            UpdateUpperMessage("Preparation: Preparing a Hand");

            if (_TurnPlayer == Player.Blue)
            {
                //Draw until 4 
                Blue_DrawCards(4);
                //if there is not active digimon and hand has no digimon, redraw.
                if(BlueActiveDigimon == null && BlueHand.HasNoDigimon())
                {
                    if(BlueDeck.Count > 0)
                    {
                        //Show warning message.
                        ShowCentralMessage("Redrawing Cards because there are no Digimon Cards.");
                        UpdateUpperMessage("Preparation: There is no Diginmon!");
                        UpdateLowerMessage("");

                        //Move to the redrawing state
                        _CurrentGameState = GameState.Redrawing;
                    }
                    else //but if the deck is empty the player losses
                    {
                        ShowCentralMessage("There are no more Cards, so " + Blue_Name + " loses!");
                        _CurrentGameState = GameState.GameOver;
                    }
                    
                }
                else
                {
                    UpdateLowerMessage(SYM_CROSS + "OK  " + SYM_TRIANGLE + "Change All Cards   " + SYM_SQUARE + "View Cards");

                    //Change game state
                    _CurrentGameState = GameState.PreparingHand;
                }                
            }
            else
            {
                //Draw until 4 
                Red_DrawCards(4);
                //if there is not active digimon and hand has no digimon, redraw.
                if (RedActiveDigimon == null && RedHand.HasNoDigimon())
                {
                    if (RedDeck.Count > 0)
                    {
                        //Show warning message.
                        ShowCentralMessage("Redrawing Cards because there are no Digimon Cards.");
                        UpdateUpperMessage("Preparation: There is no Diginmon!");
                        UpdateLowerMessage("");

                        //automatically redraw
                        Red_Redraw();
                    }
                    else //but if the deck is empty the oponent losses
                    {
                        ShowCentralMessage("There are no more Cards, so " + Red_Name + " loses!");
                        _CurrentGameState = GameState.GameOver;
                    }

                }
                else
                {
                    //Flag the current game state as the CPU taking action.
                    //During this state, the player does not have any action.
                    _CurrentGameState = GameState.CPUActive;

                    //Get the AI Choice for active digimon                    
                    int OponentChoice = OpponentAI.GetHandActiveChoice(RedHand, RedDeck.Count);

                    if(OponentChoice == -1)
                    {
                        //automatically redraw
                        Red_Redraw();
                    }
                    else
                    {
                        //Move to Digimon entrance.
                        ChangePhase(Phase.Phase2_DigimonEntrance);
                    }
                }
            }
        }
        private void Phase2DigimonEntrance()
        {
            //Update the message
            UpdateUpperMessage("Preparation: Digimon Entrance.");

            if (_TurnPlayer == Player.Blue)
            {
                //Open the viewer
                UpdateLowerMessage(SYM_DPAD + "Select   " + SYM_CROSS + "OK   " + SYM_TRIANGLE + "Return");

                //Show the digimon active status (OK, 1/2, 1/4)
                PictureBox[] Statuses = new PictureBox[4];
                Statuses[0] = PicStatus1;
                Statuses[1] = PicStatus2;
                Statuses[2] = PicStatus3;
                Statuses[3] = PicStatus4;

                //Show Status marks
                for (int x = 0; x < 4; x++)
                {
                    if (!BlueHand.IsSlotOpen(x))
                    {
                        if (BlueHand.GetSprite(x).Class == "Digimon")
                        {
                            Statuses[x].Visible = true;
                            Statuses[x].Image = ImageServer.Status(BlueHand.GetSprite(x).Level);
                        }
                        else
                        {
                            Statuses[x].Visible = false;
                        }
                    }
                    else
                    {
                        Statuses[x].Visible = false;
                    }
                }

                //Enter the view card mode, but Phase will determine with available actions in the state
                EnterCardViewMode();
            }
            else
            {
                //Update lower message
                UpdateLowerMessage("");

                //Place the best active Digimon card
                int OponentChoice = OpponentAI.GetHandActiveChoice(RedHand, RedDeck.Count);

                //Determine if opponent set a C or U as initial active and flag it.
                string level = RedHand.GetSprite(OponentChoice).Level;

                if (level == "U") { RedActiveStatsDemominator = 4; }
                else if (level == "C") { RedActiveStatsDemominator = 2; }
                else { RedActiveStatsDemominator = 1; }

                //Place digimon to the active spot
                SetDigimonAsActive(OponentChoice);
            }
        }
        private void Phase3RackUpDP()
        {          
            if (_TurnPlayer == Player.Blue)
            {
                //Validate that Blue can continue in this phase
                if(BlueHand.HasNoDigimon() || BlueDPPile.Count == 8)
                {
                    //Go to the Use digivolve card phase, if there is one in hand
                    if(BlueHand.HasDigivolveCard())
                    {
                        //Change Phase
                        ChangePhase(Phase.Phase4_UseDigivolveOption);
                    }
                    //Go to the Digivolve phase (if possible) 
                    else
                    {
                        UpdateNormalDigivolveCandidates();

                        if (DigivolveCandidates.Count > 0)
                        {
                            ChangePhase(Phase.Phase5_DigimonDigivolve);
                        }
                        else
                        {
                            //Ask to finish the digivolve phase
                            //ask end the digivolve phase
                            ShowCentralMessageQuestion("Is it OK to end the Digivolve Phase?");

                            //Change state
                            _CurrentGameState = GameState.EndOfDigivolveQuestion;
                        }
                    }
                }
                else
                {
                    //continue with the phase...
                    //Update the message
                    UpdateUpperMessage("Digivolve: Digivolve Points (DP).");

                    //Open the viewer
                    UpdateLowerMessage(SYM_DPAD + "Select   " + SYM_CROSS + "OK   " + SYM_CIRCLE + "Cancel");

                    //Show the digimon active status (all OK)
                    //Show Status marks
                    for (int x = 0; x < 4; x++)
                    {
                        if (!BlueHand.IsSlotOpen(x))
                        {
                            if (BlueHand.GetSprite(x).Class == "Digimon")
                            {
                                Statuses[x].Visible = true;
                                Statuses[x].BringToFront();
                                //Pass R so all of them get an OK one
                                Statuses[x].Image = ImageServer.Status("R");
                            }
                            else
                            {
                                Statuses[x].Visible = false;
                            }
                        }
                        else
                        {
                            Statuses[x].Visible = false;
                        }
                    }

                    //Enter the view card mode, but Phase will determine with available actions in the state
                    EnterCardViewMode();
                }
            }
            else
            {
                //Validate that Red can continue in this phase
                if (RedHand.HasNoDigimon() || RedDPPile.Count == 8)
                {
                    //Go to the Use digivolve card phase, if there is one in hand
                    if (RedHand.HasDigivolveCard())
                    {
                        //Change Phase
                        ChangePhase(Phase.Phase4_UseDigivolveOption);
                    }
                    //Go to the Digivolve phase (if possible) 
                    else
                    {
                        UpdateNormalDigivolveCandidates();

                        if (DigivolveCandidates.Count > 0)
                        {
                            ChangePhase(Phase.Phase5_DigimonDigivolve);
                        }
                        else
                        {
                            ChangePhase(Phase.Phase6_ChooseAnAttack);
                        }
                    }
                }
                else
                {
                    //continue with the phase...
                    //Update the message
                    UpdateUpperMessage("Digivolve: Digivolve Points (DP).");

                    //Determine which digimon card will be place at the DP pile
                    int handChoice = OpponentAI.GetHandDPChoice(RedHand, RedActiveDigimon.Data);

                    //If hand choice is -1, then the AI decided to not select a card for DP
                    if(handChoice != -1)
                    {
                        //Place such card as dp
                        //Move card to the DP pile
                        MoveDigimonToDPpile(handChoice);
                    }

                    //Check if digimon can normal digievolve
                    UpdateNormalDigivolveCandidates();

                    if (DigivolveCandidates.Count > 0)
                    {
                        //Change Phase
                        ChangePhase(Phase.Phase5_DigimonDigivolve);
                    }
                    else
                    {
                        //Move to the battle pahse
                        ChangePhase(Phase.Phase6_ChooseAnAttack);
                    }
                }
            }
        }
        private void Phase5DigimonDigivolve()
        {
            //Update the messages
            UpdateUpperMessage("Digivolve: Digivolve Digimon");
            UpdateLowerMessage(SYM_DPAD + "Select   " + SYM_CROSS + "OK   " + SYM_CIRCLE + "Cancel");
           
            //Mark all candidates as "OK"
            for (int x = 0; x < DigivolveCandidates.Count; x++)
            {
                Statuses[DigivolveCandidates[x]].Image = ImageServer.Status("R");
                Statuses[DigivolveCandidates[x]].Visible = true;
            }

            //Move State and phase
            EnterCardViewMode();
        }
        private void Phase6SelectAttack()
        {
            if(_TurnPlayer == Player.Blue)
            {
                //If the oponent has a digimon enter the battle phase
                //Otherwise the turn will end
                if (RedActiveDigimon == null)
                {
                    //End turn
                    UpdateUpperMessage("Battle: " + Red_Name + " has no Digimon.");
                    UpdateLowerMessage("");

                    WaitNSeconds(1000);
                    ShowCentralMessage("Since " + Red_Name + " has no Digimon, there is no Battle Phase.");

                    //Change State
                    _CurrentGameState = GameState.NoBattlePhaseMessage;
                }
                else
                {
                    //Enter battle phase
                    StartBattlePhase();
                }
            }
            else
            {
                //If the player has a digimon enter the battle phase
                //Otherwise the turn will end
                if (BlueActiveDigimon == null)
                {
                    //End turn
                    UpdateUpperMessage("Battle: " + Blue_Name + " has no Digimon.");
                    UpdateLowerMessage("");

                    WaitNSeconds(1000);
                    ShowCentralMessage("Since " + Blue_Name + " has no Digimon, there is no Battle Phase.");

                    //Change State
                    _CurrentGameState = GameState.NoBattlePhaseMessage;
                }
                else
                {
                    //Enter battle phase
                    StartBattlePhase();
                }
            }
        }
        private void StartBattlePhase()
        {
            //Initialize the Attack selectors and show then for players to select attack.
            InitializeAttackSelectors();

            //Update the lower message
            UpdateUpperMessage("Battle: Deciding an Attack.");
            UpdateLowerMessage(SYM_CIRCLE + SYM_TRIANGLE + SYM_CROSS + "OK   " + SYM_SQUARE + "View Cards");

            //Change State
            _CurrentGameState = GameState.AttackSelecion;

        }
        private void EndPhase()
        {
            //switch active player
            if (_TurnPlayer == Player.Blue)
            {
                _TurnPlayer = Player.Red;
            }
            else
            {
                _TurnPlayer = Player.Blue;
            }
            //Switch attack first
            if (_FirstAttacker == Player.Blue)
            {
                _FirstAttacker = Player.Red;
            }
            else
            {
                _FirstAttacker = Player.Blue;
            }
            Switch1stAttackMark();

            //Change the phase
            ChangePhase(Phase.Phase1_DrawCards);
        }
        private void Blue_DrawCards(int amount)
        {
            for(int x = 0; x < amount; x++)
            {
                if(BlueDeck.Count > 0)
                {
                    if(BlueHand.Size < 4)
                    {
                        //Draw a card
                        
                        //Create the cardsprite with the ID of the first card in the deck
                        int cardid = BlueDeck[0].id;
                        CardSprite cardToAdd = new CardSprite(cardid, this);

                        //Add card to the hand
                        BlueHand.AddCard(cardToAdd);

                        //Remove the card data from the deck
                        BlueDeck.RemoveAt(0);
                        lblBlueDeckCount.Text = BlueDeck.Count.ToString();
                        if (BlueDeck.Count <= 7) { lblBlueDeckCount.ForeColor = Color.Red; }

                        //Vanish the last card from the deck UI
                        if(BlueDeck.Count == 0)
                        {
                            PanelBlueDeckBack.Visible = false;
                        }

                        WaitNSeconds(500);

                    }
                }
            }
        }
        private void Red_DrawCards(int amount)
        {
            for (int x = 0; x < amount; x++)
            {
                if (RedDeck.Count > 0)
                {
                    if (RedHand.Size < 4)
                    {
                        //Draw a card

                        //Create the cardsprite with the ID of the first card in the deck
                        int cardid = RedDeck[0].id;
                        CardSprite cardToAdd = new CardSprite(cardid, this);

                        //Add card to the hand
                        RedHand.AddCard(cardToAdd);

                        //Remove the card data from the deck
                        RedDeck.RemoveAt(0);
                        lblRedDeckCount.Text = RedDeck.Count.ToString();
                        if (RedDeck.Count <= 7) { lblRedDeckCount.ForeColor = Color.Red; }

                        //Vanish the last card from the deck UI
                        if (RedDeck.Count == 0)
                        {
                            PanelRedDeckBack.Visible = false;
                        }

                        WaitNSeconds(500);

                    }
                }
            }
        }
        private void Red_Redraw()
        {
            //Close Central message
            CloseCentralMessage();

            //Do a small delay to show first card discardted
            WaitNSeconds(300);

            //Change upper message
            UpdateUpperMessage("Preparation: Changing all Cards.");

            //Discard all cards to the discards pile
            Red_DiscardHand(4);

            //Re do the Draw cards phase
            Phase1DrawCards();
        }
        private void Blue_DiscardHand(int amount)
        {
            for (int x = 0; x < amount; x++)
            {
                if(!BlueHand.IsSlotOpen(x))
                {
                    //Add the card data to the discard pile
                    BlueDiscardPile.Add(BlueHand.GetSprite(x).Data);
                    lblBlueDiscardCount.Text = BlueDiscardPile.Count.ToString();

                    //Save the cardsprite to be disposed
                    CardSprite tmp = BlueHand.GetSprite(x);

                    //Remove card from hand
                    BlueHand.RemoveCard(x);

                    //Disposing the cardsprite ref will erase the card from the UI
                    tmp.Dispose();

                    //When discarting to the offline deck
                    //make sure a card is "visible" in there.                 
                    PanelBlueDiscardBack.Visible = true;
                    WaitNSeconds(500);
                }               
            }

        }
        private void Red_DiscardHand(int amount)
        {
            for (int x = 0; x < amount; x++)
            {
                if (!RedHand.IsSlotOpen(x))
                {
                    //Add the card data to the discard pile
                    RedDiscardPile.Add(RedHand.GetSprite(x).Data);
                    lblRedDiscardCount.Text = RedDiscardPile.Count.ToString();

                    //Save the cardsprite to be disposed
                    CardSprite tmp = RedHand.GetSprite(x);

                    //Remove card from hand
                    RedHand.RemoveCard(x);

                    //Disposing the cardsprite ref will erase the card from the UI
                    tmp.Dispose();

                    //When discarting to the offline deck
                    //make sure a card is "visible" in there.                 
                    PanelRedDiscardBack.Visible = true;
                    WaitNSeconds(500);
                }
            }

        }
        private void Blue_DiscardDP(int amount)
        {
            for (int x = 0; x < amount; x++)
            {
                //Add the card data to the discard pile
                BlueDiscardPile.Add(BlueDPPile.GetTopCard());
                lblBlueDiscardCount.Text = BlueDiscardPile.Count.ToString();

                //Save the cardsprite to be disposed
                CardSprite tmp = BlueDPPile.GetTopSprite();

                //Remove card from hand
                BlueDPPile.RemoveTopCard();

                //Disposing the cardsprite ref will erase the card from the UI
                tmp.Dispose();

                //When discarting to the offline deck
                //make sure a card is "visible" in there.                 
                PanelBlueDiscardBack.Visible = true;
                WaitNSeconds(500);
            }
        }
        private void SetDigimonAsActive(int handIndex)
        {
            if (_TurnPlayer == Player.Blue)
            {
                //Remove view mode elemensts
                VanishStatusIcons();
                PanelCardViewer.Location = new Point(1000, 1000);

                //Unpoint the card before moving it ot the hand
                BlueHand.UnpointCard(handIndex);

                //Place Blue Active
                BlueActiveDigimon = BlueHand.GetSprite(handIndex);
                BlueActiveDigimon.SetLocation(BlueActiveLoc);
                BlueActiveDigimon.SetActiveStats(BlueActiveStatsDemominator);

                //Activate Active Digimon stats
                InitializeActiveDigimonStats(BlueActiveDigimon);

                //Remove card from hand
                BlueHand.RemoveCard(handIndex);

                //Move State/Phase based on which phase is currently active
                if (_CurrentPhase == Phase.Phase2_DigimonEntrance)
                {
                    UpdateUpperMessage("Preparation: Preparation Complete.");
                    UpdateLowerMessage("");

                    WaitNSeconds(1000);

                    ShowCentralMessageQuestion("Is it OK to end the Preparation Phase?");

                    //Change state
                    _CurrentGameState = GameState.EndOfPreparationQuestion;
                }
                else if (_CurrentPhase == Phase.Phase5_DigimonDigivolve)
                {
                    //Change straigh to the battle phase
                    ChangePhase(Phase.Phase6_ChooseAnAttack);
                }
            }
            else
            {
                //Place Red Active
                RedActiveDigimon = RedHand.GetSprite(handIndex);
                RedActiveDigimon.SetLocation(RedActiveLoc);
                RedActiveDigimon.SetActiveStats(RedActiveStatsDemominator);

                //Activate Active Digimon stats
                InitializeActiveDigimonStats(RedActiveDigimon);

                //Remove card from hand
                RedHand.RemoveCard(handIndex);

                //Move State/Phase based on which phase is currently active
                if (_CurrentPhase == Phase.Phase2_DigimonEntrance)
                {
                    //Move to the Rack up Phase
                    ChangePhase(Phase.Phase3_RackUpDP);
                }
                else if (_CurrentPhase == Phase.Phase5_DigimonDigivolve)
                {
                    //Change straigh to the battle phase
                    ChangePhase(Phase.Phase6_ChooseAnAttack);
                }
            }
        }
        private void MoveDigimonToDPpile(int handIndex)
        {
            if (_TurnPlayer == Player.Blue)
            {
                //Remove view mode elemensts
                VanishStatusIcons();
                PanelCardViewer.Location = new Point(1000, 1000);

                //Unpoint the card before moving it ot the hand
                BlueHand.UnpointCard(handIndex);

                //Place Card in the DP slor
                BlueDPPile.AddCard(BlueHand.GetSprite(handIndex));

                //Remove Card from Hand (data, the sprite was already moved)
                BlueHand.RemoveCard(handIndex);
            }
            else
            {
                //Place Red Active
                //Place Card in the DP slor
                RedDPPile.AddCard(RedHand.GetSprite(handIndex));

                //Remove Card from Hand (data, the sprite was already moved)
                RedHand.RemoveCard(handIndex);
            }
        }
        private void ReturnActiveToHand()
        {
            //Return the active card back to the hand.
            BlueHand.ReturnCardToHand(BlueActiveDigimon);

            //Clear the active stats
            ClearActiveDigimonStats();

            //Remove card from active
            BlueActiveDigimon = null;
        }
        private void StartUseSupportCardPhase()
        {
            //Open the support slot for the player that goes second
            if (_FirstAttacker == Player.Blue)
            {
                //REd uses support first
                _isBlueSupportTurn = false;
            }
            else
            {
                _isBlueSupportTurn = true;

                //Open support (This handles not cards for support and opening card view 
                //mode otherwise.
                Blue_OpenSupportSlot();               
            }
        }
        private void Blue_OpenSupportSlot()
        {
            //update top message
            UpdateUpperMessage("Battle: " + Blue_Name + "'s Support Card.");
            UpdateLowerMessage("");
            
            //Move active player panel, Active digion sprite, DP pile sprite(s), and digivolve banner.
            bool DPPileIsNotEmpty = BlueDPPile.Count > 0;
            for (int x = 0; x < 45; x++)
            {
                PanelPlayerActive.Location = new Point(PanelPlayerActive.Location.X-2, PanelPlayerActive.Location.Y);
                BlueActiveDigimon.SetLocation(new Point(BlueActiveDigimon.Location.X - 2, BlueActiveDigimon.Location.Y));
                if(DPPileIsNotEmpty)
                {
                    BlueDPPile.GetTopSprite().SetLocation(new Point(BlueDPPile.GetTopSprite().Location.X - 2, BlueDPPile.GetTopSprite().Location.Y));
                }
            }

            //Check if the player has an empty hand AND empty deck
            //on this scenario player cannot select support card
            if(BlueHand.IsEmpty && BlueDeck.Count == 0)
            {
                //Skip support selection
                ShowCentralMessage("You have no cards left, so\nyou can't use any Support Cards");

                //Update Game State to have player close message
                _CurrentGameState = GameState.NoSupportCardsMessage;
            }
            else
            {
                //Display Card Status to use as Support,
                //ONLY Digivolution cards cant be used as support
                PictureBox[] Statuses = new PictureBox[5];
                Statuses[0] = PicStatus1;
                Statuses[1] = PicStatus2;
                Statuses[2] = PicStatus3;
                Statuses[3] = PicStatus4;
                Statuses[4] = PicStatus5;

                //Mark the cards in the hand
                for (int x = 0; x < 4; x++)
                {
                    if (!BlueHand.IsSlotOpen(x))
                    {
                        if (BlueHand.GetSprite(x).Class != "Evolution")
                        {
                            Statuses[x].Visible = true;
                            Statuses[x].Image = ImageServer.OKStatus();
                        }
                        else
                        {
                            Statuses[x].Visible = false;
                        }
                    }
                    else
                    {
                        Statuses[x].Visible = false;
                    }
                }

                //Mark the card in the deck
                if (BlueDeck.Count > 0)
                {
                    PicStatus5.Visible = true;

                }
                else
                {
                    PicStatus5.Visible = false;
                }

                //Open card view mode to select support
                EnterCardViewMode();
            }
        }
        private void Red_OpenSupportSlot()
        {
            //update top message
            UpdateUpperMessage("Battle: " + Red_Name + "'s Support Card.");
            UpdateLowerMessage("");

            //Move active player panel, Active digion sprite, DP pile sprite(s), and digivolve banner.
            bool DPPileIsNotEmpty = RedDPPile.Count > 0;
            for (int x = 0; x < 45; x++)
            {
                PanelOponentActive.Location = new Point(PanelOponentActive.Location.X + 2, PanelOponentActive.Location.Y);
                RedActiveDigimon.SetLocation(new Point(RedActiveDigimon.Location.X + 2, RedActiveDigimon.Location.Y));
                if (DPPileIsNotEmpty)
                {
                    RedDPPile.GetTopSprite().SetLocation(new Point(RedDPPile.GetTopSprite().Location.X + 2, RedDPPile.GetTopSprite().Location.Y));
                }
            }

            //Check if the player has an empty hand AND empty deck
            //on this scenario player cannot select support card
            if (RedHand.IsEmpty && RedDeck.Count == 0)
            {
                //Skip support selection, no action from Opponet
                //go to Support Resolution Phase
                //TODO:
            }
            else
            {
                //TODO: AI make the support card selection
            }
        }
        private void SetCardAsSupport(int handIndex, CardSprite card)
        {
            if (_isBlueSupportTurn)
            {
                //Remove view mode elemensts
                VanishStatusIcons();
                PanelCardViewer.Location = new Point(1000, 1000);

                //Unpoint the card before moving it ot the hand
                UnpointCurrentCard();

                //Place Blue Support card
                BlueSupportCard = card;
                BlueSupportCard.SetLocation(BlueActiveLoc);
                BlueSupportCard.Show();

                //Remove card from hand if from hand
                if(handIndex != -1)
                {
                    BlueHand.RemoveCard(handIndex);
                }
                else
                {
                    //Remove the card data from the deck
                    BlueDeck.RemoveAt(0);
                    lblBlueDeckCount.Text = BlueDeck.Count.ToString();
                    if (BlueDeck.Count <= 7) { lblBlueDeckCount.ForeColor = Color.Red; }

                    //Vanish the last card from the deck UI
                    if (BlueDeck.Count == 0)
                    {
                        PanelBlueDeckBack.Visible = false;
                    }
                }

                //Check with state is next.
                //if blue was first attacker == blue was second to set support
                if(_FirstAttacker == Player.Blue)
                {
                    //TOD0: Start the Support Resolution phase
                }
                else
                { 
                    //TODO: have REd select support
                    _isBlueSupportTurn = false;
                    Red_OpenSupportSlot();
                }       
            }
            else
            {
                Red_OpenSupportSlot();
            }
        }
        #endregion

        #region Controls
        /// <summary>
        /// Controls the Commands of the W Button.
        /// </summary>
        protected virtual void WButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.CardViewMode: UpCommand_CardViewMode(); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the S Button.
        /// </summary>
        protected virtual void SButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.CardViewMode: DownCommand_CardViewMode(); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the A Button.
        /// </summary>
        protected virtual void AButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.TurnDeciderInView: LeftCommand_TurnDeciderView(); break;
                case GameState.CardViewMode: LeftCommand_CardViewMode(); break;
                case GameState.DiscardQuestion: LeftRightCommand_Question(); break;
                case GameState.EndOfPreparationQuestion: LeftRightCommand_Question(); break;
                case GameState.EndOfDigivolveQuestion: LeftRightCommand_Question(); break;
                case GameState.NoSupportCardUsageQuestion: LeftRightCommand_Question(); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the D Button.
        /// </summary>
        protected virtual void DButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.TurnDeciderInView: RightCommand_TurnDeciderView(); break;
                case GameState.CardViewMode: RightCommand_CardViewMode(); break;
                case GameState.DiscardQuestion: LeftRightCommand_Question(); break;
                case GameState.EndOfPreparationQuestion: LeftRightCommand_Question(); break;
                case GameState.EndOfDigivolveQuestion: LeftRightCommand_Question(); break;
                case GameState.NoSupportCardUsageQuestion: LeftRightCommand_Question(); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the J Button.
        /// </summary>
        protected virtual void JButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.TurnDeciderInView: SelectCommand_TurnDeciderView(); break;
                case GameState.PreparingHand: CrossCommand_PreparingHand(); break;
                case GameState.Redrawing: SelectCommand_Redrawing(); break;
                case GameState.DiscardQuestion: SelectCommand_DiscardQuestion(); break;
                case GameState.CardViewMode: CrossCommand_CardViewMode(); break;
                case GameState.EndOfPreparationQuestion: SelectCommand_EndOfPreparationQuestion(); break;
                case GameState.NoBattlePhaseMessage: SelectCommand_NoBattlePhaseMessage(); break;
                case GameState.EndOfDigivolveQuestion: SelectCommand_EndOfDigivolveQuestion(); break;
                case GameState.AttackSelecion: AttackCommand_DecidingAttack("CROSS"); break;
                case GameState.NoSupportCardsMessage: SelectCommand_NoSupportCardsMessage(); break;
                case GameState.NoSupportCardUsageQuestion: SelectCommand_NoSupportCardUsageQuestion(); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the I Button.
        /// </summary>
        protected virtual void IButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.CardViewMode: TriangleCommand_CardViewMode(); break;
                case GameState.PreparingHand: TriangleCommand_PreparingHand(); break;
                case GameState.AttackSelecion: AttackCommand_DecidingAttack("TRIANGLE"); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the K Button.
        /// </summary>
        protected virtual void KButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.PreparingHand: SquareCommand_PreparingHand(); break;
                case GameState.AttackSelecion: SquareCommand_DecidingAttack(); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the L Button.
        /// </summary>
        protected virtual void LButtom()
        {
            switch (_CurrentGameState)
            {
                case GameState.CardViewMode: CircleCommand_CardViewMode(); break;
                case GameState.AttackSelecion: AttackCommand_DecidingAttack("CIRCLE"); break;
                default: break;
            }
        }
        /// <summary>
        /// Controls the Commands of the Enter Button.
        /// </summary>
        protected virtual void EnterButtom()
        {
            switch (_CurrentGameState)
            {
                default: break;
            }
        }
        #endregion

        #region Internal System functions
        /// <summary>
        /// Processes the Key Press in the Key Board.
        /// </summary>
        /// <param name="msg">Windows message.</param>
        /// <param name="keyData">Name of the Key pressed.</param>
        /// <returns><b>True</b> if the Key was processed.</returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.W)
            {
                WButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.S)
            {
                SButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.A)
            {
                AButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.D)
            {
                DButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.J)
            {
                JButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.I)
            {
                IButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.K)
            {
                KButtom();
                UpdateDebugData();
                return true;
            }
            else if (keyData == Keys.L)
            {
                LButtom();
                return true;
            }
            else if (keyData == Keys.Enter)
            {
                EnterButtom();
                return true;
            }
            else
                return base.ProcessCmdKey(ref msg, keyData);
        }
        /// <summary>
        /// Closes the whole application when this Form is closed.
        /// </summary>
        /// <param name="e">Event Argument.</param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #region Board Element Locations
        private Point TurnDeciderPanelLoc = new Point(156, 100);
        private Point PhasePanelCenterLoc = new Point(0, 160);
        private Point BluePhasePanelLoc = new Point(0, 280);
        private Point RedPhasePanelLoc = new Point(10, 20);
        private Point Blue1stAttackPanelLoc = new Point(602, 291);
        private Point Red1stAttackPanelLoc = new Point(602, 40);
        private Point BlueActiveLoc = new Point(268, 187);
        private Point RedActiveLoc = new Point(352, 187);
        private Point CentralMessageLoc = new Point(198, 181);
        private Point RedAttackSelectorLoc = new Point(64, 39);
        private Point BlueAttackSelectorLoc = new Point(64, 234);
        #endregion

        #region Text Symbols
        private const string SYM_DPAD = "✚";
        private const string SYM_CIRCLE = "⚫";
        private const string SYM_TRIANGLE = "▲";
        private const string SYM_CROSS = "✖";
        private const string SYM_SQUARE = "☐";
        private const string SYM_DARK = "☠";
        private const string SYM_NATURE = "🌼";
        private const string SYM_ICE = "❄️";
        private const string SYM_FIRE = "🔥";
        private const string SYM_RARE = "❓";
        #endregion
    }
}