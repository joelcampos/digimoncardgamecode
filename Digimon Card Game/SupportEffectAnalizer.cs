﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digimon_Card_Game
{
    public static class SupportEffectAnalizer
    {
        public static void InitializeAnalazerData(PlayerBattleData activePlayerData, PlayerBattleData oponentPlayerData)
        {
            _ActivePlayerData = activePlayerData;
            _OpponentPlayerData = oponentPlayerData;
        }
        public static bool IsConditionMet(CardInfo cardData)
        {
            bool conditionMet = false;

            Condition thisCardCondition = cardData.condition.condition;

            switch(thisCardCondition)
            {
                //NOTE: for all conditions the opponent Attack selecton is Unknown, therefore conditions
                //that are met based on the attack selection will all be mark as true.
                case Condition.None: 
                    conditionMet = true; break;
                case Condition.If_Both_Attack_Different: 
                    conditionMet = true; break;
                case Condition.If_Both_Attack_Same: 
                    conditionMet = true; break;
                case Condition.If_Both_Levels_Are:
                    //Condition: if Both Levels Are X
                    //Where X is the condition mark ("R","C","U","A")
                    string conditionMark = cardData.condition.condition_Mark;

                    string ownLevel = _ActivePlayerData.ActiveDigimonLevel;
                    string oppLevel = _OpponentPlayerData.ActiveDigimonLevel;

                    //if both actives' Levels are the same as the condition mark.
                    conditionMet = (ownLevel == conditionMark && oppLevel == conditionMark);
                    break;
                case Condition.If_Both_Specialties_Same:
                    //Condition: if Both specialities are the same

                    string ownSpeciality = _ActivePlayerData.ActiveDigimonSpecialty;
                    string oppSpeciality = _OpponentPlayerData.ActiveDigimonSpecialty;

                    conditionMet = ownSpeciality == oppSpeciality;
                    break;
                case Condition.If_OpoHp_MoreThan:
                    //Condition: If opponent HP is > X
                    //Where X is the condition mark (int value)
                    conditionMark = cardData.condition.condition_Mark;

                    conditionMet = (_OpponentPlayerData.ActiveDigimonCurrentHP > Convert.ToInt32(conditionMark));
                    break;
                case Condition.If_Opo_Attack_Is: 
                     conditionMet = true;  break;
                case Condition.If_Opo_DP_IsMore:
                    //Condition: if Oponent DP Amount is > Own
                    conditionMet =  (_OpponentPlayerData.CurrentDPAmount > _ActivePlayerData.CurrentDPAmount);
                    break;
                case Condition.If_Opo_level_Is:
                    //Condition: If the Oponent level is X
                    //Where X is the condition mark ("R","C","U","A")
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _OpponentPlayerData.ActiveDigimonLevel == conditionMark;
                    break;
                case Condition.If_Opo_Speciality_Is:
                    //Condition: If Oponent Active's Speciality is X
                    //Where X is the condition mark ("FIRE", "ICE", "NATURE", "DARK", "RARE")
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _OpponentPlayerData.ActiveDigimonSpecialty == conditionMark;
                    break;
                case Condition.If_Opo_Speciality_IsNot:
                    //Condition: If Oponent Active's Speciality is X
                    //Where X is the condition mark ("FIRE", "ICE", "NATURE", "DARK", "RARE")
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _OpponentPlayerData.ActiveDigimonSpecialty != conditionMark;
                    break;
                case Condition.If_Opo_Specialty_FIREICE:
                    //Condition: If Oponent Active's Speciality is FIRE or ICE
                    conditionMet = _OpponentPlayerData.ActiveDigimonSpecialty == "FIRE" || _OpponentPlayerData.ActiveDigimonSpecialty == "ICE";
                    break;
                case Condition.If_Opo_Specialty_NATUREDARK:
                    //Condition: If Oponent Active's Speciality is NATURE or DARK
                    conditionMet = _OpponentPlayerData.ActiveDigimonSpecialty == "NATURE" || _OpponentPlayerData.ActiveDigimonSpecialty == "DARK";
                    break;
                case Condition.If_OwnHp_IsHigher:
                    //Condition: if Own active HP is higher than Oponents.
                    conditionMet = _ActivePlayerData.ActiveDigimonCurrentHP > _OpponentPlayerData.ActiveDigimonCurrentHP; 
                    break;
                case Condition.If_OwnHp_IsLower:
                    //Condition: if Own active HP is lower than Oponents.
                    conditionMet = _ActivePlayerData.ActiveDigimonCurrentHP < _OpponentPlayerData.ActiveDigimonCurrentHP;
                    break;
                case Condition.If_OwnHP_Lessthan:
                    //Condition: if Own HP is Less than X
                    //Where X is the condition mark (An amount)
                    conditionMark = cardData.condition.condition_Mark;
                    int targetAmount = Convert.ToInt32(conditionMark);
                    conditionMet = _ActivePlayerData.ActiveDigimonCurrentHP < targetAmount;
                    break;
                case Condition.If_Own_Attack_Is:
                    //Condition: If own attack selection is X
                    //Where X is the condition mark.
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _ActivePlayerData.AttackSelection == conditionMark;
                    break;
                case Condition.If_Own_Attack_ISNOT:
                    //Condition: If own attack selection is NOT X
                    //Where X is the condition mark.
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _ActivePlayerData.AttackSelection != conditionMark;
                    break;
                case Condition.If_Own_Hand_LessThan:
                    //Condition: if own hand size is less than X
                    //Where X is the condition mark
                    conditionMark = cardData.condition.condition_Mark;
                    int handsize = Convert.ToInt32(conditionMark);
                    conditionMet = _ActivePlayerData.CardsOnHand < handsize;
                    break;
                case Condition.If_Own_Hand_MoreThan:
                    //Condition: if own hand size is less than X
                    //Where X is the condition mark
                    conditionMark = cardData.condition.condition_Mark;
                    handsize = Convert.ToInt32(conditionMark);
                    conditionMet = _ActivePlayerData.CardsOnHand > handsize;
                    break;
                case Condition.If_Own_level_Is:
                    //Condition: if own active Level is X
                    //Where X is the condition mark
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _ActivePlayerData.ActiveDigimonLevel == conditionMark;
                    break;
                case Condition.If_Own_level_IsLower:
                    //Condition: if Own active Level is Lower than oponents
                    conditionMet = LevelIsLowerTo(_ActivePlayerData.ActiveDigimonLevel, _OpponentPlayerData.ActiveDigimonLevel);
                    break;
                case Condition.If_Own_Speciality_Is:
                    //Condition: If Own Active's Speciality is X
                    //Where X is the condition mark ("FIRE", "ICE", "NATURE", "DARK", "RARE")
                    conditionMark = cardData.condition.condition_Mark;
                    conditionMet = _ActivePlayerData.ActiveDigimonSpecialty == conditionMark;
                    break;
                default:
                    throw new Exception("SupportEffectAnalizer.isConditionMet fail: Condition is not in the Switch statement. Conditio: " + thisCardCondition);
            }

            return conditionMet;
        }
        private static bool LevelIsLowerTo(string level, string levelToCompareTo)
        {
            bool isLower = false;

            //U is higher than all Non-U
            if (levelToCompareTo == "U" && level != "U")
            {
                isLower = true;
            }
            //C is higher then A and R
            if(levelToCompareTo == "C" && (level == "R" || level == "A"))
            {
                isLower = true;
            }
            //A is higher than R
            if(levelToCompareTo == "A" && level == "R") 
            {
                isLower = true;
            }
            return isLower;
        }

        private static PlayerBattleData _ActivePlayerData;
        private static PlayerBattleData _OpponentPlayerData;
    }
}
