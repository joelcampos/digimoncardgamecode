﻿//Joel Campos
//2/27/2021
//Image Server Class


using System;
using System.Drawing;
using System.IO;

namespace Digimon_Card_Game
{
    public static class ImageServer
    {
        public static Image CardImage(int id)
        {
            if (id == -1) 
            {
                return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Cards\\static.png");
            }
            else
            {
                return Image.FromFile(
                    Directory.GetCurrentDirectory() + "\\images\\Cards\\" + id + ".png");
            }                
        }

        public static Image CardFaceDownImage()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Cards\\BackStand.png");
        }

        public static Image Level(string l)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Icons\\" + l + "Level.png");
        }

        public static Image OptionIcon()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Icons\\Option.png");
        }

        public static Image SevensIcon()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Icons\\Sevens.png");
        }

        public static Image EvolutionIcon()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Icons\\Evolution.png");
        }

        public static Image TurnDeciderCard(bool firstturn)
        {
            if(firstturn)
                return Image.FromFile(
                Directory.GetCurrentDirectory() + "\\images\\Cards\\1stTurn.png");
            else
                return Image.FromFile(
                Directory.GetCurrentDirectory() + "\\images\\Cards\\2ndTurn.png");
        }

        public static Image Blinking()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\BoardElements\\blink.gif");
        }

        public static Image Priority(string pri)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\BoardElements\\" + pri + ".png");
        }

        public static Image Status(string level)
        {
            string status = "none";

            switch(level)
            {
                case "R": status = "OK"; break;
                case "C": status = "12"; break;
                case "U": status = "14"; break;
                case "A": status = "OK"; break;
            }
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\BoardElements\\" + status + "Status.png");
        }
        public static Image OKStatus()
        {
             return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\BoardElements\\OKStatus.png");
        }

        public static Image Phase(bool blue1stAttack, Phase currentPhase)
        {
            string activeplayer = "none";
            if (blue1stAttack) { activeplayer = "Blue"; } else { activeplayer = "Red"; }


            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\images\\Phases\\" + activeplayer + currentPhase + ".png");
        }
    }
}
