﻿//Joel Campos
//2/27/2021
//Card Info Class

namespace Digimon_Card_Game
{
    public class CardInfo
    {
        public CardInfo()
        {

        }


        public int id { get; set; }
        public string name { get; set; }
        public string Class { get; set; }
        public string level { get; set; }
        public string type { get; set; }

        public int hp { get; set; }
        public int dp { get; set; }
        public int plusp { get; set; }
        public int circle { get; set; }
        public int triangle { get; set; }
        public int cross { get; set; }
        public string xeffect { get; set; }
        public string xpriority { get; set; }
        public string priority { get; set; }
        public string supporteffect { get; set; }

        public SupportCondition condition = new SupportCondition();
        public SupportEffect effect1 = new SupportEffect();
        public SupportEffect effect2 = new SupportEffect();
        public SupportEffect effect3 = new SupportEffect();
        public CrossEffect Ceffect = new CrossEffect();

        public class SupportCondition
        {
            public SupportCondition()
            {

            }
            public Condition condition = Condition.None;
            public string condition_Mark = "None";
            public int condition_Amount = -1;
        }

        public class SupportEffect
        {
            public SupportEffect()
            {

            }
            public S_Effect Effect = S_Effect.None;
            public string Effect_Mark = "None";
            public int Effect_Amount = -1;
        }

        public class CrossEffect
        {
            public CrossEffect()
            {

            }
            public X_Effect Effect = X_Effect.None;
            public string Effect_Mark = "None";
            public int Effect_Amount = -1;
        }
    }
}
