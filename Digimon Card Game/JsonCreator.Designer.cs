﻿
namespace Digimon_Card_Game
{
    partial class JsonCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtType = new System.Windows.Forms.TextBox();
            this.txtLevel = new System.Windows.Forms.TextBox();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listCrossEffect = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtXPriority = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSupport = new System.Windows.Forms.TextBox();
            this.txtPriority = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtXEffect = new System.Windows.Forms.TextBox();
            this.txtCross = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHp = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTriangle = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCircle = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPlusP = new System.Windows.Forms.TextBox();
            this.txtDP = new System.Windows.Forms.TextBox();
            this.listMainList = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.listSupEffectCode = new System.Windows.Forms.ListBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtXEffectMark = new System.Windows.Forms.TextBox();
            this.txtSupEffectMark = new System.Windows.Forms.TextBox();
            this.txtSupEffectAmount = new System.Windows.Forms.TextBox();
            this.txtSupEffectAmount2 = new System.Windows.Forms.TextBox();
            this.txtSupEffectMark2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.listSupEffectCode2 = new System.Windows.Forms.ListBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSupEffectAmount3 = new System.Windows.Forms.TextBox();
            this.txtSupEffectMark3 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.listSupEffectCode3 = new System.Windows.Forms.ListBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtConditionMark = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.listConditions = new System.Windows.Forms.ListBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtConditionAmount = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(52, 6);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 0;
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(52, 110);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(100, 20);
            this.txtType.TabIndex = 4;
            // 
            // txtLevel
            // 
            this.txtLevel.Location = new System.Drawing.Point(52, 84);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.Size = new System.Drawing.Size(100, 20);
            this.txtLevel.TabIndex = 3;
            // 
            // txtClass
            // 
            this.txtClass.Location = new System.Drawing.Point(52, 58);
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(100, 20);
            this.txtClass.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(52, 32);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(10, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(10, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Class";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(11, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "level";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(11, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Type";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.txtConditionAmount);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.txtConditionMark);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.listConditions);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.txtSupEffectAmount3);
            this.panel1.Controls.Add(this.txtSupEffectMark3);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.listSupEffectCode3);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.txtSupEffectAmount2);
            this.panel1.Controls.Add(this.txtSupEffectMark2);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.listSupEffectCode2);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.txtSupEffectAmount);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.txtSupEffectMark);
            this.panel1.Controls.Add(this.txtXEffectMark);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.listSupEffectCode);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.listCrossEffect);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtXPriority);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtSupport);
            this.panel1.Controls.Add(this.txtPriority);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtXEffect);
            this.panel1.Controls.Add(this.txtCross);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtHp);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtTriangle);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtCircle);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtPlusP);
            this.panel1.Controls.Add(this.txtDP);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtID);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtType);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtLevel);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtClass);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Location = new System.Drawing.Point(194, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 433);
            this.panel1.TabIndex = 10;
            // 
            // listCrossEffect
            // 
            this.listCrossEffect.FormattingEnabled = true;
            this.listCrossEffect.Location = new System.Drawing.Point(249, 11);
            this.listCrossEffect.Name = "listCrossEffect";
            this.listCrossEffect.Size = new System.Drawing.Size(160, 17);
            this.listCrossEffect.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label16.Location = new System.Drawing.Point(168, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "X Effect code:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label15.Location = new System.Drawing.Point(3, 321);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "X Priority";
            // 
            // txtXPriority
            // 
            this.txtXPriority.Location = new System.Drawing.Point(52, 316);
            this.txtXPriority.Name = "txtXPriority";
            this.txtXPriority.Size = new System.Drawing.Size(100, 20);
            this.txtXPriority.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label13.Location = new System.Drawing.Point(6, 371);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Support";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label14.Location = new System.Drawing.Point(11, 345);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Priority";
            // 
            // txtSupport
            // 
            this.txtSupport.Location = new System.Drawing.Point(52, 365);
            this.txtSupport.Multiline = true;
            this.txtSupport.Name = "txtSupport";
            this.txtSupport.Size = new System.Drawing.Size(214, 62);
            this.txtSupport.TabIndex = 14;
            // 
            // txtPriority
            // 
            this.txtPriority.Location = new System.Drawing.Point(52, 340);
            this.txtPriority.Name = "txtPriority";
            this.txtPriority.Size = new System.Drawing.Size(100, 20);
            this.txtPriority.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(6, 298);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "X Effect";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(11, 271);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Cross";
            // 
            // txtXEffect
            // 
            this.txtXEffect.Location = new System.Drawing.Point(52, 292);
            this.txtXEffect.Name = "txtXEffect";
            this.txtXEffect.Size = new System.Drawing.Size(100, 20);
            this.txtXEffect.TabIndex = 11;
            // 
            // txtCross
            // 
            this.txtCross.Location = new System.Drawing.Point(52, 266);
            this.txtCross.Name = "txtCross";
            this.txtCross.Size = new System.Drawing.Size(100, 20);
            this.txtCross.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(10, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "HP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(6, 246);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Triangle";
            // 
            // txtHp
            // 
            this.txtHp.Location = new System.Drawing.Point(52, 136);
            this.txtHp.Name = "txtHp";
            this.txtHp.Size = new System.Drawing.Size(100, 20);
            this.txtHp.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Location = new System.Drawing.Point(11, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Circle";
            // 
            // txtTriangle
            // 
            this.txtTriangle.Location = new System.Drawing.Point(52, 240);
            this.txtTriangle.Name = "txtTriangle";
            this.txtTriangle.Size = new System.Drawing.Size(100, 20);
            this.txtTriangle.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Location = new System.Drawing.Point(10, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "+P";
            // 
            // txtCircle
            // 
            this.txtCircle.Location = new System.Drawing.Point(52, 214);
            this.txtCircle.Name = "txtCircle";
            this.txtCircle.Size = new System.Drawing.Size(100, 20);
            this.txtCircle.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(10, 164);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "DP";
            // 
            // txtPlusP
            // 
            this.txtPlusP.Location = new System.Drawing.Point(52, 188);
            this.txtPlusP.Name = "txtPlusP";
            this.txtPlusP.Size = new System.Drawing.Size(100, 20);
            this.txtPlusP.TabIndex = 7;
            // 
            // txtDP
            // 
            this.txtDP.Location = new System.Drawing.Point(52, 162);
            this.txtDP.Name = "txtDP";
            this.txtDP.Size = new System.Drawing.Size(100, 20);
            this.txtDP.TabIndex = 6;
            // 
            // listMainList
            // 
            this.listMainList.BackColor = System.Drawing.SystemColors.InfoText;
            this.listMainList.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listMainList.ForeColor = System.Drawing.SystemColors.Window;
            this.listMainList.FormattingEnabled = true;
            this.listMainList.ItemHeight = 17;
            this.listMainList.Location = new System.Drawing.Point(3, 5);
            this.listMainList.Name = "listMainList";
            this.listMainList.Size = new System.Drawing.Size(187, 395);
            this.listMainList.TabIndex = 11;
            this.listMainList.SelectedIndexChanged += new System.EventHandler(this.listMainList_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(311, 331);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(58, 87);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "AddCard";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(386, 331);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(55, 87);
            this.btnUpdate.TabIndex = 13;
            this.btnUpdate.Text = "Update Card";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label17.Location = new System.Drawing.Point(168, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "X Effect mark:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label18.Location = new System.Drawing.Point(160, 247);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Sup Effect mark:";
            // 
            // listSupEffectCode
            // 
            this.listSupEffectCode.FormattingEnabled = true;
            this.listSupEffectCode.Location = new System.Drawing.Point(163, 146);
            this.listSupEffectCode.Name = "listSupEffectCode";
            this.listSupEffectCode.Size = new System.Drawing.Size(218, 95);
            this.listSupEffectCode.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label19.Location = new System.Drawing.Point(160, 130);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Sup Effect 1 code:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label20.Location = new System.Drawing.Point(162, 288);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 13);
            this.label20.TabIndex = 38;
            this.label20.Text = "Sup Effect amount:";
            // 
            // txtXEffectMark
            // 
            this.txtXEffectMark.Location = new System.Drawing.Point(248, 34);
            this.txtXEffectMark.Name = "txtXEffectMark";
            this.txtXEffectMark.Size = new System.Drawing.Size(100, 20);
            this.txtXEffectMark.TabIndex = 39;
            // 
            // txtSupEffectMark
            // 
            this.txtSupEffectMark.Location = new System.Drawing.Point(163, 264);
            this.txtSupEffectMark.Name = "txtSupEffectMark";
            this.txtSupEffectMark.Size = new System.Drawing.Size(100, 20);
            this.txtSupEffectMark.TabIndex = 40;
            // 
            // txtSupEffectAmount
            // 
            this.txtSupEffectAmount.Location = new System.Drawing.Point(163, 304);
            this.txtSupEffectAmount.Name = "txtSupEffectAmount";
            this.txtSupEffectAmount.Size = new System.Drawing.Size(100, 20);
            this.txtSupEffectAmount.TabIndex = 41;
            // 
            // txtSupEffectAmount2
            // 
            this.txtSupEffectAmount2.Location = new System.Drawing.Point(387, 304);
            this.txtSupEffectAmount2.Name = "txtSupEffectAmount2";
            this.txtSupEffectAmount2.Size = new System.Drawing.Size(100, 20);
            this.txtSupEffectAmount2.TabIndex = 47;
            // 
            // txtSupEffectMark2
            // 
            this.txtSupEffectMark2.Location = new System.Drawing.Point(387, 264);
            this.txtSupEffectMark2.Name = "txtSupEffectMark2";
            this.txtSupEffectMark2.Size = new System.Drawing.Size(100, 20);
            this.txtSupEffectMark2.TabIndex = 46;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label21.Location = new System.Drawing.Point(386, 288);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 13);
            this.label21.TabIndex = 45;
            this.label21.Text = "Sup Effect amount:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label22.Location = new System.Drawing.Point(384, 247);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 44;
            this.label22.Text = "Sup Effect mark:";
            // 
            // listSupEffectCode2
            // 
            this.listSupEffectCode2.FormattingEnabled = true;
            this.listSupEffectCode2.Location = new System.Drawing.Point(387, 146);
            this.listSupEffectCode2.Name = "listSupEffectCode2";
            this.listSupEffectCode2.Size = new System.Drawing.Size(202, 95);
            this.listSupEffectCode2.TabIndex = 43;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label23.Location = new System.Drawing.Point(384, 130);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "Sup Effect 2 code:";
            // 
            // txtSupEffectAmount3
            // 
            this.txtSupEffectAmount3.Location = new System.Drawing.Point(595, 304);
            this.txtSupEffectAmount3.Name = "txtSupEffectAmount3";
            this.txtSupEffectAmount3.Size = new System.Drawing.Size(100, 20);
            this.txtSupEffectAmount3.TabIndex = 53;
            // 
            // txtSupEffectMark3
            // 
            this.txtSupEffectMark3.Location = new System.Drawing.Point(595, 264);
            this.txtSupEffectMark3.Name = "txtSupEffectMark3";
            this.txtSupEffectMark3.Size = new System.Drawing.Size(100, 20);
            this.txtSupEffectMark3.TabIndex = 52;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label24.Location = new System.Drawing.Point(594, 288);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(98, 13);
            this.label24.TabIndex = 51;
            this.label24.Text = "Sup Effect amount:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label25.Location = new System.Drawing.Point(592, 247);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 13);
            this.label25.TabIndex = 50;
            this.label25.Text = "Sup Effect mark:";
            // 
            // listSupEffectCode3
            // 
            this.listSupEffectCode3.FormattingEnabled = true;
            this.listSupEffectCode3.Location = new System.Drawing.Point(595, 146);
            this.listSupEffectCode3.Name = "listSupEffectCode3";
            this.listSupEffectCode3.Size = new System.Drawing.Size(202, 95);
            this.listSupEffectCode3.TabIndex = 49;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label26.Location = new System.Drawing.Point(592, 130);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 13);
            this.label26.TabIndex = 48;
            this.label26.Text = "Sup Effect 3 code:";
            // 
            // txtConditionMark
            // 
            this.txtConditionMark.Location = new System.Drawing.Point(543, 76);
            this.txtConditionMark.Name = "txtConditionMark";
            this.txtConditionMark.Size = new System.Drawing.Size(100, 20);
            this.txtConditionMark.TabIndex = 57;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label27.Location = new System.Drawing.Point(463, 79);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(80, 13);
            this.label27.TabIndex = 56;
            this.label27.Text = "Condition mark:";
            // 
            // listConditions
            // 
            this.listConditions.FormattingEnabled = true;
            this.listConditions.Location = new System.Drawing.Point(544, 13);
            this.listConditions.Name = "listConditions";
            this.listConditions.Size = new System.Drawing.Size(160, 56);
            this.listConditions.TabIndex = 55;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label28.Location = new System.Drawing.Point(463, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(81, 13);
            this.label28.TabIndex = 54;
            this.label28.Text = "Condition code:";
            // 
            // txtConditionAmount
            // 
            this.txtConditionAmount.Location = new System.Drawing.Point(543, 102);
            this.txtConditionAmount.Name = "txtConditionAmount";
            this.txtConditionAmount.Size = new System.Drawing.Size(100, 20);
            this.txtConditionAmount.TabIndex = 59;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label29.Location = new System.Drawing.Point(452, 105);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(92, 13);
            this.label29.TabIndex = 58;
            this.label29.Text = "Condition amount:";
            // 
            // JsonCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 450);
            this.Controls.Add(this.listMainList);
            this.Controls.Add(this.panel1);
            this.Name = "JsonCreator";
            this.Text = "JsonCreator";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.TextBox txtLevel;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtSupport;
        private System.Windows.Forms.TextBox txtPriority;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtXEffect;
        private System.Windows.Forms.TextBox txtCross;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTriangle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCircle;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPlusP;
        private System.Windows.Forms.TextBox txtDP;
        private System.Windows.Forms.ListBox listMainList;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtXPriority;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ListBox listCrossEffect;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListBox listSupEffectCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtSupEffectAmount;
        private System.Windows.Forms.TextBox txtSupEffectMark;
        private System.Windows.Forms.TextBox txtXEffectMark;
        private System.Windows.Forms.TextBox txtSupEffectAmount3;
        private System.Windows.Forms.TextBox txtSupEffectMark3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ListBox listSupEffectCode3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtSupEffectAmount2;
        private System.Windows.Forms.TextBox txtSupEffectMark2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ListBox listSupEffectCode2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtConditionAmount;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtConditionMark;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListBox listConditions;
        private System.Windows.Forms.Label label28;
    }
}