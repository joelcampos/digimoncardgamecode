﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace Digimon_Card_Game
{
    public class PlayerBattleData
    {
        public PlayerBattleData(CardSprite activeDigimon, Hand ownHand, string attackSelection, DPPile pile, List<CardInfo> deck)
        {
            _activeDigimon = activeDigimon;
            _ownHand = ownHand;
            _attackSelection = attackSelection;
            _dpPile = pile;
            _deck = deck;
        }

        //Active Digimon Data
        public string ActiveDigimonLevel
        {
            get { return _activeDigimon.Level; }
        }
        public string ActiveDigimonSpecialty
        {
            get { return _activeDigimon.Type; }
        }
        public int ActiveDigimonCurrentHP
        {
            get { return _activeDigimon.Active_HP; }
        }
        //Hand Data
        public int CardsOnHand
        {
            get { return _ownHand.Size; }
        }
        public bool HandIndexIsOpenSlot(int i)
        {
            return _ownHand.IsSlotOpen(i);
        }
        public CardInfo CardOnHandAtIndex(int i)
        {
            return _ownHand.GetCardInfo(i);
        }
        //Attack Data
        public string AttackSelection
        {
            get { return _attackSelection; }
        }
        //DP Data
        public int CurrentDPAmount
        {
            get { return _dpPile.Amount; }
        }
        //Deck Data
        public int CardsOnDeck
        {
            get { return _deck.Count;}
        }


        private CardSprite _activeDigimon;
        private Hand _ownHand;
        private string _attackSelection;
        DPPile _dpPile;
        List<CardInfo> _deck;
    }
}
