﻿//Joel campos
//2/27/2021
//Enum Tupes

namespace Digimon_Card_Game
{
    public enum Player
    {
        Blue,
        Red
    }
    public enum GameState
    {
        None = 0,
        TurnDeciderInView,
        PreparingHand,
        Redrawing,
        CardViewMode,
        DiscardQuestion,
        GameOver,
        EndOfPreparationQuestion,
        NoBattlePhaseMessage,
        EndOfDigivolveQuestion,
        AttackSelecion,
        CPUActive,
        NoSupportCardsMessage,
        NoSupportCardUsageQuestion
    }
    public enum CardPointerTarget
    {
        None = 0,
        BlueActive,
        BlueHand1,
        BlueHand2,
        BlueHand3,
        BlueHand4,
        BlueDeck,
        BlueDP,
        RedActive,
        RedHand1,
        RedHand2,
        RedHand3,
        RedHand4,
        RedDeck,
        RedDP,
    }
    public enum Phase
    {
        None = 0,
        Phase1_DrawCards,
        Phase2_DigimonEntrance,
        Phase3_RackUpDP,
        Phase4_UseDigivolveOption,
        Phase5_DigimonDigivolve,
        Phase6_ChooseAnAttack,
        Phase7_UseSupportCard
    }
    public enum Level
    {
        R,
        C,
        U,
        A,
        Rp
    }

    public enum S_Effect
    {
        None = 0,
        //Attack Turn Effects
        Attack_First,
        Attack_Second,
        //Change Specialty Effects
        Change_own_Speciality,
        Change_Opo_Speciality,
        Swap_Speciality,
        Change_Opo_Speciality_ToOWn,
        Change_Own_Speciality_ToOpo,
        //Boost/Reduce own Attack Effects
        Boost_Own_AllAttack,
        Boost_Own_Specific,
        Boost_Own_Double,
        Boost_Own_Triple,
        Boost_Own_Specific_Tripled,
        Boost_Own_ByHP,
        Boost_Own_Attack_By_HandAmount100,
        Discard_Own_AllDP_MultiATAByAmount,
        Discard_Own_AllHand_MultiATAByAmount,
        Lower_Own_Half,
        Lower_Own_All_ToZero,
        //Boost/Reduce oponent Attack Effects
        Lower_Opo_All_ToZero,
        Lower_Opo_Specific_ToZero,
        Lower_Opo_Half,
        Lower_Opo_AllAttack,
        Lower_Opo_Specific,
        Opo_Attack_Becomes,
        //Attack to HP.
        Own_Specific_Becomes_HP,
        //Attack change Effects.
        Change_Own_AttackTo,
        Change_Opo_AttackTo,
        Change_Both_AttackTo,
        //HP Change Effects.
        Own_HP_Becomes,
        Opo_HP_Becomes,
        Own_HP_BecomesOpp,
        Opo_HP_BecomesOwn,
        Recover_Own_Hp,
        Opo_HP_double,
        Opo_HP_Half,
        Own_HP_Half,
        //Draw/Deck Card Effects
        Draw,
        Return_Own_AllHand_ToDeck,
        ShuffleDeck,
        //Discard Effects.
        Discard_Own_AllHand,
        Discard_Opo_AllHand,
        Discard_Own_AllDP,
        Discard_Opo_AllDP,
        Discard_Own_DP,
        Discard_Opo_DP,
        Discard_Own_FromDeck,
        Discard_Opo_FromDeck,
        Discard_Own_Hand,
        Discard_Opo_Hand,
        Discard_Own_AllDP_Opo_Discard_Same,
        //Counter Effects
        Counter,
        //Void Effects
        VoidEffect,
        //Revive Effects
        OWn_KO_Revives,
        //Eat Up Effects
        Own_Attack_EatUp,
        //Discard Pile effects
        OWn_Discard_ToDeck,
        Opo_Discard_ToDeck,
        //Other
        Boost_Own_Attack_By_DPAmount100,
        Recover_HP_By_DPAmount100,
        Lower_Own_AllAttack,
        Boost_Opo_AllAttack,
        Lowe_Own_Specific_ToZero,
        Boost_Opo_Doubled,
        Discard_AllDP_RecoverHpx100,
        Own_AllAttack_Becomes_HP,
        Recover_Opo_HP,
        Swap_Hp,
        DataCopy,
        DrawPartner,
        DisruptRay,
        Own_Specific_Doubled,
    }

    public enum Condition
    {
        None,
        If_Own_Attack_Is,
        If_Own_Attack_ISNOT,
        If_Opo_Attack_Is,
        If_Both_Attack_Same,
        If_Both_Attack_Different,
        If_Own_Speciality_Is,
        If_Opo_Speciality_Is,
        If_Opo_Specialty_FIREICE,
        If_Opo_Specialty_NATUREDARK,
        If_Opo_Speciality_IsNot,
        If_Both_Specialties_Same,
        If_Own_level_Is,
        If_Opo_level_Is,
        If_Both_Levels_Are,
        If_Own_level_IsLower,
        If_Own_Hand_MoreThan,
        If_Own_Hand_LessThan,
        If_OwnHP_Lessthan,
        If_OwnHp_IsHigher,
        If_OwnHp_IsLower,
        If_OpoHp_MoreThan,
        If_Opo_DP_IsMore,

    }

    public enum X_Effect
    {
        None,
        Counter,
        Foe_XAttack,
        EatUp,
        Attack_ToZero,
        FirstAttack,
        Crash,
        Jamming,
    }
}
